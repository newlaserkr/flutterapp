import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/util/string.dart';
import 'package:preetibhojan/util/ui/CustomBackground.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

class CustomScaffold extends StatefulWidget {
  final Widget body;
  final GetCredentialsFunc getCredentialsFunc;
  final bool showCircularProgressIndicator;
  final Widget floatingActionButton;

  static final appBar = AppBar(
    title: Row(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white,
          ),
          child: Image.asset(
            'assets/app icon.png',
            height: 50,
            width: 50,
          ),
        ),
        Image.asset(
          'assets/app bar.png',
          height: 60,
          width: 180,
        ),
      ],
    ),
  );

  const CustomScaffold({
    Key key,
    @required this.body,
    @required this.getCredentialsFunc,
    this.showCircularProgressIndicator = false,
    this.floatingActionButton,
  }) : super(key: key);

  @override
  _CustomScaffoldState createState() => _CustomScaffoldState();
}

class _CustomScaffoldState extends State<CustomScaffold> {
  Future<CustomerCredentials> customerCredentials;

  @override
  void initState() {
    super.initState();
    customerCredentials = widget.getCredentialsFunc();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: customerCredentials,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return buildScaffold(
              snapshot.data.name, snapshot.data.isRegularCustomer);
        }

        if (snapshot.hasError) {
          SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
            showDialog(context: context, builder: (_) => SomethingWentWrong());
          });
        }

        return buildScaffold('');
      },
    );
  }

  Widget buildScaffold(String name, [bool isRegularCustomer]) {
    if (isRegularCustomer == null) {
      isRegularCustomer = false;
    }

    return Scaffold(
      appBar: CustomScaffold.appBar,
      body: SafeArea(
        child: CustomBackground(
          child: this.widget.body,
          showCircularProgressIndicator: widget.showCircularProgressIndicator,
        ),
      ),
      floatingActionButton: widget.floatingActionButton,
      drawer: SizedBox(
        width: 320,
        child: Drawer(
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 70,
                child: DrawerHeader(
                  padding: const EdgeInsets.fromLTRB(30, 0, 0, 0),
                  decoration: BoxDecoration(
                    color: secondaryColor,
                  ),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                            ),
                            child: Image.asset(
                              'assets/app icon.png',
                              height: 50,
                              width: 50,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                            child: Image.asset(
                              'assets/app bar.png',
                              height: 60,
                              width: 180,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              CustomTile(
                  title: Text(
                    'Hello, ${name.capitalize()}',
                  ),
                  onTap: (context) {}),
              CustomTile(
                title: Text('Home'),
                onTap: (context) {
                  if (Scaffold.of(context).isDrawerOpen) {
                    Navigator.of(context).pop();
                  }
                  Navigator.of(context).pushNamedAndRemoveUntil(
                    '/home',
                    ModalRoute.withName('/home'),
                  );
                },
              ),
              CustomTile(
                title: Text('Profile'),
                onTap: (context) {
                  if (Scaffold.of(context).isDrawerOpen) {
                    Navigator.of(context).pop();
                  }
                  Navigator.of(context).pushNamedAndRemoveUntil(
                    '/profile',
                    ModalRoute.withName('/home'),
                  );
                },
              ),
              CustomTile(
                title: Text(isRegularCustomer ? 'Re Schedule' : 'Schedule'),
                onTap: (context) {
                  if (Scaffold.of(context).isDrawerOpen) {
                    Navigator.of(context).pop();
                  }
                  Navigator.of(context).pushNamedAndRemoveUntil(
                    '/schedule',
                    ModalRoute.withName('/home'),
                  );
                },
              ),
              CustomTile(
                title: Text('Booking'),
                onTap: (context) {
                  if (Scaffold.of(context).isDrawerOpen) {
                    Navigator.of(context).pop();
                  }

                  Navigator.of(context).pushNamedAndRemoveUntil(
                    '/booking',
                    ModalRoute.withName('/home'),
                  );
                },
              ),
              CustomTile(
                title: Text('Cancellation'),
                onTap: (context) {
                  if (Scaffold.of(context).isDrawerOpen) {
                    Navigator.of(context).pop();
                  }
                  Navigator.of(context).pushNamedAndRemoveUntil(
                    '/cancellation',
                    ModalRoute.withName('/home'),
                  );
                },
              ),
              CustomTile(
                title: Text('Resume'),
                onTap: (context) {
                  if (Scaffold.of(context).isDrawerOpen) {
                    Navigator.of(context).pop();
                  }
                  Navigator.of(context).pushNamedAndRemoveUntil(
                    '/resume',
                    ModalRoute.withName('/home'),
                  );
                },
              ),
              CustomTile(
                title: Text('Your Orders'),
                onTap: (context) {
                  if (Scaffold.of(context).isDrawerOpen) {
                    Navigator.of(context).pop();
                  }
                  Navigator.of(context).pushNamedAndRemoveUntil(
                    '/my_orders',
                    ModalRoute.withName('/home'),
                  );
                },
              ),
              CustomTile(
                title: Text('Payment History'),
                onTap: (context) {},
              ),
              CustomTile(
                title: Text('Log out'),
                onTap: (context) async {
                  if (Scaffold.of(context).isDrawerOpen) {
                    Navigator.of(context).pop();
                  }

                  await deleteCredentials();

                  Navigator.of(context).popUntil((route) => route.isFirst);
                  Navigator.of(context).pushReplacementNamed('/');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

typedef void OnTap(context);

class CustomTile extends StatelessWidget {
  final Widget title;
  final OnTap onTap;

  const CustomTile({
    @required this.title,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: title,
      onTap: () => onTap(context),
    );
  }
}
