import 'package:flutter/material.dart';

class EditTextDialog extends StatefulWidget {
  /// Label for text editing controller
  final String label;

  /// Optional initial value for TextFormField. Empty by default
  final String initialValue;

  /// What to do when customer cancels the edit
  final VoidCallback onCancel;

  /// What to do when customer successfully edits the field and saved the value
  final ValueChanged<String> onSave;

  /// Validator for the text field
  final FormFieldValidator<String> validator;

  /// type of keyboard
  final TextInputType keyboardType;

  const EditTextDialog({
    Key key,
    @required this.label,
    @required this.onCancel,
    @required this.onSave,
    @required this.validator,
    this.initialValue = '',
    this.keyboardType,
  }) : super(key: key);

  @override
  _EditTextDialogState createState() => _EditTextDialogState();
}

class _EditTextDialogState extends State<EditTextDialog> {
  final controller = TextEditingController();
  final key = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    controller.text = widget.initialValue;
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Edit'),
      content: Form(
        key: key,
        autovalidate: true,
        child: TextFormField(
          autofocus: true,
          controller: controller,
          validator: widget.validator,
          decoration: InputDecoration(
            labelText: widget.label,
          ),
          keyboardType: widget.keyboardType,
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Save'),
          onPressed: () {
            if (key.currentState.validate()) {
              Navigator.of(context).pop();
              widget.onSave(controller.text.trim());
            }
          },
        ),
        FlatButton(
          child: Text('Cancel'),
          onPressed: widget.onCancel,
        ),
      ],
    );
  }
}
