import 'package:flutter/material.dart';

class QuantityPicker extends StatelessWidget {
  final Color borderColor, textColor;
  final int value;
  final VoidCallback onDecreaseQuantity, onIncreaseQuantity;

  const QuantityPicker({
    Key key,
    @required this.value,
    @required this.onDecreaseQuantity,
    @required this.onIncreaseQuantity,
    this.borderColor = Colors.black,
    this.textColor = Colors.black,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: borderColor),
        borderRadius: BorderRadius.circular(12),
      ),
      height: 30,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          IconButton(
            onPressed: onDecreaseQuantity,
            icon: Icon(
              Icons.remove,
              color: textColor,
            ),
            iconSize: 14,
          ),
          Text(
            '$value',
            style: TextStyle(color: textColor, fontWeight: FontWeight.bold),
          ),
          IconButton(
            onPressed: onIncreaseQuantity,
            icon: Icon(
              Icons.add,
              color: textColor,
            ),
            iconSize: 14,
          ),
        ],
      ),
    );
  }
}
