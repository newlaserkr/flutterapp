import 'package:validators/validators.dart';

bool isAlphaNumericUnderScoreOrSpace(String str) =>
    matches(str.trim(), r'^[\w\s]+$');

extension StringExtenstion on String {
  String capitalize() {
    if (this != null && this.length > 0)
      return '${this[0].toUpperCase()}${this.substring(1).toLowerCase()}';
    return this;
  }
}
