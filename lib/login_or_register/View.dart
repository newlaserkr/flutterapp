import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/util/ui/CardTile.dart';
import 'package:preetibhojan/util/ui/CustomBackground.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/InvalidInputDialog.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

class LoginOrRegister extends StatefulWidget {
  @override
  _LoginOrRegisterState createState() => _LoginOrRegisterState();
}

const CUSTOMER_DOES_NOT_EXIST = 1, INVALID_PASSWORD = 2;

class _LoginOrRegisterState extends State<LoginOrRegister> {
  final phoneNumberController = TextEditingController();
  final passwordController = TextEditingController();

  var _passwordVisible = false;

  final _log = Logger('LogInState');

  bool showCircularProgressIndicator = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomScaffold.appBar,
      body: SafeArea(
        child: CustomBackground(
          showCircularProgressIndicator: showCircularProgressIndicator,
          child: ListView(
            children: [
              CardTile(
                title: Text(
                  'Login',
                  style: Theme.of(context).textTheme.headline6,
                ),
                child: Column(
                  children: <Widget>[
                    TextField(
                      controller: phoneNumberController,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        icon: Icon(Icons.phone),
                        labelText: 'Phone number',
                      ),
                    ),
                    TextField(
                      controller: passwordController,
                      keyboardType: TextInputType.visiblePassword,
                      decoration: InputDecoration(
                        icon: Icon(Icons.security),
                        labelText: 'Password',
                        suffixIcon: IconButton(
                          icon: Icon(
                            _passwordVisible
                                ? Icons.visibility_off
                                : Icons.visibility,
                            semanticLabel: _passwordVisible
                                ? 'hide password'
                                : 'show password',
                          ),
                          onPressed: () {
                            setState(
                              () {
                                /// Copied from stack overflow. a xor 1 is same as !a. i.e. short hand for a = !a
                                _passwordVisible ^= true;
                              },
                            );
                          },
                        ),
                      ),
                      obscureText: !_passwordVisible,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        child: Text(
                          'Log in',
/*                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.bold,
                              ),*/
                        ),
                        onPressed: logIn,
                      ),
                    )
                  ],
                ),
              ),
              CardTile(
                title: null,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Don\'t have an account?',
                      style: TextStyle(fontSize: 16),
                    ),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed('/register');
                      },
                      child: Text(
                        'Register',
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void logIn() async {
    /// trimming input is extremely important
    var phoneNumber = phoneNumberController.text.trim();
    var password = passwordController.text.trim();

    if (phoneNumber.isEmpty || password.isEmpty) {
      showDialog(
        context: context,
        builder: (_) => InvalidInputDialog(),
      );

      return;
    }

    setState(() {
      showCircularProgressIndicator = true;
    });

    final request = {
      'phone_number': phoneNumber,
      'password': password,
    };

    http.Response authenticateResponse;
    try {
      _log.info('Authenticating...');
      authenticateResponse = await http.post(
        base_url + '/authenticate',
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode(request),
      );
    } catch (exception, stacktrace) {
      _log.severe('Exception: ', exception, stacktrace);
      showDialog(
        context: context,
        builder: (_) => SomethingWentWrong(),
      );
      return;
    }

    if (authenticateResponse.statusCode != 200) {
      setState(() {
        showCircularProgressIndicator = false;
      });
      final errorCode = jsonDecode(authenticateResponse.body)['error'];

      switch (errorCode) {
        case CUSTOMER_DOES_NOT_EXIST:
          showDialog(
            context: context,
            builder: (_) => AlertDialog(
              title: Text('Oops!'),
              content: Text('No customer with this phone number is registered'),
              actions: <Widget>[
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('Okay'),
                ),
              ],
            ),
          );
          _log.severe('Customer does not exist');
          break;

        case INVALID_PASSWORD:
          showDialog(
            context: context,
            builder: (_) => AlertDialog(
              title: Text('Oops!'),
              content: Text('The password entered is incorrect'),
              actions: <Widget>[
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('Okay'),
                ),
              ],
            ),
          );

          _log.severe('Invalid password');
          break;

        default:
          showDialog(
            context: context,
            builder: (_) => SomethingWentWrong(),
          );
          _log.severe('Authentication error code: $errorCode');
      }

      return;
    }

    http.Response utilityInfoResponse;
    try {
      _log.info('Getting utility info');
      utilityInfoResponse = await http.post(
        base_url + '/utility_information',
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode(request),
      );
    } catch (exception, stacktrace) {
      _log.severe('Exception: ', exception, stacktrace);
    }

    String name = 'Customer';
    bool isRegularCustomer = false;

    if (utilityInfoResponse.statusCode != 200) {
      setState(() {
        showCircularProgressIndicator = false;
      });

      final errorCode = jsonDecode(authenticateResponse.body)['error'];

      _log.warning('Name could not be fetched');
      _log.info('error code: $errorCode');
      _log.info('status code: ${utilityInfoResponse.statusCode}');
      // it is okay if name could not be fetched, It is not needed for the app
      // to work that's why I am not returning here.
    } else {
      var json = jsonDecode(utilityInfoResponse.body);
      name = json['name'];
      isRegularCustomer = json['is_regular_customer'];
    }

    /// Note: write overrides value if exists
    final storage = FlutterSecureStorage();

    await storage.write(
      key: 'phone_number',
      value: phoneNumber,
    );

    await storage.write(
      key: 'password',
      value: password,
    );

    await storage.write(
      key: 'name',
      value: name,
    );

    await storage.write(
      key: 'is_regular_customer',
      value: isRegularCustomer.toString(),
    );

    _log.info('Log in successful');

    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Text('Log in successful'),
        actions: <Widget>[
          FlatButton(
            child: Text('Okay'),
            onPressed: () => Navigator.of(context)
                .pushNamedAndRemoveUntil('/home', (route) => false),
          ),
        ],
      ),
    );
  }
}
