import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';

class DisplayMeal with ChangeNotifier {
  bool _enabled = false;

  bool get enabled => _enabled;

  set enabled(value) {
    _enabled = value;
    notifyListeners();
  }

  final String name;
  final int id;
  final DateTime date;

  DisplayMeal(
    this.name,
    this.date,
    this.id,
  );

  @override
  String toString() {
    return 'name: $name date: $date';
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'date': DateFormat('yyyy-MM-dd').format(date),
    };
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DisplayMeal &&
          runtimeType == other.runtimeType &&
          _enabled == other._enabled &&
          name == other.name;

  @override
  int get hashCode => _enabled.hashCode ^ name.hashCode;
}

class DisplayTile with ChangeNotifier {
  final DateTime date;
  final List<DisplayMeal> meals;

  DisplayTile(this.date, this.meals) {
    for (final meal in meals) {
      meal.addListener(() {
        this.notifyListeners();
      });
    }
  }

  @override
  String toString() {
    return 'date: $date meals: $meals';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DisplayTile &&
          runtimeType == other.runtimeType &&
          date.isAtSameMomentAs(other.date) &&
          ListEquality().equals(meals, other.meals);

  @override
  int get hashCode => date.hashCode ^ meals.hashCode;
}

class CancelledMeal {
  final int id;
  final DateTime date;

  CancelledMeal(this.id, this.date);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CancelledMeal &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          date == other.date;

  @override
  int get hashCode => id.hashCode ^ date.hashCode;

  factory CancelledMeal.fromJson(Map<String, dynamic> json) {
    return CancelledMeal(
      json['id'],
      DateTime.parse(json['date']),
    );
  }
}
