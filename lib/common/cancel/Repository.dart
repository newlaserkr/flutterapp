import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';

import '../Config.dart';
import '../Storage.dart';
import 'Model.dart';

abstract class CancelledMealsRepository {
  Future<List<CancelledMeal>> get cancelledMeals;
}

class StubCancelledMealsRepository implements CancelledMealsRepository {
  final List<CancelledMeal> input;

  StubCancelledMealsRepository(this.input);

  @override
  Future<List<CancelledMeal>> get cancelledMeals => Future.value(input);
}

class NetworkCancelledMealsRepository implements CancelledMealsRepository {
  List<CancelledMeal> _cache;
  final GetCredentialsFunc getCredentialsFunc;

  static final _log = Logger('Network Cancelled Meals');

  NetworkCancelledMealsRepository(this.getCredentialsFunc);

  @override
  Future<List<CancelledMeal>> get cancelledMeals async {
    _log.info('Get cancelled meals');

    if (_cache != null) {
      _log.info('Returning cache');
      return _cache;
    }

    _log.info('Getting credentials');
    final credentials = await getCredentialsFunc();

    final response = await http.post(
      base_url + '/cancelled_meals',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.severe('Could not fetch cancelled meals');
      throw Exception('Could not fetch cancelled meals');
    }

    _cache = (jsonDecode(response.body) as List)
        .map((json) => CancelledMeal.fromJson(json))
        .toList();

    return _cache;
  }
}
