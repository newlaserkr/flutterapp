import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';

import '../Config.dart';
import '../Storage.dart';
import 'Model.dart';

abstract class MealsRepository {
  Future<List<Meal>> get meals;
}

class StubMealsRepository implements MealsRepository {
  final List<Meal> input;

  StubMealsRepository(this.input);

  Future<List<Meal>> get meals => Future.value(input);
}

class NetworkMealsRepository implements MealsRepository {
  static final _log = Logger('Network meals repository');

  final GetCredentialsFunc getCredentialsFunc;
  List<Meal> _cache;

  NetworkMealsRepository(this.getCredentialsFunc);

  @override
  Future<List<Meal>> get meals async {
    _log.info('Get meals');

    if (_cache != null) {
      _log.info('Returning cache');
      return _cache;
    }

    _log.info('Getting credentials');
    final credentials = await getCredentialsFunc();

    _log.info('Sending post request for meals');

    final response = await http.post(base_url + '/meals',
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          'phone_number': credentials.phoneNumber,
          'password': credentials.password,
        }));

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.severe('Could not fetch all meals');
      throw Exception('Could not fetch all meals');
    }

    _cache = (jsonDecode(response.body) as List)
        .map((json) => Meal.fromJson(json))
        .toList();

    return _cache;
  }
}
