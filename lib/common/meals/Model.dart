class Meal implements Comparable {
  final int id, priority, cannotOrderOn;
  final String name, imageUrl;
  final Duration orderBefore;
  final Duration deliveryTime;

  Meal(
    this.id,
    this.priority,
    this.cannotOrderOn,
    this.name,
    this.orderBefore,
    this.deliveryTime,
    this.imageUrl,
  );

  factory Meal.fromJson(Map<String, dynamic> json) {
    return Meal(
        json['id'],
        json['priority'],
        json['cannot_order_on'],
        json['name'],
        Duration(seconds: json['order_before']),
        Duration(seconds: json['delivery_time']),
        json['image_url']);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Meal &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          priority == other.priority &&
          cannotOrderOn == other.cannotOrderOn &&
          name == other.name &&
          imageUrl == other.imageUrl &&
          orderBefore == other.orderBefore &&
          deliveryTime == other.deliveryTime;

  @override
  int get hashCode =>
      id.hashCode ^
      priority.hashCode ^
      cannotOrderOn.hashCode ^
      name.hashCode ^
      imageUrl.hashCode ^
      orderBefore.hashCode ^
      deliveryTime.hashCode;

  /// Used for ordering in cart
  @override
  int compareTo(other) {
    return this.priority.compareTo(other.priority);
  }
}
