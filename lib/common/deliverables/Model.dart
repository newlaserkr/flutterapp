import 'package:preetibhojan/common/delivery_location/Models.dart';

class Deliverable {
  final int packageID, quantity;
  final double price;
  final DeliveryLocation deliveryLocation;
  final DateTime date;

  Deliverable(this.packageID, this.price, this.quantity, this.deliveryLocation,
      this.date);

  factory Deliverable.fromJson(Map<String, dynamic> json) {
    return Deliverable(
      json['package_id'],
      double.parse(json['price']),
      json['quantity'],
      DeliveryLocation.fromInt(json['delivery_location']),
      DateTime.parse(json['date']),
    );
  }
}
