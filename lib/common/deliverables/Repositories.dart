import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';

import '../Config.dart';
import '../Storage.dart';
import 'Model.dart';

abstract class DeliverableRepository {
  Future<List<Deliverable>> get deliverablesForNextWorkingDay;

  Future<List<Deliverable>> get allDeliverables;

  Future<List<Deliverable>> get deliverablesFromTomorrowToTillDate;
}

class StubDeliverableRepository implements DeliverableRepository {
  final List<Deliverable> input1, input2, input3;

  StubDeliverableRepository(this.input1, this.input2, this.input3);

  @override
  Future<List<Deliverable>> get deliverablesForNextWorkingDay =>
      Future.value(input1);

  @override
  Future<List<Deliverable>> get allDeliverables => Future.value(input2);

  @override
  Future<List<Deliverable>> get deliverablesFromTomorrowToTillDate =>
      Future.value(input2);
}

class NetworkDeliverableRepository implements DeliverableRepository {
  List<Deliverable> _deliverablesForTodayCache,
      _allDeliverablesCache,
      _deliverablesFromTomorrowToTillDateCache;

  static final _log = Logger('Network deliverable repository');

  final GetCredentialsFunc getCredentialsFunc;

  NetworkDeliverableRepository(this.getCredentialsFunc);

  @override
  Future<List<Deliverable>> get deliverablesForNextWorkingDay async {
    _log.info('Getting deliverables for today');
    if (_deliverablesForTodayCache != null) {
      _log.info('Returning cache');
      return _deliverablesForTodayCache;
    }

    final credentials = await getCredentialsFunc();

    final response = await http.post(
      base_url + '/deliverables/next_working_day',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.info('Something went wrong while fetching deliverables for today');
      throw Exception(
          'Something went wrong while fetching deliverables for today');
    }

    _deliverablesForTodayCache = (jsonDecode(response.body) as List)
        .map((json) => Deliverable.fromJson(json))
        .toList();

    return _deliverablesForTodayCache;
  }

  @override
  Future<List<Deliverable>> get allDeliverables async {
    _log.info('Getting all deliverables');
    if (_allDeliverablesCache != null) {
      _log.info('Returning cache');
      return _allDeliverablesCache;
    }

    final credentials = await getCredentialsFunc();

    final response = await http.post(
      base_url + '/deliverables',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.info('Something went wrong while fetching all deliverables');
      throw Exception('Something went wrong while fetching all deliverables');
    }

    _allDeliverablesCache = (jsonDecode(response.body) as List)
        .map((json) => Deliverable.fromJson(json))
        .toList();

    return _allDeliverablesCache;
  }

  @override
  Future<List<Deliverable>> get deliverablesFromTomorrowToTillDate async {
    _log.info('Getting deliverables from tomorrow to till date');
    if (_deliverablesFromTomorrowToTillDateCache != null) {
      _log.info('Returning cache');
      return _deliverablesFromTomorrowToTillDateCache;
    }

    final credentials = await getCredentialsFunc();

    final response = await http.post(
      base_url + '/deliverables_from_tomorrow_to_till_date',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.info(
        'Something went wrong while fetching deliverables '
        'from tomorrow to till date',
      );

      throw Exception(
        'Something went wrong while fetching deliverables '
        'from tomorrow to till date',
      );
    }

    _deliverablesFromTomorrowToTillDateCache =
        (jsonDecode(response.body) as List)
            .map((json) => Deliverable.fromJson(json))
            .toList();

    return _deliverablesFromTomorrowToTillDateCache;
  }
}
