class Package {
  final int id, cuisineId, mealId;
  final String name, description, group;

  /// price needs to be changed. So it is not final
  int price;

  Package(this.id, this.name, this.description, this.price, this.cuisineId,
      this.mealId, this.group);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Package && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'Name: $name Description: $description Price: $price';
  }

  factory Package.fromJson(Map<String, dynamic> json) {
    return Package(
      json['id'],
      json['name'],
      json['description'],
      json['price'],
      json['cuisine_id'],
      json['meal_id'],
      json['group'],
    );
  }
}
