import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';

import '../Config.dart';
import 'Model.dart';

abstract class PackageRepository {
  Future<List<Package>> get packages;
}

class StubPackageRepository implements PackageRepository {
  final List<Package> input;

  StubPackageRepository({@required this.input});

  @override
  Future<List<Package>> get packages => Future.value(input);
}

class NetworkPackageRepository implements PackageRepository {
  static final _log = Logger('NetworkPackageRepository');

  List<Package> toList(List<dynamic> packages) {
    return packages.map((json) => Package.fromJson(json)).toList();
  }

  var _cache = List<Package>();

  @override
  Future<List<Package>> get packages async {
    _log.info('Getting packages');

    if (_cache.length > 0) {
      _log.info('Returning cache');
      return _cache;
    }

    _log.info('Sending get request to packages');

    final response = await http.get(base_url +
            '/packages') /*.timeout(
          Duration(seconds: 5),
        )*/
        ;

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode == 200) {
      _cache = (jsonDecode(response.body) as List)
          .map((json) => Package.fromJson(json))
          .toList();
      return _cache;
    }

    _log.severe('Something went wrong ${response.statusCode}');
    throw Exception('Failed to load packages');
  }
}
