import 'package:flutter_secure_storage/flutter_secure_storage.dart';

/// Credentials was already defined in dart:html
class CustomerCredentials {
  String phoneNumber, password, name;
  bool isRegularCustomer = false;

  CustomerCredentials(this.phoneNumber, this.password, this.name,
      [this.isRegularCustomer]);

  @override
  String toString() {
    return '$phoneNumber $password $isRegularCustomer';
  }
}

Future<CustomerCredentials> getCredentials() async {
  final storage = FlutterSecureStorage();

  return CustomerCredentials(
    await storage.read(key: 'phone_number'),
    await storage.read(key: 'password'),
    await storage.read(key: 'name'),
    await storage.read(key: 'is_regular_customer') == 'true',
  );
}

typedef Future<CustomerCredentials> GetCredentialsFunc();

Future deleteCredentials() async {
  final storage = FlutterSecureStorage();

  await storage.delete(key: 'phone_number');
  await storage.delete(key: 'password');
  await storage.delete(key: 'name');
  await storage.delete(key: 'is_regular_customer');
}