import 'dart:convert';
import 'dart:ui';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';

const base_url = 'http://10.0.2.2:5000/api/v1';

const rupeeSymbol = '\u20B9';

const secondaryColor = Color.fromRGBO(39, 43, 75, 1);

class Config {
  final double gst;
  final int needBasedCustomerDelta, deposit;

  Config(this.gst, this.needBasedCustomerDelta, this.deposit);

  factory Config.fromJson(Map<String, dynamic> json) {
    return Config(
      json['GST_percent'],
      (json['need_based_customer_delta'] as double).toInt(),
      (json['deposit'] as double).toInt(),
    );
  }
}

abstract class ConfigRepository {
  Future<Config> get config;
}

class StubConfigRepository implements ConfigRepository {
  final Config input;

  StubConfigRepository(this.input);

  @override
  Future<Config> get config => Future.value(input);
}

class NetworkConfigRepository implements ConfigRepository {
  static final _log = Logger('Network Config Repository');

  Config _cache;

  @override
  Future<Config> get config async {
    _log.info('getting config from internet');
    if (_cache != null) {
      _log.info('returning cache');
      return _cache;
    }

    final response = await http.get(base_url + '/front_end_config');

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      throw Exception('Could not fetch config');
    }

    _cache = Config.fromJson(jsonDecode(response.body));
    return _cache;
  }
}

Config config;

Future<void> initConfig(ConfigRepository configRepository) async {
  if (config == null) {
    config = await configRepository.config;
  }
}
