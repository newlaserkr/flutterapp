import 'dart:convert';
import 'dart:core';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';

import 'Models.dart';

/// Area is not a getter to maintain consistency because locality is dependent
/// on area and hence needs an input
abstract class AreaRepository {
  Future<List<Area>> area();

  Future<List<Locality>> locality(int areaID);

  Future<List<Locality>> get allLocalities;
}

class StubAreaRepository implements AreaRepository {
  final List<Area> input1;
  final List<Locality> input2;

  /// input2 is not required because locality may not be needed like in OfficeAddress
  StubAreaRepository({@required this.input1, this.input2});

  @override
  Future<List<Area>> area() => Future.value(input1);

  @override
  Future<List<Locality>> locality(int areaID) =>
      Future.value(
          input2.where((element) => element.areaID == areaID).toList());

  @override
  Future<List<Locality>> get allLocalities => Future.value(input2);
}

class NetworkAreaRepository implements AreaRepository {
  static final _log = Logger('Network area repository');

  var _areaCache = List<Area>();
  var _localityCache = List<Locality>();

  @override
  Future<List<Area>> area() async {
    _log.info('Getting area');

    if (_areaCache.length > 0) {
      _log.info('returning cache');

      return _areaCache;
    }

    final response = await http.get(base_url + '/delivery_locations')
        /*.timeout(Duration(seconds: 5))*/;

    _log.info('status code: ${response.statusCode}');
    _log.info('response ${response.body}');

    if (response.statusCode == 200) {
      final temp = json.decode(response.body);
      _areaCache =
          temp['areas'].map<Area>((json) => Area.fromJson(json)).toList();
      _localityCache = temp['localities']
          .map<Locality>((json) => Locality.fromJson(json))
          .toList();
      return _areaCache;
    }

    _log.severe('Could not fetch areas');
    throw Exception('Failed to load areas');
  }

  @override
  Future<List<Locality>> locality(int areaID) async {
    return (await allLocalities)
        .where((element) => element.areaID == areaID)
        .toList();
  }

  @override
  Future<List<Locality>> get allLocalities async {
    _log.info('Getting area');

    if (_localityCache.length > 0) {
      _log.info('returning cache');
      return _localityCache;
    }

    final response = await http
        .get(base_url + '/delivery_locations')
    /*.timeout(Duration(seconds: 5))*/;

    _log.info('status code: ${response.statusCode}');
    _log.info('response ${response.body}');

    if (response.statusCode == 200) {
      final temp = json.decode(response.body);
      _areaCache =
          temp['areas'].map<Area>((json) => Area.fromJson(json)).toList();
      _localityCache = temp['localities']
          .map<Locality>((json) => Locality.fromJson(json))
          .toList();

      return _localityCache;
    }

    _log.severe('Could not fetch areas');
    throw Exception('Failed to load localities');
  }
}

abstract class DeliveryLocationsRepository {
  Future<List<DeliveryLocation>> get deliveryLocations;
}

class StubDeliveryLocationsRepository implements DeliveryLocationsRepository {
  final List<DeliveryLocation> input;

  StubDeliveryLocationsRepository(this.input);

  @override
  Future<List<DeliveryLocation>> get deliveryLocations => Future.value(input);
}

class NetworkDeliveryLocationsRepository
    implements DeliveryLocationsRepository {
  static final _log = Logger('Network delivery locations repository');

  final GetCredentialsFunc getCredentialsFunc;

  NetworkDeliveryLocationsRepository(this.getCredentialsFunc);

  List<DeliveryLocation> _cache;

  @override
  Future<List<DeliveryLocation>> get deliveryLocations async {
    _log.info('Get delivery locations');

    if (_cache != null) {
      _log.info('Returning cache');
      return _cache;
    }

    _log.info('Get credentials');
    final credentials = await getCredentialsFunc();

    var response = await http.post(
      base_url + '/home_address',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      throw Exception('could not fetch home address');
    }

    _cache = [];

    if (!_cache.contains(DeliveryLocation.Home) &&
        !(jsonDecode(response.body) as Map).containsKey('enabled')) {
      _cache.add(DeliveryLocation.Home);
    }

    response = await http.post(
      base_url + '/office_address',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      throw Exception('could not fetch office address');
    }

    if (!_cache.contains(DeliveryLocation.Office) &&
        !(jsonDecode(response.body) as Map).containsKey('enabled')) {
      _cache.add(DeliveryLocation.Office);
    }

    if (!_cache.contains(DeliveryLocation.NotRequired))
      _cache.add(DeliveryLocation.NotRequired);

    return _cache;
  }
}
