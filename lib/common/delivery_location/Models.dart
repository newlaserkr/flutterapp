class Area {
  final int id;
  final String name;

  Area(this.id, this.name);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Area && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;

  factory Area.fromJson(Map<String, dynamic> json) {
    return Area(
      json['id'],
      json['area'],
    );
  }
}

class Locality {
  final int id, areaID;
  final String name;

  Locality(this.id, this.name, this.areaID);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Locality && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;

  factory Locality.fromJson(Map<String, dynamic> json) {
    return Locality(
      json['id'],
      json['locality'],
      json['area_id'],
    );
  }
}

/// Delivery Location is Kind of like an enum except I wanted to be able to
/// parse and serialize it. Hence a custom class
class DeliveryLocation {
  static const Home = DeliveryLocation._('Home');
  static const Office = DeliveryLocation._('Office');
  static const NotRequired = DeliveryLocation._('Not required');

  static const values = [Home, Office, NotRequired];

  final String name;

  const DeliveryLocation._(this.name);

  factory DeliveryLocation.fromInt(int value) {
    if (value == null) {
      return null;
    }

    return value == 0 ? DeliveryLocation.Home : DeliveryLocation.Office;
  }

  int toInt() {
    return this == DeliveryLocation.Home ? 0 : 1;
  }

  int toJson() {
    return toInt();
  }

  @override
  String toString() {
    return 'DeliveryLocation{name: $name}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DeliveryLocation &&
          runtimeType == other.runtimeType &&
          name == other.name;

  @override
  int get hashCode => name.hashCode;
}