import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/deliverables/Repositories.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/home/Booking/Repositories.dart';
import 'package:preetibhojan/home/Booking/View.dart';
import 'package:preetibhojan/home/Booking/ViewModel.dart';
import 'package:preetibhojan/util/ui/CardTile.dart';
import 'package:preetibhojan/util/ui/CustomBackground.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:provider/provider.dart';

import 'Booking/Model.dart';
import 'Schedule/MealTypeCard.dart';
import 'Schedule/Models.dart';
import 'Schedule/Title.dart';
import 'Schedule/ViewModel.dart';

class Schedule extends StatefulWidget {
  final ScheduleViewModel viewModel;
  final DeliverableRepository deliverablesRepository;
  final GetCredentialsFunc getCredentialsFunc;

  const Schedule(
    this.viewModel,
    this.getCredentialsFunc,
    this.deliverablesRepository,
  );

  @override
  _ScheduleState createState() => _ScheduleState();
}

class _ScheduleState extends State<Schedule> {
  static final _log = Logger('Schedule');

  // static const NoPackageOrderedForMoreThan3Days = 3;
  static const MinimumDaysAtLeastOnePackageNeedsToBeScheduled = 3;

  bool showCircularProgressIndicator = false;

  PageController controller = PageController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: widget.viewModel,
      child: CustomScaffold(
        showCircularProgressIndicator: showCircularProgressIndicator,
        body: FutureBuilder(
          future: widget.viewModel.fetchData(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            _log.info(snapshot);
            if (snapshot.hasError) {
              _log.severe('Something went wrong: ', snapshot.error.toString());
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                    context: context, builder: (_) => SomethingWentWrong());
              });

              return CustomBackground(
                child: Container(),
              );
            }

            if (snapshot.connectionState == ConnectionState.done) {
              return PageView(
                controller: controller,
                children: widget.viewModel.mealInfoManager.keys.map((meal) {
                  return ListView(
                    children: [
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                          child: Text(
                            widget.viewModel.isRegularCustomer
                                ? 'Re Schedule'
                                : 'Schedule',
                            style: Theme.of(context).textTheme.headline6,
                          ),
                        ),
                      ),
                      Consumer<ScheduleViewModel>(
                        builder: (context, scheduleViewModel, _) {
                          return CardTile(
                            title: TitleWithSwitch(meal),
                            child: Column(
                              children: <Widget>[
                                MealTypeCard(
                                  meal: meal,
                                  enabled: scheduleViewModel
                                      .mealInfoManager[meal].required,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: RaisedButton(
                                    child: Text('Next'),
                                    onPressed: () {
                                      onNextPage(scheduleViewModel, meal);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      )
                    ],
                  );
                }).toList(),
              );
            }

            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
        getCredentialsFunc: widget.getCredentialsFunc,
      ),
    );
  }

  void onNextPage(ScheduleViewModel scheduleViewModel, Meal meal) {
    if (isLastPage()) {
      schedule(scheduleViewModel);
      return;
    }

    final mealInfo = scheduleViewModel.mealInfoManager[meal];

    if (mealInfo.required) {
      if (mealInfo.requiredPackage == null) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Oops'),
            content: Text(
              'Please select package before continuing',
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );

        return;
      }

      if (mealInfo.deliveryInfo.values
              .where((deliveryLocation) => deliveryLocation == null)
              .length ==
          mealInfo.deliveryInfo.length) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Oops'),
            content: Text(
                'Please set at least one delivery location to Home or Office'),
            actions: <Widget>[
              FlatButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );

        return;
      }
    }

    controller.nextPage(
        duration: Duration(milliseconds: 500), curve: Curves.easeInOut);
  }

  bool isLastPage() {
    return controller.page.toInt() ==
        widget.viewModel.mealInfoManager.length - 1;
  }

  void schedule(ScheduleViewModel scheduleViewModel) async {
    _log.info('Finding required meals');

    final requiredMeals = scheduleViewModel.mealInfoManager.internal.entries
        .where((element) => element.value.required)
        .toList();

    _log.info('Required meals: $requiredMeals');

    if (requiredMeals.length == 0) {
      _log.info('Required meals empty');

      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text('Oops'),
          content: Text(
            'Please enable at least one meal by clicking on the switch '
            'at the top right',
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        ),
      );

      return;
    }

    double totalCostOfSchedule = 0;
    final packageInfo = <Meal, PackageInfo>{};
    final daysScheduledFor = <Meal, int>{};

    try {
      requiredMeals.forEach((meal) {
        int daysRequiredCount = 0;

        if (meal.value.requiredPackage == null) {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text('Oops'),
              content: Text('Please select package for ${meal.key.name}'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Okay'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );

          throw Exception();
        }

        meal.value.deliveryInfo.forEach((_, value) {
          if (value != null || value != DeliveryLocation.NotRequired) {
            daysRequiredCount++;
          }
        });

        if (daysRequiredCount == 0) {
          showDialog(
            context: context,
            builder: (context) =>
                AlertDialog(
                  title: Text('Oops'),
                  content: Text(
                    'Please select delivery location for at least one day for '
                        '${meal.key.name}',
                  ),
                  actions: <Widget>[
                    FlatButton(
                      child: Text('Okay'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
          );
          throw Exception();
        }

        final price = meal.value.requiredPackage.price;

        final daysRequired = meal.value.deliveryInfo.entries
            .where((entry) => entry.value != null)
            .map((entry) => entry.key)
            .toList();

        final listOfDaysRequired =
        daysRequired.map((day) => daysRequired.indexOf(day) + 1).toList();

        totalCostOfSchedule += price * daysRequiredCount;
        packageInfo[meal.key] = PackageInfo(price, listOfDaysRequired);
        daysScheduledFor[meal.key] = listOfDaysRequired.length;
      });
    } catch (on) {
      return;
    }

    final maxDaysScheduledFor = daysScheduledFor.values.reduce(max);

    if (maxDaysScheduledFor < MinimumDaysAtLeastOnePackageNeedsToBeScheduled) {
      showDialog(
        context: context,
        builder: (_) =>
            AlertDialog(
              title: Text('Oops'),
              content: Text(
                'At least one meal must be ordered for more than '
                    '$MinimumDaysAtLeastOnePackageNeedsToBeScheduled days',
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('Okay'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
      );

      return;
    }

    final tax = totalCostOfSchedule * config.gst / 100;
    totalCostOfSchedule += tax + config.deposit;

    setState(() {
      showCircularProgressIndicator = true;
    });

    final deliverables =
    await widget.deliverablesRepository.deliverablesFromTomorrowToTillDate;

    /// Deliverables include tax. So no need to add tax to price
    final double creditAvailable = deliverables.isNotEmpty
        ? deliverables
        .map((deliverable) => deliverable.price)
        .reduce((value, element) => value + element)
        : 0;

    setState(() {
      showCircularProgressIndicator = false;
    });

    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) =>
            Booking(
              widget.getCredentialsFunc,
              BookingViewModel(
                StubPaymentInfoRepository(
                  PaymentInfo(packageInfo, creditAvailable),
                ),
                widget.deliverablesRepository,
                DateTime.now(),
              ),
            ),
      ),
    );

    // _log.info('Getting credentials');
    // final credentials = await widget.getCredentialsFunc();
    //
    // _log.info('Posting to schedule');
    //
    // final response = await http.post(
    //   base_url + '/schedule',
    //   headers: {'Content-Type': 'application/json'},
    //   body: jsonEncode(
    //     makeRequest(credentials, requiredMeals),
    //   ),
    // );
    //
    // _log.info('status code: ${response.statusCode}');
    // _log.info('response: ${response.body}');
    //
    // setState(() {
    //   showCircularProgressIndicator = false;
    // });
    //
    // if (response.statusCode == 200) {
    //   _log.info('Schedule success');
    //
    //   showDialog(
    //     context: context,
    //     builder: (_) {
    //       if (!widget.viewModel.isRegularCustomer) {
    //         return AlertDialog(
    //           title: Text('Schedule Successful'),
    //           content: Text(
    //             'Delivery of Scheduled Meals will be effective only after '
    //             'Booking. Do you want to Book Now?',
    //             textAlign: TextAlign.justify,
    //           ),
    //           actions: <Widget>[
    //             FlatButton(
    //               child: Text('Yes'),
    //               onPressed: () {
    //                 Navigator.of(context).pop();
    //                 Navigator.of(context).pushReplacementNamed('/booking');
    //               },
    //             ),
    //             FlatButton(
    //               child: Text('No'),
    //               onPressed: () {
    //                 Navigator.of(context)
    //                     .pushNamedAndRemoveUntil('/home', (route) => false);
    //               },
    //             ),
    //           ],
    //         );
    //       }
    //
    //       return AlertDialog(
    //         title: Text(
    //           'Reschedule Successful',
    //         ),
    //         content: Column(
    //           mainAxisSize: MainAxisSize.min,
    //           crossAxisAlignment: CrossAxisAlignment.start,
    //           children: <Widget>[
    //             Text(
    //               'Do you want to pay the difference now? ',
    //               textAlign: TextAlign.justify,
    //             ),
    //             Padding(
    //               padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
    //               child: Text(
    //                 'If not paid now, Meals will be delivered '
    //                     'according to new schedule from tomorrow till there is balance in your '
    //                     'account',
    //                 textAlign: TextAlign.justify,
    //               ),
    //             ),
    //             Padding(
    //               padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
    //               child: Text(
    //                 'Note: All Cancelled Meals have been deleted. Please Cancel them again',
    //                 style: TextStyle(
    //                   fontWeight: FontWeight.bold,
    //                   color: Colors.lime,
    //                 ),
    //                 textAlign: TextAlign.justify,
    //               ),
    //             ),
    //           ],
    //         ),
    //         actions: <Widget>[
    //           FlatButton(
    //             child: Text('Yes'),
    //             onPressed: () {
    //               Navigator.of(context).pop();
    //               Navigator.of(context).pushReplacementNamed('/booking');
    //             },
    //           ),
    //           FlatButton(
    //             child: Text('No'),
    //             onPressed: () {
    //               Navigator.of(context)
    //                   .pushNamedAndRemoveUntil('/home', (route) => false);
    //             },
    //           ),
    //         ],
    //       );
    //     },
    //   );
    //
    //   return;
    // } else if (response.statusCode == 400) {
    //   final errorCode = jsonDecode(response.body)['error'];
    //
    //   if (errorCode == NoPackageOrderedForMoreThan3Days) {
    //     _log.info('No package ordered for more than 3 days');
    //
    //     showDialog(
    //       context: context,
    //       builder: (_) => AlertDialog(
    //         title: Text('Oops'),
    //         content: Text(
    //           'At least one meal must be ordered for more than 3 days',
    //         ),
    //         actions: <Widget>[
    //           FlatButton(
    //             child: Text('Okay'),
    //             onPressed: () {
    //               Navigator.of(context).pop();
    //             },
    //           )
    //         ],
    //       ),
    //     );
    //
    //     return;
    //   }
    //
    //   _log.severe('Something went wrong. Error code: $errorCode');
    // }
    //
    // SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
    //   showDialog(
    //     context: context,
    //     builder: (_) => SomethingWentWrong(),
    //   );
    // });
  }

  Map<String, dynamic> makeRequest(
    CustomerCredentials credentials,
    List<MapEntry<Meal, MealInfo>> requiredMeals,
  ) {
    final packages = [];
    final result = <String, dynamic>{
      'phone_number': credentials.phoneNumber,
      'password': credentials.password,
    };

    /// find the meals where required is true
    requiredMeals.forEach((mapEntry) {
      final mealInfo = mapEntry.value;
      final deliveryInfo = [];

      mealInfo.deliveryInfo.forEach((day, deliveryLocation) {
        if (deliveryLocation != null &&
            deliveryLocation != DeliveryLocation.NotRequired) {
          deliveryInfo.add({
            'day': mealInfo.deliveryInfo.keys.toList().indexOf(day) + 1,
            'delivery_location': deliveryLocation.toInt(),
          });
        }
      });

      packages.add({
        'id': mealInfo.requiredPackage.id,
        'delivery_info': deliveryInfo,
      });
    });

    result['packages'] = packages;

    return result;
  }
}
