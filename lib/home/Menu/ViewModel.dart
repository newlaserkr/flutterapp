import 'package:flutter/material.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/home/Schedule/Repositories.dart';

import 'Models.dart';
import 'Repository.dart';

class MenuViewModel {
  final MenuRepository menuRepository;
  final PackagesForMenuRepository packageRepository;
  final CuisineRepository cuisineRepository;
  final DeliveryLocationsRepository deliveryLocationsRepository;
  final MealsRepository mealsRepository;

  final GetCredentialsFunc getCredentialsFunc;

  MenuViewModel({
    @required this.menuRepository,
    @required this.packageRepository,
    @required this.cuisineRepository,
    @required this.getCredentialsFunc,
    @required this.deliveryLocationsRepository,
    @required this.mealsRepository,
  });

  List<PackageForMenu> packages;
  List<DeliveryLocation> deliveryLocations;
  List<Menu> menu;
  List<Meal> meals;

  int cuisineID;

  Future<void> fetchData() async {
    if (menu == null) {
      menu = await menuRepository.menu;
    }

    if (meals == null) {
      meals = await mealsRepository.meals;
      meals.sort((a, b) => a.priority.compareTo(b.priority));
    }

    if (cuisineID == null) {
      cuisineID = await cuisineRepository.cuisineID;
    }

    if (packages == null) {
      packages = await packageRepository.packages;

      packages = packages
          .where(
            (element) => element.cuisineId == cuisineID,
          )
          .toList();
    }

    if (deliveryLocations == null) {
      deliveryLocations = await deliveryLocationsRepository.deliveryLocations;
    }
  }
}
