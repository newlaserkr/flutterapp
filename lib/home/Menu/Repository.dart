import 'dart:convert';
import 'dart:core';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';

import 'Models.dart';

abstract class MenuRepository {
  Future<List<Menu>> get menu;
}

class StubMenuRepository implements MenuRepository {
  final List<Menu> input;

  StubMenuRepository({@required this.input});

  @override
  Future<List<Menu>> get menu => Future.value(input);
}

class NetworkMenuRepository implements MenuRepository {
  static final _log = Logger('NetworkMenuRepository');

  final GetCredentialsFunc getCredentialsFunc;

  var _cache = <Menu>[];

  NetworkMenuRepository(this.getCredentialsFunc);

  @override
  Future<List<Menu>> get menu async {
    _log.info('Getting menu');
    if (_cache.length > 0) {
      _log.info('Returning cache');
      return _cache;
    }

    _log.info('Get credentials');
    final credentials = await getCredentialsFunc();

    _log.info('Sending post request for menu');
    final response = await http.post(
      base_url + '/menu',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
      }),
    );

    _log.info('statusCode: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode == 200) {
      _cache = (jsonDecode(response.body) as List)
          .map<Menu>((json) => Menu.fromJson(json))
          .toList();

      return _cache;
    }

    _log.severe('Could not load menu');
    throw Exception('Could not load menu');
  }
}

abstract class PackagesForMenuRepository {
  Future<List<PackageForMenu>> get packages;
  void invalidateCache();
}

class StubPackagesForMenuRepository implements PackagesForMenuRepository {
  final List<PackageForMenu> input;

  StubPackagesForMenuRepository(this.input);

  @override
  Future<List<PackageForMenu>> get packages => Future.value(input);

  @override
  void invalidateCache() {}
}

class NetworkPackagesForMenuRepository implements PackagesForMenuRepository {
  List<PackageForMenu> _cache;

  static final _log = Logger('Network packages for menu repository');

  @override
  Future<List<PackageForMenu>> get packages async {
    _log.info('Getting packages');

    if (_cache != null) {
      _log.info('Returning cache');
      return _cache;
    }

    _log.info('sending post request to packages for menu');

    final response = await http.get(base_url + '/packages_for_menu');

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode == 200) {
      _cache = (jsonDecode(response.body) as List)
          .map((json) => PackageForMenu.fromJson(json))
          .toList();
      return _cache;
    }

    _log.severe('Something went wrong ${response.statusCode}');
    throw Exception('Failed to load packages');
  }

  @override
  void invalidateCache() {
    _cache = null;
  }
}
