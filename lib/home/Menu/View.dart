import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/home/Menu/MenuDetails.dart';
import 'package:preetibhojan/home/Menu/ViewModel.dart';
import 'package:preetibhojan/util/ui/CardTile.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

class MenuScreen extends StatefulWidget {
  final MenuViewModel menuViewModel;
  final GetCredentialsFunc getCredentialsFunc;

  const MenuScreen(
    this.menuViewModel,
    this.getCredentialsFunc,
  );

  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  static final _log = Logger('Menu screen');

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: widget.menuViewModel.fetchData(),
      builder: (context, snapshot) {
        _log.info(snapshot);
        if (snapshot.hasError) {
          _log.severe('something went wrong: ', snapshot.error);
          SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
            showDialog(
              context: context,
              builder: (_) => SomethingWentWrong(),
            );
          });

          return CustomScaffold(
            body: Container(),
            getCredentialsFunc: widget.getCredentialsFunc,
          );
        }

        if (snapshot.connectionState == ConnectionState.done &&
            !snapshot.hasError) {
          final currentDate = DateTime.now();

          var todaysMenu = widget.menuViewModel.menu
              .where((menu) =>
                  menu != null &&
                  DateFormat.yMd().format(currentDate) ==
                      DateFormat.yMd().format(menu.date))
              .toList();

          var nextWorkingDaysMenu = widget.menuViewModel.menu
              .where((menu) =>
                  menu != null &&
                  DateFormat.yMd().format(currentDate) !=
                      DateFormat.yMd().format(menu.date))
              .toList();

          final maxDeliveryTimeInSeconds = widget.menuViewModel.meals
              .map((meal) => meal.deliveryTime.inSeconds)
              .reduce(max);

          final currentTimeInSeconds = currentDate.hour * 60 * 60 +
              currentDate.minute * 60 +
              currentDate.second;

          final shouldDisplayTodaysMenu =
              currentTimeInSeconds < maxDeliveryTimeInSeconds &&
                  todaysMenu.length > 0;

          final data =
              shouldDisplayTodaysMenu ? todaysMenu : nextWorkingDaysMenu;

          if (data.length == 0) {
            SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
              showDialog(
                context: context,
                builder: (_) => AlertDialog(
                  title: Text('Oops!'),
                  content: Text('No Menu Found'),
                ),
              );
            });

            return CustomScaffold(
              body: Container(),
              getCredentialsFunc: widget.getCredentialsFunc,
            );
          }

          return CustomScaffold(
            /// duplicated in MenuDetails.dart
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.of(context).pushNamed(
                  '/cart',
                );
              },
              child: Icon(Icons.shopping_cart),
              foregroundColor: secondaryColor,
            ),
            getCredentialsFunc: widget.getCredentialsFunc,
            body: ListView(
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: Text(
                      shouldDisplayTodaysMenu
                          ? 'Today\'s Menu'
                          : DateFormat.yMd().format(data[0].date) ==
                                  DateFormat.yMd().format(
                                    currentDate.add(
                                      Duration(days: 1),
                                    ),
                                  )
                              ? 'Tomorrow\'s Menu'
                              : '${DateFormat('EEEE').format(data[0].date)}\'s Menu',
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                ...widget.menuViewModel.meals.map<Widget>((meal) {
                  final menu = data.firstWhere(
                    (element) => element.mealID == meal.id,
                    orElse: () => null,
                  );

                  final orderBefore = menu == null
                      ? null
                      : menu.date
                          .add(meal.deliveryTime)
                          .subtract(meal.orderBefore);

                  return InkWell(
                    onTap: () {
                      if (menu == null) {
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: Text('Oops'),
                            content: Text(
                                '${meal.name} is not currently served. Will start soon.'),
                            actions: <Widget>[
                              FlatButton(
                                child: Text('Okay'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          ),
                        );

                        return;
                      }

                      if (orderBefore.isBefore(DateTime.now())) {
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: Text('Oops'),
                            content: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  '${meal.name} must be ordered from Previous Day '
                                  '8:00 PM to ${DateFormat('jm').format(orderBefore)}.',
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(0, 8, 0, 0),
                                  child: Wrap(
                                    children: [
                                      Text(
                                        'Please WhatsApp your requirement on ',
                                      ),
                                      Text(
                                        '7030700477',
                                        style: TextStyle(
                                          color: Colors.lime,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Text(
                                        ' to check availability',
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            actions: <Widget>[
                              FlatButton(
                                child: Text('Okay'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          ),
                        );

                        return;
                      }

                      if (meal.cannotOrderOn == menu.date.weekday) {
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: Text('Oops'),
                            content: Text(
                              '${meal.name} cannot be ordered today',
                            ),
                            actions: <Widget>[
                              FlatButton(
                                child: Text('Okay'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          ),
                        );

                        return;
                      }

                      _log.info(widget.menuViewModel);
                      assert(widget.menuViewModel.packages != null);

                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return MenuDetails(
                              imageUrl: meal.imageUrl,
                              meal: meal,
                              packages: widget.menuViewModel.packages
                                  .where((package) =>
                                      package.mealId == menu.mealID)
                                  .toList(),
                              getCredentialsFunc: widget.getCredentialsFunc,
                              deliveryLocations:
                                  widget.menuViewModel.deliveryLocations,
                            );
                          },
                        ),
                      );
                    },
                    child: Hero(
                      tag: meal.name,
                      child: CardTile(
                        padding: const EdgeInsets.all(2),
                        title: Column(
                          children: <Widget>[
                            Divider(),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 0, 15),
                              child: Text(
                                '${meal.name}',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline6
                                    .copyWith(fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Flexible(
                                  fit: FlexFit.tight,
                                  flex: 1,
                                  child: CachedNetworkImage(
                                    height: 100,
                                    width: 130,
                                    imageUrl: meal.imageUrl,
                                    progressIndicatorBuilder:
                                        (context, url, downloadProgress) =>
                                            Center(
                                      child: CircularProgressIndicator(
                                        value: downloadProgress.progress,
                                      ),
                                    ),
                                    errorWidget: (context, url, error) =>
                                        Container(
                                      height: 100,
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                                Flexible(
                                  fit: FlexFit.loose,
                                  flex: 1,
                                  child: DefaultTextStyle(
                                    style: TextStyle(
                                      fontSize: 16,
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(2),
                                          child: Text(menu == null
                                              ? ''
                                              : menu.sabji ?? ''),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(2),
                                          child: Text(menu == null
                                              ? ''
                                              : menu.daal ?? ''),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(2),
                                          child: Text(menu == null
                                              ? ''
                                              : menu.rice ?? ''),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }).toList(),
              ],
            ),
          );
        }

        return CustomScaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
          getCredentialsFunc: widget.getCredentialsFunc,
        );
      },
    );
  }
}
