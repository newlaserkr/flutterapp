class Menu {
  final String sabji, daal, rice;
  final int mealID;
  final DateTime date;

  Menu(
    this.mealID,
    this.sabji,
    this.daal,
    this.rice,
    this.date,
  );

  factory Menu.fromJson(Map<String, dynamic> json) {
    return Menu(
      json['meal_id'],
      json['sabji'],
      json['daal'],
      json['rice'],
      DateTime.parse(json['date']),
    );
  }
}

class PackageForMenu {
  final int id, cuisineId, mealId, quantity;
  final String name, description, group;

  /// price needs to be changed. So it is not final
  int price;

  PackageForMenu(
    this.id,
    this.name,
    this.description,
    this.price,
    this.cuisineId,
    this.mealId,
    this.group,
    this.quantity,
  );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PackageForMenu &&
          runtimeType == other.runtimeType &&
          id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'Name: $name Description: $description Price: $price';
  }

  factory PackageForMenu.fromJson(Map<String, dynamic> json) {
    return PackageForMenu(
      json['id'],
      json['name'],
      json['description'],
      json['price'],
      json['cuisine_id'],
      json['meal_id'],
      json['group'],
      json['quantity'],
    );
  }
}
