import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/QuantityPicker.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:provider/provider.dart';

import '../Cart/Models.dart';
import 'Models.dart';

class MenuDetails extends StatefulWidget {
  final GetCredentialsFunc getCredentialsFunc;
  final String imageUrl;
  final Meal meal;
  final List<PackageForMenu> packages;
  final List<DeliveryLocation> deliveryLocations;

  const MenuDetails({
    @required this.getCredentialsFunc,
    @required this.meal,
    @required this.packages,
    @required this.imageUrl,
    @required this.deliveryLocations,
  });

  @override
  _MenuDetailsState createState() => _MenuDetailsState();
}

class _MenuDetailsState extends State<MenuDetails> {
  List<CartItem> cartItems = [];

  bool showCircularProgressIndicator = false;

  Future<void> future = Future.value();

  @override
  void initState() {
    super.initState();

    widget.packages.sort((a, b) => a.name.compareTo(b.name));
  }

  void initCartItems(Cart cart) {
    if (cart.items[widget.meal] == null || didInitializeCart) return;

    cartItems = cart.items[widget.meal]
        .map(
          (cartItem) => CartItem.clone(cartItem),
    )
        .toList();

    didInitializeCart = true;
  }

  static final _log = Logger('Menu Details');

  bool didInitializeCart = false;

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<Cart>(context);

    if (!cart.didFetchData) {
      future = cart.loadData();
    }

    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        _log.info(snapshot);
        if (snapshot.hasError) {
          _log.severe('Could not fetch cart');
          SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
            showDialog(
              context: context,
              builder: (_) => SomethingWentWrong(),
            );
          });

          return CustomScaffold(
            getCredentialsFunc: widget.getCredentialsFunc,
            body: Container(),
          );
        }

        if (snapshot.connectionState == ConnectionState.done) {
          initCartItems(cart);
          return CustomScaffold(
            showCircularProgressIndicator: showCircularProgressIndicator,

            /// duplicated in View.dart
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.of(context).pushNamed(
                  '/cart',
                );
              },
              child: Icon(Icons.shopping_cart),
              foregroundColor: secondaryColor,
            ),
            getCredentialsFunc: widget.getCredentialsFunc,
            body: ListView(
              children: <Widget>[
                Hero(
                  tag: widget.meal,
                  child: CachedNetworkImage(
                    imageUrl: widget.imageUrl,
                    progressIndicatorBuilder:
                        (context, url, downloadProgress) => Center(
                      child: CircularProgressIndicator(
                        value: downloadProgress.progress,
                      ),
                    ),
                    errorWidget: (context, url, error) => Container(
                      height: 100,
                      color: Colors.black,
                    ),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Package',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GroupPackages(
                    packages: widget.packages,
                    cartItems: cartItems,
                    onCartItemsChanged: (bool added, CartItem cartItem) {
                      setState(() {
                        if (added) {
                          cartItems.add(cartItem);
                        } else {
                          cartItems.remove(cartItem);
                        }
                      });
                    },
                  ),
                ),
                Column(
                  children: <Widget>[
                    RaisedButton(
                      child: Text('Save Changes'),
                      onPressed: () async {
                        setState(() {
                          showCircularProgressIndicator = true;
                        });

                        try {
                          if (cartItems.length > 0) {
                            cart.items[widget.meal] = cartItems;
                          } else {
                            cart.items.remove(widget.meal);
                          }

                          await cart.saveChanges();

                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text('Changes Saved'),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text('Okay'),
                                  onPressed: () {
                                    Navigator.of(context)
                                        .popUntil((route) => route.isFirst);
                                  },
                                ),
                              ],
                            ),
                          );
                        } catch (exception) {
                          showDialog(
                            context: context,
                            builder: (_) => SomethingWentWrong(),
                          );
                        } finally {
                          setState(() {
                            showCircularProgressIndicator = false;
                          });
                        }
                      },
                    )
                  ],
                ),
              ],
            ),
          );
        }

        return CustomScaffold(
          body: Center(child: CircularProgressIndicator()),
          getCredentialsFunc: widget.getCredentialsFunc,
        );
      },
    );
  }
}

typedef OnCartItemsChanged(bool added, CartItem cartItem);

class GroupPackages extends StatefulWidget {
  /// List of all packages
  final List<PackageForMenu> packages;

  /// Current required package. Needs to be maintained by parent.
  /// Used as group value for radio buttons
  final List<CartItem> cartItems;

  /// callback when cart item is selected
  final OnCartItemsChanged onCartItemsChanged;

  GroupPackages({
    Key key,
    @required this.packages,
    @required this.cartItems,
    @required this.onCartItemsChanged,
  }) : super(key: key);

  @override
  _GroupPackagesState createState() => _GroupPackagesState();
}

class _GroupPackagesState extends State<GroupPackages> {
  List<String> groups;
  List<bool> isExpanded = [];

  @override
  void initState() {
    super.initState();
    groups = widget.packages.map((package) => package.group).toSet().toList();
    groups.forEach((group) {
      isExpanded.add(
        widget.cartItems.firstWhere(
                (cartItem) => cartItem.package.group == group,
            orElse: () => null) !=
            null,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ExpansionPanelList(
          expansionCallback: (index, isPanelExpanded) {
            setState(() {
              isExpanded[index] = !isPanelExpanded;
            });
          },
          expandedHeaderPadding: const EdgeInsets.all(8),
          children: groups
              .map(
                (group) => ExpansionPanel(
              isExpanded: isExpanded[groups.indexOf(group)],
              canTapOnHeader: true,
              headerBuilder: (context, isExpanded) {
                return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          group,
                          style: Theme
                              .of(context)
                              .textTheme
                              .headline6,
                        ),
                      ),
                    ]);
              },
              body: Column(
                children: widget.packages
                    .where((package) => package.group == group)
                    .map((package) {
                  final displayGST = config.gst > 0;

                  final cartItem = widget.cartItems.length == 0
                      ? null
                      : widget.cartItems.firstWhere(
                        (item) => item.package == package,
                    orElse: () => null,
                  );
                  final onCartItemChanged =
                      (selected) =>
                      widget.onCartItemsChanged(
                        selected,
                        cartItem ?? CartItem(package, 1),
                      );

                  var quantity = (cartItem != null ? cartItem.quantity : 1);

                  /// If current quantity available for package is less than
                  /// the quantity in cart, use the available quantity
                  quantity = min(quantity, package.quantity);

                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                      onTap: () {
                        if (cartItem == null) {
                          onCartItemChanged(true);
                        }
                      },
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Checkbox(
                                onChanged: onCartItemChanged,
                                value: cartItem != null,
                              ),
                              Text(
                                package.name,
                                style: TextStyle(fontSize: 16),
                              ),
                              Spacer(),
                              Container(
                                margin:
                                const EdgeInsets.fromLTRB(10, 0, 30, 0),
                                padding: const EdgeInsets.all(8),
                                child: Visibility(
                                  visible: cartItem != null,
                                  child: QuantityPicker(
                                    onIncreaseQuantity: () {
                                      if (cartItem.quantity <
                                          package.quantity) {
                                        setState(() {
                                          cartItem.quantity++;
                                        });
                                      }
                                    },
                                    onDecreaseQuantity: () {
                                      setState(() {
                                        if (cartItem.quantity > 1) {
                                          cartItem.quantity--;
                                        } else {
                                          widget.cartItems.remove(cartItem);
                                        }
                                      });
                                    },
                                    value: quantity,
                                    borderColor: Colors.white,
                                    textColor: Colors.white,
                                  ),
                                ),
                              ),
                              Text(
                                rupeeSymbol +
                                    (package.price * quantity).toString() +
                                    (displayGST ? '+ GST' : ''),
                                style: TextStyle(fontSize: 16),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(50, 0, 0, 0),
                            child: Align(
                              child: Text(package.description),
                              alignment: Alignment.centerLeft,
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
          )
              .toList(),
        ),
      ],
    );
  }
}
