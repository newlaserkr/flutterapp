import 'package:flutter/material.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/packages/Model.dart';

class GroupPackages extends StatefulWidget {
  /// List of all packages
  final List<Package> packages;

  /// Callback for parent to react to user changing the package.
  /// Also used to disable expansion panel when onPackageChanged in null
  final ValueChanged<Package> onPackageChanged;

  /// Current required package. Needs to be maintained by parent.
  /// Used as group value for radio buttons
  final Package requiredPackage;

  GroupPackages({
    Key key,
    @required this.packages,
    @required this.onPackageChanged,
    @required this.requiredPackage,
  }) : super(key: key);

  @override
  _GroupPackagesState createState() => _GroupPackagesState();
}

class _GroupPackagesState extends State<GroupPackages> {
  List<String> groups;

  @override
  void initState() {
    super.initState();
    groups = widget.packages.map((package) => package.group).toSet().toList();
  }

  @override
  Widget build(BuildContext context) {
    return ExpansionPanelList.radio(
      initialOpenPanelValue: widget.requiredPackage == null
          ? groups[0]
          : groups[groups.indexOf(
              groups
                  .firstWhere((group) => group == widget.requiredPackage.group),
            )],
      expandedHeaderPadding: const EdgeInsets.all(8),
      children: widget.onPackageChanged == null
          ? []
          : groups
              .map(
                (group) => ExpansionPanelRadio(
                  value: group,
                  canTapOnHeader: true,
                  headerBuilder: (context, isExpanded) {
                    return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              group,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                          ),
                        ]);
                  },
                  body: Column(
                    children: widget.packages
                        .where((package) => package.group == group)
                        .map((package) {
                      final displayGST = config.gst > 0;

                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: InkWell(
                          onTap: () {
                            if (widget.onPackageChanged != null) {
                              widget.onPackageChanged(package);
                            }
                          },
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Radio<Package>(
                                    onChanged: widget.onPackageChanged,
                                    groupValue: widget.requiredPackage,
                                    value: package,
                                  ),
                                  Text(
                                    package.name,
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  Expanded(
                                    child: Align(
                                      alignment: Alignment.topRight,
                                      child: Text(
                                        rupeeSymbol +
                                            package.price.toString() +
                                            ((displayGST) ? '+ GST' : ''),
                                        style: TextStyle(fontSize: 16),
                                      ),
                                    ),
                                  ),
                                ],
                                mainAxisSize: MainAxisSize.max,
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(50, 0, 0, 0),
                                child: Align(
                                  child: Text(package.description),
                                  alignment: Alignment.centerLeft,
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }).toList(),
                  ),
                ),
              )
              .toList(),
    );
  }
}
