import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';

final _log = Logger('Generate Transaction ID');

Future<String> generateTransactionIDFromBackend(
    GetCredentialsFunc getCredentialsFunc) async {
  _log.info('getting credentials');
  final credentials = await getCredentialsFunc();

  _log.info('sending post request to transaction id');

  final response = await http.post(
    base_url + '/transaction_id',
    headers: {'Content-Type': 'application/json'},
    body: jsonEncode({
      'phone_number': credentials.phoneNumber,
      'password': credentials.password,
    }),
  );

  _log.info('status code: ${response.statusCode}');
  _log.info('response: ${response.body}');

  if (response.statusCode != 200) {
    _log.severe('Could not generate transaction id');
    throw Exception('Could not generate transaction id');
  }

  return jsonDecode(response.body)['result'];
}
