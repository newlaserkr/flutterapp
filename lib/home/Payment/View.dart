import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:upi_india/upi_india.dart';

enum PaymentErrorCode { FAILED, SUCCESS, PENDING }

class PaymentResult {
  final PaymentErrorCode errorCode;
  final String transactionID;

  PaymentResult(this.errorCode, [this.transactionID]);
}

typedef Future<String> GenerateTransactionID();

class Payment extends StatefulWidget {
  final GetCredentialsFunc getCredentialsFunc;
  final GenerateTransactionID generateTransactionID;

  const Payment(this.getCredentialsFunc, this.generateTransactionID);

  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  double amount;

  UpiIndia _upiIndia = UpiIndia();
  List<UpiApp> upiApps;
  Future<List<UpiApp>> future;

  bool hasDialogShown = false;

  static final _log = Logger('Payment');

  bool showCircularProgressIndicator = false;

  @override
  void initState() {
    super.initState();
    future = _upiIndia.getAllUpiApps();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    amount = ModalRoute.of(context).settings.arguments;
    assert(amount != null, 'Amount should not be null');

    _log.info('Amount: $amount');
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      showCircularProgressIndicator: showCircularProgressIndicator,
      getCredentialsFunc: widget.getCredentialsFunc,
      body: FutureBuilder(
        future: future,
        builder: (context, snapshot) {
          _log.info('snapshot');

          if (snapshot.hasError) {
            _log.severe('Something went wrong while fetching apps');

            SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
              showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  title: Text('Oops'),
                  content: Text('Could not fetch UPI apps'),
                  actions: [
                    FlatButton(
                      child: Text('Okay'),
                      onPressed: () {
                        Navigator.of(context).pop();
                        Navigator.of(context)
                            .pop(PaymentResult(PaymentErrorCode.FAILED));
                      },
                    ),
                  ],
                ),
              );
            });

            return Container();
          }

          if (snapshot.hasData) {
            _log.info(snapshot.data);

            if (snapshot.data.length == 0 && !hasDialogShown) {
              hasDialogShown = true;
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (_) => AlertDialog(
                    title: Text('Oops'),
                    content: Text('No UPI apps'),
                    actions: [
                      FlatButton(
                        child: Text('Okay'),
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context)
                              .pop(PaymentResult(PaymentErrorCode.FAILED));
                        },
                      ),
                    ],
                  ),
                );
              });

              return Container();
            }

            return ListView(
              children: [
                Container(
                  margin: const EdgeInsets.all(20),
                  alignment: Alignment.center,
                  child: Text(
                    'Select app to pay',
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                ),
                Wrap(
                  children: snapshot.data
                      .map<Widget>(
                        (app) => PaymentApp(
                      app: app,
                      onTap: () async {
                            setState(() {
                              showCircularProgressIndicator = true;
                            });

                            final transactionID =
                                await widget.generateTransactionID();
                            final response = await initiateTransaction(
                                app.app, transactionID);

                            setState(() {
                              showCircularProgressIndicator = false;
                            });

                            if (response.error != null) {
                              String text;
                              switch (response.error) {
                                case UpiError.APP_NOT_INSTALLED:
                                  text = '${app.app} not installed on device';
                                  break;
                                case UpiError.INVALID_PARAMETERS:
                                  text =
                                      '${app.app} cannot handle the transaction';
                                  break;
                            case UpiError.NULL_RESPONSE:
                              text =
                              '${app.app} did not returned any response';
                              break;
                            case UpiError.USER_CANCELLED:
                              text = 'You cancelled the transaction';
                              break;
                          }

                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text('Oops'),
                              content: Text(text),
                              actions: [
                                FlatButton(
                                  child: Text('Okay'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop(
                                      PaymentResult(
                                        PaymentErrorCode.FAILED,
                                      ),
                                    );
                                  },
                                ),
                              ],
                            ),
                          );

                          return;
                        }

                        if (response.status == UpiPaymentStatus.SUCCESS) {
                          showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text('Payment Success'),
                              actions: [
                                FlatButton(
                                  child: Text('Okay'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop(
                                      PaymentResult(
                                        PaymentErrorCode.SUCCESS,
                                        transactionID,
                                      ),
                                    );
                                  },
                                ),
                              ],
                            ),
                          );

                          return;
                        }

                        if (response.status == UpiPaymentStatus.FAILURE) {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text('Oops'),
                              content: Text('Payment Failed'),
                              actions: [
                                FlatButton(
                                  child: Text('Okay'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop(PaymentResult(
                                        PaymentErrorCode.FAILED));
                                  },
                                ),
                              ],
                            ),
                          );

                          return;
                        }

                        showDialog(
                          context: context,
                          builder: (_) => AlertDialog(
                            title: Text('Oops'),
                            content: Text(
                              'Payment request was submitted but could not be confirmed. '
                                  'Please try again. If money is debited from your account, get in '
                                  'touch with Customer Care',
                            ),
                            actions: [
                              FlatButton(
                                child: Text('Okay'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                  Navigator.of(context)
                                      .pop(PaymentResult(
                                      PaymentErrorCode.FAILED));
                                },
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  )
                      .toList(),
                ),
              ],
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  Future<UpiResponse> initiateTransaction(String app,
      String transactionID) async {
    return _upiIndia.startTransaction(
      app: app,
      receiverUpiId: '7030700477@upi',
      receiverName: 'Preeti Bhojan',
      transactionRefId: transactionID,
      amount: 1.00,
      transactionNote: 'Tiffin ordered on Preeti Bhojan',
    );
  }
}

class PaymentApp extends StatelessWidget {
  final UpiApp app;
  final VoidCallback onTap;

  const PaymentApp({
    Key key,
    @required this.app,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(12),
            child: Image.memory(
              app.icon,
              height: 60,
              width: 60,
            ),
          ),
          Text(app.name)
        ],
      ),
    );
  }
}
