import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/cancel/Model.dart';
import 'package:preetibhojan/home/Exceptions.dart';
import 'package:preetibhojan/home/Resume/ViewModel.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

class Resume extends StatefulWidget {
  final GetCredentialsFunc getCredentialsFunc;
  final ResumeViewModel resumeViewModel;

  const Resume(
    this.getCredentialsFunc,
    this.resumeViewModel,
  );

  @override
  _ResumeState createState() => _ResumeState();
}

class _ResumeState extends State<Resume> {
  Future<List<DisplayTile>> future;
  bool showCircularProgressIndicator = false;

  static final _log = Logger('Resume');

  @override
  void initState() {
    super.initState();
    future = widget.resumeViewModel.displayTiles;
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      showCircularProgressIndicator: showCircularProgressIndicator,
      getCredentialsFunc: widget.getCredentialsFunc,
      body: FutureBuilder(
        future: future,
        builder: (context, snapshot) {
          _log.info(snapshot);

          if (snapshot.hasData) {
            if (snapshot.data.length == 0) {
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (_) => AlertDialog(
                    title: Text('Oops'),
                    content: Text(
                      'No Meals found to Resume. This means either no Meals '
                      'are Cancelled or All Meals Cancelled are Resumed',
                      textAlign: TextAlign.justify,
                    ),
                    actions: <Widget>[
                      FlatButton(
                        child: Text('Okay'),
                        onPressed: () {
                          Navigator.of(context)
                              .popUntil((route) => route.isFirst);
                        },
                      ),
                    ],
                  ),
                );
              });

              return Container();
            }

            final displayTiles = snapshot.data as List<DisplayTile>;

            for (final tile in displayTiles) {
              tile.addListener(() {
                this.setState(() {});
              });
            }

            return ListView(
              children: [
                ...displayTiles
                    .map(
                      (displayTile) => Padding(
                        padding: const EdgeInsets.all(15),
                        child: Column(
                          children: <Widget>[
                            Text(
                              DateFormat.yMMMd().format(displayTile.date),
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            Column(
                              children: displayTile.meals
                                  .map(
                                    (meal) => Row(
                                      children: <Widget>[
                                        Text(
                                          meal.name,
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        Spacer(),
                                        Checkbox(
                                          value: meal.enabled,
                                          onChanged: (bool value) =>
                                              meal.enabled = value,
                                        )
                                      ],
                                    ),
                                  )
                                  .toList(),
                            ),
                          ],
                        ),
                      ),
                    )
                    .toList(),
                Column(
                  children: <Widget>[
                    RaisedButton(
                      child: Text('Resume'),
                      onPressed: onResumePressed,
                    ),
                  ],
                ),
              ],
            );
          }

          if (snapshot.hasError) {
            if (snapshot.error is CouldNotFetchPaymentInfo) {
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: Text('Oops'),
                    content: Text(
                      'Meal\'s Schedule Not Found. Please First Schedule your Meal',
                    ),
                    actions: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context)
                              .pushReplacementNamed('/schedule');
                        },
                        child: Text('Take me to schedule'),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context)
                              .popUntil((route) => route.isFirst);
                        },
                        child: Text('Go back home'),
                      ),
                    ],
                  ),
                );
              });
            } else {
              _log.severe('Something went wrong', snapshot.error);
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (_) => SomethingWentWrong(),
                );
              });
            }

            return Container();
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  void onResumePressed() async {
    final meals = <DisplayMeal>[];

    _log.info('Creating list of meals to resume');

    for (final tile in widget.resumeViewModel.cache) {
      meals.addAll(tile.meals.where((meal) => meal.enabled));
    }

    if (meals.length == 0) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops'),
          content:
              Text('Please select a Meal to Cancel before pressing Cancel'),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );

      return;
    }

    setState(() {
      showCircularProgressIndicator = true;
    });

    _log.info('fetching credentials');

    final credentials = await widget.getCredentialsFunc();

    final response = await http.post(
      base_url + '/resume',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
        'meals': meals,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    setState(() {
      showCircularProgressIndicator = false;
    });

    if (response.statusCode != 200) {
      showDialog(
        context: context,
        builder: (_) => SomethingWentWrong(),
      );

      return;
    }

    final resumedMeals = jsonDecode(response.body)['success'] as List;

    if (resumedMeals.length == meals.length) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Success'),
          content: Text('Resume Successful'),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.popUntil(context, ModalRoute.withName('/home'));
              },
            )
          ],
        ),
      );

      return;
    }

    /// The only meals that won't be successfully cancelled would be today's
    /// meals. This will happen when deliverables are set after cancellation
    /// is loaded.
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Oops'),
        content: Text(
          'Today\'s meals cannot be resumed. Other meals have been resumed. '
          'To resume today\'s meal, go to cart and book from there',
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.popUntil(context, ModalRoute.withName('/home'));
            },
          )
        ],
      ),
    );
  }
}
