import 'package:intl/intl.dart';
import 'package:preetibhojan/common/cancel/Model.dart';
import 'package:preetibhojan/common/cancel/Repository.dart';
import 'package:preetibhojan/common/deliverables/Repositories.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';

class ResumeViewModel {
  final CancelledMealsRepository cancelledMealsRepository;
  final DeliverableRepository deliverableRepository;
  final MealsRepository mealsRepository;
  DateTime now;

  ResumeViewModel(
    this.cancelledMealsRepository,
    this.mealsRepository,
    this.deliverableRepository,
    this.now,
  ) {
    if (now == null) {
      now = DateTime.now();
    }
  }

  List<DisplayTile> cache;

  Future<List<DisplayTile>> get displayTiles async {
    if (cache != null) {
      return cache;
    }

    var cancelledMeals = await cancelledMealsRepository.cancelledMeals;
    final allMeals = await mealsRepository.meals;
    final deliverables =
        await deliverableRepository.deliverablesForNextWorkingDay;

    final tomorrow = now.add(Duration(days: 1));
    final isDeliverablesSetForTomorrow = deliverables.firstWhere(
            (deliverable) =>
                DateFormat('yyyy/MM/dd').format(deliverable.date) ==
                DateFormat('yyyy/MM/dd').format(tomorrow),
            orElse: () => null) !=
        null;

    final temp = <DateTime, List<Meal>>{};

    if (isDeliverablesSetForTomorrow) {
      cancelledMeals = cancelledMeals
          .where((meal) =>
              DateFormat('yyyy/MM/dd').format(meal.date) !=
              DateFormat('yyyy/MM/dd').format(tomorrow))
          .toList();
    }

    for (final cancelledMeal in cancelledMeals) {
      final meal = allMeals.firstWhere((meal) => meal.id == cancelledMeal.id);

      if (temp.containsKey(cancelledMeal.date)) {
        temp[cancelledMeal.date].add(meal);
      } else {
        temp[cancelledMeal.date] = [meal];
      }
    }

    final _displayTiles = <DisplayTile>[];

    temp.forEach((date, meals) {
      meals.sort((a, b) => a.priority.compareTo(b.priority));

      final displayMeals =
      meals.map((meal) => DisplayMeal(meal.name, date, meal.id)).toList();

      _displayTiles.add(DisplayTile(date, displayMeals));
    });

    cache = _displayTiles;
    return cache;
  }
}
