import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/home/Exceptions.dart';

import 'Model.dart';

abstract class CancellationInfoRepository {
  Future<CancellationInfo> get info;
}

class StubCancellationInfoRepository implements CancellationInfoRepository {
  final CancellationInfo input;

  StubCancellationInfoRepository(this.input);

  @override
  Future<CancellationInfo> get info => Future.value(input);
}

class NetworkCancellationInfoRepository implements CancellationInfoRepository {
  static final _log = Logger('Network Cancellation Info Repository');
  final GetCredentialsFunc getCredentialsFunc;

  CancellationInfo _cache;

  static const NotScheduled = 2;

  NetworkCancellationInfoRepository(this.getCredentialsFunc);

  @override
  Future<CancellationInfo> get info async {
    _log.info('Get meals');

    if (_cache != null) {
      _log.info('Returning cache');
      return _cache;
    }

    _log.info('Getting credentials');
    final credentials = await getCredentialsFunc();

    _log.info('Sending post request for meals');

    final response = await http.post(
      base_url + '/meals',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode(
        {
          'phone_number': credentials.phoneNumber,
          'password': credentials.password,
        },
      ),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.severe('Could not fetch all meals');
      throw Exception('Could not fetch all meals');
    }

    final meals = (jsonDecode(response.body) as List)
        .map((json) => Meal.fromJson(json))
        .toList();

    final paymentInfoResponse = await http.post(
      base_url + '/payment_info',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
      }),
    );

    _log.info('status code: ${paymentInfoResponse.statusCode}');
    _log.info('response: ${paymentInfoResponse.body}');

    if (paymentInfoResponse.statusCode != 200) {
      final error = jsonDecode(response.body)['error'];

      if (error == NotScheduled) {
        return CancellationInfo([], null);
      }
      _log.severe('Could not fetch payment info');
      throw CouldNotFetchPaymentInfo();
    }

    final json = jsonDecode(paymentInfoResponse.body) as Map<String, dynamic>;

    return CancellationInfo.fromJson(json, meals);
  }
}