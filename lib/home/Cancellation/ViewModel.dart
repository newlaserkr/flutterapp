import 'package:intl/intl.dart';
import 'package:preetibhojan/common/cancel/Model.dart';
import 'package:preetibhojan/common/cancel/Repository.dart';
import 'package:preetibhojan/common/deliverables/Repositories.dart';

import 'Repository.dart';

class CancellationViewModel {
  final CancellationInfoRepository cancellationInfoRepository;
  final CancelledMealsRepository cancelledMealsRepository;
  final DeliverableRepository deliverableRepository;

  DateTime now;

  CancellationViewModel(
    this.cancellationInfoRepository,
    this.cancelledMealsRepository,
    this.deliverableRepository,
    now,
  ) {
    this.now = DateTime(now.year, now.month, now.day);
  }

  List<DisplayTile> cache;

  Future<List<DisplayTile>> get displayTiles async {
    if (cache != null) {
      return cache;
    }

    final _cancellationInfo = await cancellationInfoRepository.info;
    final _displayTiles = <DisplayTile>[];
    final _cancelledMeals = await cancelledMealsRepository.cancelledMeals;
    final deliverables =
        await deliverableRepository.deliverablesForNextWorkingDay;

    if (_cancellationInfo.tillDate == null) {
      return [];
    }

    final tomorrow = now.add(Duration(days: 1));
    final isDeliverablesSetForTomorrow = deliverables.firstWhere(
            (deliverable) =>
                DateFormat('yyyy/MM/dd').format(deliverable.date) ==
                DateFormat('yyyy/MM/dd').format(tomorrow),
            orElse: () => null) !=
        null;

    for (var date = isDeliverablesSetForTomorrow
            ? tomorrow.add(Duration(days: 1))
            : tomorrow;
        DateFormat('yyyy/MM/dd').format(date).compareTo(
                DateFormat('yyyy/MM/dd').format(_cancellationInfo.tillDate)) <=
            0;
        date = date.add(
      Duration(days: 1),
    ),) {
      var temp = _cancellationInfo.cancellationMeals
          .where(
            (cancellationMeal) =>
            cancellationMeal.daysRequired.contains(date.weekday),
      )
          .toList();

      if (temp.length == 0) {
        continue;
      }

      temp.removeWhere(
            (cancellationMeal) =>
            _cancelledMeals.contains(
              CancelledMeal(cancellationMeal.id, date),
            ),
      );

      temp.sort((a, b) => a.priority.compareTo(b.priority));

      final meals = temp
          .map(
            (cancellationMeal) => DisplayMeal(
              cancellationMeal.name,
              date,
              cancellationMeal.id,
            ),
      )
          .toList();

      /// don't add meals where everything is cancelled
      if (meals.length > 0) {
        _displayTiles.add(DisplayTile(date, meals));
      }
    }

    cache = _displayTiles;
    return _displayTiles;
  }
}
