import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/cancel/Model.dart';
import 'package:preetibhojan/home/Cancellation/ViewModel.dart';
import 'package:preetibhojan/home/Exceptions.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

class Cancellation extends StatefulWidget {
  final GetCredentialsFunc getCredentialsFunc;
  final CancellationViewModel cancellationViewModel;

  const Cancellation(
    this.getCredentialsFunc,
    this.cancellationViewModel,
  );

  @override
  _CancellationState createState() => _CancellationState();
}

class _CancellationState extends State<Cancellation> {
  Future<List<DisplayTile>> future;
  bool showCircularProgressIndicator = false;

  static final _log = Logger('Cancellation');

  @override
  void initState() {
    super.initState();
    future = widget.cancellationViewModel.displayTiles;
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      showCircularProgressIndicator: showCircularProgressIndicator,
      getCredentialsFunc: widget.getCredentialsFunc,
      body: FutureBuilder(
        future: future,
        builder: (context, snapshot) {
          _log.info(snapshot);

          if (snapshot.hasData) {
            if (snapshot.data.length == 0) {
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (_) => AlertDialog(
                    title: Text('Oops'),
                    content: Text(
                      'No Meal found to Cancel. This means either no Meals '
                      'are Booked yet or all Meals booked have been Cancelled',
                      textAlign: TextAlign.justify,
                    ),
                    actions: <Widget>[
                      FlatButton(
                        child: Text('Okay'),
                        onPressed: () {
                          Navigator.of(context)
                              .popUntil((route) => route.isFirst);
                        },
                      ),
                    ],
                  ),
                );
              });

              return Container();
            }

            final displayTiles = snapshot.data as List<DisplayTile>;

            for (final tile in displayTiles) {
              tile.addListener(() {
                this.setState(() {});
              });
            }

            return ListView(
              children: [
                ...displayTiles
                    .map(
                      (displayTile) => Padding(
                        padding: const EdgeInsets.all(15),
                        child: Column(
                          children: <Widget>[
                            Text(
                              DateFormat.yMMMd().format(displayTile.date),
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            Column(
                              children: displayTile.meals
                                  .map(
                                    (meal) => Row(
                                      children: <Widget>[
                                        Text(
                                          meal.name,
                                          style: TextStyle(fontSize: 16),
                                        ),
                                        Spacer(),
                                        Checkbox(
                                          value: meal.enabled,
                                          onChanged: (bool value) =>
                                              meal.enabled = value,
                                        )
                                      ],
                                    ),
                                  )
                                  .toList(),
                            ),
                          ],
                        ),
                      ),
                    )
                    .toList(),
                Column(
                  children: <Widget>[
                    RaisedButton(
                      child: Text('Cancel'),
                      onPressed: onCancelledPressed,
                    ),
                  ],
                ),
              ],
            );
          }

          if (snapshot.hasError) {
            if (snapshot.error is CouldNotFetchPaymentInfo) {
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: Text('Oops'),
                    content: Text(
                      'Meal\'s Schedule Not Found. Please First Schedule your Meal',
                    ),
                    actions: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context)
                              .pushReplacementNamed('/schedule');
                        },
                        child: Text('Take me to schedule'),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context)
                              .popUntil((route) => route.isFirst);
                        },
                        child: Text('Go back home'),
                      ),
                    ],
                  ),
                );
              });
            } else {
              _log.severe('Something went wrong', snapshot.error);
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (_) => SomethingWentWrong(),
                );
              });
            }

            return Container();
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  void onCancelledPressed() async {
    final meals = <DisplayMeal>[];

    _log.info('Creating list of meals to cancel');

    for (final tile in widget.cancellationViewModel.cache) {
      meals.addAll(tile.meals.where((meal) => meal.enabled));
    }

    if (meals.length == 0) {
      showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(
              title: Text('Oops'),
              content:
              Text('Please select a Meal to Cancel before pressing Cancel'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Okay'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
      );

      return;
    }

    setState(() {
      showCircularProgressIndicator = true;
    });

    _log.info('fetching credentials');

    final credentials = await widget.getCredentialsFunc();

    final response = await http.post(
      base_url + '/cancel',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
        'meals': meals,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    setState(() {
      showCircularProgressIndicator = false;
    });

    if (response.statusCode != 200) {
      showDialog(
        context: context,
        builder: (_) => SomethingWentWrong(),
      );

      return;
    }

    final cancelledMeals = jsonDecode(response.body)['success'] as List;

    final allMealsSuccessfullyCancelled = cancelledMeals.length == meals.length;
    if (allMealsSuccessfullyCancelled) {
      showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(
              title: Text('Success'),
              content: Text('Cancellation Successful'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Okay'),
                  onPressed: () {
                    Navigator.popUntil(context, ModalRoute.withName('/home'));
                  },
                )
              ],
            ),
      );

      return;
    }

    /// The only meals that won't be successfully cancelled would be today's
    /// meals. This will happen when deliverables are set after cancellation
    /// is loaded.
    showDialog(
      context: context,
      builder: (context) =>
          AlertDialog(
            title: Text('Oops'),
            content: Text(
              'Today\'s meals cannot be cancelled. Other meals have been cancelled. '
                  'To cancel today\'s meal, go to cart and book from there',
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Okay'),
                onPressed: () {
                  Navigator.popUntil(context, ModalRoute.withName('/home'));
                },
              )
            ],
          ),
    );
  }
}
