import 'package:preetibhojan/common/meals/Model.dart';

class CancellationInfo {
  final List<CancellationMeal> cancellationMeals;
  final DateTime tillDate;

  CancellationInfo(this.cancellationMeals, this.tillDate);

  factory CancellationInfo.fromJson(
    Map<String, dynamic> json,
    List<Meal> meals,
  ) {
    final cancellationMeals = <CancellationMeal>[];

    DateTime tillDate;

    if (json.containsKey('till_date')) {
      tillDate = DateTime.parse(json['till_date']);
    }

    if (tillDate != null && tillDate.difference(DateTime.now()).inDays > 0) {
      json['package_info'].forEach((package) {
        final meal = meals.firstWhere((m) => m.id == package['meal_id']);
        cancellationMeals.add(CancellationMeal.fromJson(package, meal));
      });
    }

    return CancellationInfo(
      cancellationMeals,
      tillDate,
    );
  }
}

class CancellationMeal {
  final int id;
  final String name;
  final List<int> daysRequired;
  final int priority;

  CancellationMeal(this.id, this.name, this.daysRequired, this.priority);

  factory CancellationMeal.fromJson(Map<String, dynamic> json, Meal meal) {
    return CancellationMeal(
      meal.id,
      meal.name,
      (json['delivery_info'] as List)
          .map<int>((deliveryInfo) => deliveryInfo['day'])
          .toList(),
      meal.priority,
    );
  }
}
