import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/deliverables/Model.dart';
import 'package:preetibhojan/common/deliverables/Repositories.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';
import 'package:preetibhojan/home/Booking/Model.dart';
import 'package:preetibhojan/home/Booking/Repositories.dart';
import 'package:preetibhojan/home/Payment/View.dart';
import 'package:preetibhojan/util/ui/CardTile.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/QuantityPicker.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:provider/provider.dart';

import 'Models.dart';

class CartScreen extends StatefulWidget {
  final GetCredentialsFunc getCredentialsFunc;
  final DeliveryLocationsRepository deliveryLocationsRepository;
  final DeliverableRepository deliverableRepository;
  final PaymentInfoRepository paymentInfoRepository;

  CartScreen(
    this.getCredentialsFunc,
    this.deliveryLocationsRepository,
    this.deliverableRepository,
    this.paymentInfoRepository,
  );

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  bool showCircularProgressIndicator = false;

  Future<void> future;
  Cart tempCart;

  bool isRegularCustomer = false;

  static final _log = Logger('Cart Screen');

  @override
  void initState() {
    super.initState();
    widget.getCredentialsFunc().then(
      (credentials) {
        setState(() {
          isRegularCustomer = credentials.isRegularCustomer;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var cart = Provider.of<Cart>(context);

    if (future == null) {
      future = Future.wait([
        cart.loadData(),
        widget.deliveryLocationsRepository.deliveryLocations,
        widget.deliverableRepository.deliverablesForNextWorkingDay,
        widget.paymentInfoRepository.paymentInfo,
      ]);
    }

    return FutureBuilder(
      future: future,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        _log.info(snapshot);

        if (snapshot.hasError) {
          _log.severe('Something went wrong');
          SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
            showDialog(
                context: context, builder: (context) => SomethingWentWrong());
          });

          return CustomScaffold(
            getCredentialsFunc: widget.getCredentialsFunc,
            body: Container(),
          );
        }

        if (snapshot.connectionState == ConnectionState.done) {
          tempCart = cart.clone();

          final deliveryLocations = snapshot.data[1] as List<DeliveryLocation>;

          deliveryLocations.remove(DeliveryLocation.NotRequired);

          final expandedCartItems =
              tempCart.items.values.expand((i) => i).toList();

          var deliverables = snapshot.data[2] as List<Deliverable>;

          double total = 0;

          if (expandedCartItems.length > 0) {
            deliverables = deliverables
                .where((deliverable) =>
                    expandedCartItems.firstWhere(
                      (item) => item.package.id == deliverable.packageID,
                      orElse: () => null,
                    ) !=
                        null)
                .toList();

            total += expandedCartItems
                .map((item) => item.package.price * item.quantity)
                .reduce((value, element) => value + element);
          }

          final tempCredit = (snapshot.data[3] as PaymentInfo).creditAvailable;

          final paid = (deliverables.isNotEmpty
              ? deliverables.map((d) => d.price).reduce((a, b) => a + b)
              : 0) + tempCredit;

          /// If customer if regular, give them need based customer delta
          /// discount per meal
          final regularCustomerDiscount = isRegularCustomer
              ? tempCart.items.length * config.needBasedCustomerDelta
              : 0;

          final tax = (total - regularCustomerDiscount) * config.gst / 100;

          final totalIncludingTax =
              total + tax - paid - regularCustomerDiscount;

          return CustomScaffold(
            showCircularProgressIndicator: showCircularProgressIndicator,
            getCredentialsFunc: widget.getCredentialsFunc,
            body: ListView(
              children: [
                Visibility(
                  visible: tempCart.items.length == 0,
                  child: Container(
                    margin: const EdgeInsets.fromLTRB(80, 50, 80, 400),
                    alignment: Alignment.center,
                    child: Text(
                      'Cart is currently empty',
                      style: Theme.of(context)
                          .textTheme
                          .headline4
                          .copyWith(fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                ...tempCart.items.entries.map(
                  (cartItems) {
                    final meal = cartItems.key;

                    return CardTile(
                      title: Text(
                        meal.name,
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(6, 0, 6, 0),
                            child: Row(
                              children: [
                                Text(
                                  'Delivery Location: ',
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .headline6,
                                ),
                                Spacer(),
                                DropdownButton<DeliveryLocation>(
                                  items: deliveryLocations
                                      .map(
                                        (location) =>
                                        DropdownMenuItem<DeliveryLocation>(
                                          child: Text(location.name),
                                          value: location,
                                        ),
                                  )
                                      .toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      tempCart.deliveryLocationOf[meal.name] =
                                          value;
                                    });
                                  },
                                  value: tempCart.deliveryLocationOf[meal.name],
                                ),
                              ],
                            ),
                          ),
                          DataTable(
                            columnSpacing: 30,
                            horizontalMargin: 5,
                            columns: <DataColumn>[
                              DataColumn(
                                label: Container(
                                  width: 100,
                                  child: Center(
                                    child: Text(
                                      'Package',
                                      style:
                                      Theme
                                          .of(context)
                                          .textTheme
                                          .headline6,
                                    ),
                                  ),
                                ),
                              ),
                              DataColumn(
                                label: Container(
                                  width: 100,
                                  child: Center(
                                    child: Text(
                                      'Quantity',
                                      style:
                                      Theme
                                          .of(context)
                                          .textTheme
                                          .headline6,
                                    ),
                                  ),
                                ),
                              ),
                              DataColumn(
                                label: Container(
                                  width: 90,
                                  child: Center(
                                    child: Text(
                                      'Price',
                                      style:
                                      Theme
                                          .of(context)
                                          .textTheme
                                          .headline6,
                                    ),
                                  ),
                                ),
                                numeric: true,
                              ),
                            ],
                            rows: cartItems.value
                                .map(
                                  (cartItem) => DataRow(
                                cells: <DataCell>[
                                  buildDataCell(cartItem.package.name),
                                  DataCell(
                                    QuantityPicker(
                                      textColor: Colors.white,
                                      borderColor: Colors.white,
                                      value: cartItem.quantity,
                                      onDecreaseQuantity: () {
                                        if (cartItem.quantity > 1) {
                                          cartItem.quantity--;
                                        } else {
                                          cartItems.value.remove(cartItem);

                                          if (cartItems.value.length == 0) {
                                            tempCart.items
                                                .remove(cartItems.key);
                                          }
                                        }
                                        setState(() {});
                                      },
                                      onIncreaseQuantity: () {
                                        setState(() {
                                          cartItem.quantity++;
                                        });
                                      },
                                    ),
                                  ),
                                  buildDataCell(
                                    '$rupeeSymbol ${(cartItem.package.price *
                                        cartItem.quantity).toStringAsFixed(2)}',
                                  ),
                                ],
                              ),
                            )
                                .toList(),
                          ),
                        ],
                      ),
                    );
                  },
                ).toList(),
                Visibility(
                  visible: tempCart.items.length != 0,
                  child: DefaultTextStyle(
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 25, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text('Total   : '),
                              ),
                              Visibility(
                                visible: isRegularCustomer,
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Text('Regular Customer Discount   : '),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text('Paid   : '),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text('Tax   : '),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text('Final Bill Amount   : '),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                    '  $rupeeSymbol ${total.toStringAsFixed(
                                        2)}'),
                              ),
                              Visibility(
                                visible: isRegularCustomer,
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Text(
                                      '-  $rupeeSymbol ${regularCustomerDiscount
                                          .toStringAsFixed(2)}'),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                    '  $rupeeSymbol ${paid.toStringAsFixed(
                                        2)}'),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                    '  $rupeeSymbol ${tax.toStringAsFixed(2)}'),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                    '  $rupeeSymbol ${totalIncludingTax
                                        .toStringAsFixed(2)}'),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        child: Text('Save'),
                        onPressed: () async {
                          setState(() {
                            showCircularProgressIndicator = true;
                          });

                          try {
                            cart = tempCart.clone();
                            await cart.saveChanges();

                            showDialog(
                              context: context,
                              builder: (context) =>
                                  AlertDialog(
                                    title: Text('Save successful'),
                                    actions: [
                                      FlatButton(
                                        child: Text('Okay'),
                                        onPressed: () {
                                          Navigator.of(context)
                                              .popUntil((route) =>
                                          route.isFirst);
                                        },
                                      )
                                    ],
                                  ),
                            );
                          } catch (exception) {
                            showDialog(
                              context: context,
                              builder: (context) => SomethingWentWrong(),
                            );
                          } finally {
                            setState(() {
                              showCircularProgressIndicator = false;
                            });
                          }
                        },
                      ),
                    ),
                    Visibility(
                      visible: tempCart.items.length != 0,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          child: Text(
                            totalIncludingTax > 0 ? 'Proceed to Pay' : 'Book',
                          ),
                          onPressed: () async {
                            for (final meal in cart.items.keys) {
                              if (cart.deliveryLocationOf[meal] == null) {
                                showDialog(
                                  context: context,
                                  builder: (context) =>
                                      AlertDialog(
                                        title: Text('Oops!'),
                                        content: Text(
                                          'Please set the deliver location for ${meal
                                              .name}',
                                        ),
                                        actions: [
                                          FlatButton(
                                            child: Text('Okay'),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      ),
                                );
                                return;
                              }
                            }

                            _log.info('Save changes to cart');

                            setState(() {
                              showCircularProgressIndicator = true;
                            });

                            try {
                              cart = tempCart.clone();
                              await cart.saveChanges();
                            } catch (exception) {
                              _log.severe('Something went wrong while saving '
                                  'changes to cart');
                              showDialog(
                                context: context,
                                builder: (context) => SomethingWentWrong(),
                              );

                              setState(() {
                                showCircularProgressIndicator = false;
                              });

                              return;
                            }

                            _log.info('Getting credentials');
                            final credentials =
                            await widget.getCredentialsFunc();

                            _log.info('Sending post request to reserve');

                            final reserveResponse = await http.post(
                              base_url + '/reserve',
                              headers: {'Content-Type': 'application/json'},
                              body: jsonEncode({
                                'phone_number': credentials.phoneNumber,
                                'password': credentials.password,
                                'packages_to_reserve': cart.itemsToJson
                              }),
                            );

                            _log.info(
                                'status code: ${reserveResponse.statusCode}');
                            _log.info('response: ${reserveResponse.body}');

                            if (reserveResponse.statusCode != 200) {
                              _log.info('Something went wrong while reserving');
                              showDialog(
                                context: context,
                                builder: (context) => SomethingWentWrong(),
                              );

                              setState(() {
                                showCircularProgressIndicator = false;
                              });

                              return;
                            }

                            final reserveJson = jsonDecode(reserveResponse.body)
                            as Map<String, dynamic>;

                            if (reserveJson.containsKey('failed')) {
                              cart.invalidateCache();

                              showDialog(
                                barrierDismissible: false,
                                context: context,
                                builder: (context) =>
                                    AlertDialog(
                                      title: Text('Oops'),
                                      content: Text(
                                        'Some packages could not be ordered. '
                                            'Click on okay to refresh quantities',
                                      ),
                                      actions: [
                                        FlatButton(
                                          child: Text('Okay'),
                                          onPressed: () {
                                            Navigator.of(context)
                                                .popUntil((route) =>
                                            route.isFirst);

                                            Navigator.of(context)
                                                .pushReplacementNamed('/home');
                                          },
                                        )
                                      ],
                                    ),
                              );

                              return;
                            }

                            final reservationID = reserveJson['reservation_id'];

                            final result =
                            await Navigator.of(context).pushNamed(
                              '/payment',
                              arguments: totalIncludingTax,
                            ) as PaymentResult;

                            if (result == null ||
                                result.transactionID == null) {
                              _log.info('Something went wrong while payment');
                              showDialog(
                                context: context,
                                builder: (_) => SomethingWentWrong(),
                              );

                              setState(() {
                                showCircularProgressIndicator = false;
                              });

                              return;
                            }

                            final bookingResponse = await http.post(
                              base_url + '/book/spot',
                              headers: {'content-type': 'application/json'},
                              body: jsonEncode({
                                'phone_number': credentials.phoneNumber,
                                'password': credentials.password,
                                'reservation_id': reservationID,
                                'transaction_id': result.transactionID
                              }),
                            );

                            _log.info(
                                'status code: ${bookingResponse.statusCode}');
                            _log.info('response: ${bookingResponse.body}');

                            if (bookingResponse.statusCode != 200) {
                              _log.info('Something went wrong while booking');
                              showDialog(
                                context: context,
                                builder: (_) => SomethingWentWrong(),
                              );

                              setState(() {
                                showCircularProgressIndicator = false;
                              });

                              return;
                            }

                            setState(() {
                              showCircularProgressIndicator = false;
                            });

                            showDialog(
                              context: context,
                              builder: (context) =>
                                  AlertDialog(
                                    title: Text('Success'),
                                    content: Text('Tiffin Booked successfully'),
                                    actions: [
                                      FlatButton(
                                        child: Text('Okay'),
                                        onPressed: () {
                                          Navigator.of(context)
                                              .popUntil((route) =>
                                          route.isFirst);
                                        },
                                      )
                                    ],
                                  ),
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        }

        return CustomScaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
          getCredentialsFunc: widget.getCredentialsFunc,
        );
      },
    );
  }

  DataCell buildDataCell(String text) {
    return DataCell(
      Text(
        text,
        style: TextStyle(
          fontSize: 16,
          color: Colors.white,
        ),
      ),
    );
  }
}
