import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/home/Menu/Models.dart';

class CartItem {
  final PackageForMenu package;
  int quantity;

  CartItem(
    this.package,
    this.quantity,
  );

  factory CartItem.clone(CartItem cartItem) {
    return CartItem(
      cartItem.package,
      cartItem.quantity,
    );
  }

  @override
  String toString() {
    return 'CartItem{package: $package, quantity: $quantity}';
  }

  Map<String, dynamic> toJson() {
    return {
      'package_id': package.id,
      'quantity': quantity,
    };
  }
}

abstract class Cart {
  Map<Meal, List<CartItem>> get items;

  bool get didFetchData;

  Map<String, DeliveryLocation> get deliveryLocationOf;

  Cart(Map<Meal, List<CartItem>> items,
    Map<String, DeliveryLocation> deliverLocationOf,
    bool didFetchData,);

  Future<void> loadData();

  Future<void> saveChanges();

  Future<void> clear();

  Cart clone();

  void invalidateCache();

  List<Map<String, dynamic>> get itemsToJson;
}
