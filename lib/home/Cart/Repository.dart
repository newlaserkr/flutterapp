import 'dart:collection';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/home/Menu/Repository.dart';

import 'Models.dart';

class FoodCart implements Cart {
  /// SplayTreeMap used to preserve order according to priority of meal
  Map<Meal, List<CartItem>> items = SplayTreeMap<Meal, List<CartItem>>();
  Map<String, DeliveryLocation> deliveryLocationOf = {};

  final GetCredentialsFunc getCredentialsFunc;
  final PackagesForMenuRepository packageRepository;
  final MealsRepository mealsRepository;

  FoodCart(
    this.getCredentialsFunc,
    this.packageRepository,
    this.mealsRepository,
  );

  static final _log = Logger('Food Cart');

  @override
  String toString() {
    return 'FoodCart{items: $items, deliveryLocationOf: $deliveryLocationOf}';
  }

  @override
  Future<void> loadData() async {
    items.clear();

    _log.info('Getting packages');
    final packages = await packageRepository.packages;

    if (!didFetchData) {
      packages.forEach((package) {
        package.price += config.needBasedCustomerDelta;
      });
    }

    _log.info('getting meals');
    final meals = await mealsRepository.meals;

    _log.info('getting credentials');
    final credentials = await getCredentialsFunc();

    _log.info('loading cart from internet');
    final response = await http.post(
      '$base_url/my_cart',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.info('Could not load cart');
      throw Exception('Could not load cart');
    }

    (jsonDecode(response.body) as List).forEach((json) {
      final package = packages.firstWhere(
              (package) => package.id == json['package_id'],
          orElse: () => null);

      /// Package can be null if package added to cart is no more available.
      /// In that case, skip the package.
      if (package == null) {
        return;
      }

      final meal = meals.firstWhere((meal) => meal.id == package.mealId);

      final cartItem = CartItem(
        package,
        json['quantity'],
      );

      if (items[meal] == null) {
        items[meal] = [cartItem];
      } else {
        items[meal].add(cartItem);
      }

      deliveryLocationOf[meal.name] =
          DeliveryLocation.fromInt(json['delivery_location']);
    });

    /// Cart needs to be saved to remove all the packages which are no more
    /// available
    await saveChanges();

    didFetchData = true;
  }

  @override
  bool didFetchData = false;

  @override
  Future<void> saveChanges() async {
    _log.info('getting credentials');
    final credentials = await getCredentialsFunc();

    if (items.length == 0) {
      await _clearCart(credentials);
    } else {
      await _saveCart(credentials);
    }
  }

  Future _saveCart(CustomerCredentials credentials) async {
    _log.info('saving data from cart onto the internet');
    final response = await http.post(
      base_url + '/overwrite_cart',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
        'items': itemsToJson,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.info('Something went wrong while saving $itemsToJson');
      throw Exception('Something went wrong while saving');
    }
  }

  List<Map<String, dynamic>> get itemsToJson {
    final temp = <Map<String, dynamic>>[];
    items.forEach((meal, values) {
      for (final value in values) {
        temp.add({
          'package_id': value.package.id,
          'quantity': value.quantity,
          'delivery_location': deliveryLocationOf[meal.name],
        });
      }
    });

    return temp;
  }

  Future _clearCart(CustomerCredentials credentials) async {
    _log.info('items is empty. Clearing cart');
    final response = await http.post(
      base_url + '/cart/clear',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      _log.info('Something went wrong while clearing cart');
      throw Exception('Something went wrong while clearing cart');
    }

    deliveryLocationOf.clear();
    items.clear();
    didFetchData = false;
  }

  @override
  Future<void> clear() async {
    await _clearCart(await getCredentialsFunc());
    items.clear();
  }

  @override
  Cart clone() {
    final cart = FoodCart(
      this.getCredentialsFunc,
      this.packageRepository,
      this.mealsRepository,
    );

    cart.items = this.items;
    cart.deliveryLocationOf = deliveryLocationOf;
    cart.didFetchData = didFetchData;

    return cart;
  }

  @override
  void invalidateCache() {
    packageRepository.invalidateCache();
    didFetchData = false;
  }
}