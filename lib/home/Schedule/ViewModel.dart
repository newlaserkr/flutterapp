import 'package:flutter/foundation.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/common/packages/Repositories.dart';
import 'package:preetibhojan/util/datetime.dart';

import 'Models.dart';
import 'Repositories.dart';

class ScheduleViewModel with ChangeNotifier {
  final PackageRepository packageRepository;
  final CuisineRepository cuisineRepository;
  final MealsRepository mealRepository;
  final DeliveryLocationsRepository deliveryLocationsRepository;
  final ScheduleInfoRepository scheduleInfoRepository;
  final GetCredentialsFunc getCredentialsFunc;

  ScheduleViewModel({
    @required this.cuisineRepository,
    @required this.packageRepository,
    @required this.mealRepository,
    @required this.deliveryLocationsRepository,
    @required this.scheduleInfoRepository,
    @required this.getCredentialsFunc,
  });

  MealInfoManager mealInfoManager;

  void addMeal(Meal meal) {
    final mealInfo = MealInfo(
      deliveryLocations,
      cannotOrderOn: meal.cannotOrderOn,
    );

    final schedule = _scheduleInfo.firstWhere(
        (schedule) => schedule.mealID == meal.id,
        orElse: () => null);
    if (schedule != null) {
      mealInfo.required = true;
      mealInfo.requiredPackage = packages.firstWhere(
        (package) => package.id == schedule.packageID,
      );

      schedule.deliveryInfo.forEach((deliveryInfo) {
        mealInfo.deliveryInfo[dayOfWeekToString(deliveryInfo.day)] =
            deliveryInfo.location;
      });
    }

    mealInfoManager.add(meal, mealInfo);
  }

  List<Package> packagesFor(Meal meal) {
    final temp = packages
        .where((element) =>
    element.mealId == meal.id && element.cuisineId == requiredCuisineID)
        .toList();

    return temp;
  }

  List<Package> packages;
  List<Meal> meals;
  List<ScheduleInfo> _scheduleInfo;

  List<DeliveryLocation> deliveryLocations;

  bool isRegularCustomer;

  Future<void> fetchData() async {
    packages = await packageRepository.packages;
    meals = await mealRepository.meals;
    deliveryLocations = await deliveryLocationsRepository.deliveryLocations;
    requiredCuisineID = await cuisineRepository.cuisineID;
    _scheduleInfo = await scheduleInfoRepository.scheduleInfo;

    packages.sort((a, b) => a.name.compareTo(b.name));

    final mealsToRemove = <Meal>[];
    for (final meal in meals) {
      if (packages.firstWhere((package) => package.mealId == meal.id,
              orElse: () => null) ==
          null) {
        mealsToRemove.add(meal);
      }
    }

    meals.removeWhere((meal) => mealsToRemove.contains(meal));

    if (mealInfoManager == null) {
      mealInfoManager = MealInfoManager(meals);
      mealInfoManager.addListener(() => notifyListeners());
      for (var meal in meals) {
        addMeal(meal);
      }
    }

    if (isRegularCustomer == null) {
      final credentials = await getCredentialsFunc();
      isRegularCustomer = credentials.isRegularCustomer;
    }
  }

  int requiredCuisineID;

  bool isValid() {
    bool isValid = true;

    if (mealInfoManager.length < 1) {
      return false;
    }

    return isValid;
  }

  void requiredMealsUpdated() {
    notifyListeners();
  }

  @override
  String toString() {
    return 'Cuisine: $requiredCuisineID Meals: $mealInfoManager';
  }
}
