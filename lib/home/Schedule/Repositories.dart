import 'dart:convert';
import 'dart:core';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';

import 'Models.dart';

abstract class CuisineRepository {
  Future<int> get cuisineID;
}

class StubCuisines implements CuisineRepository {
  final int input;

  StubCuisines({@required this.input});

  @override
  Future<int> get cuisineID => Future.value(input);
}

class NetworkCuisineRepository implements CuisineRepository {
  static final _log = Logger('NetworkCuisineRepository');
  int _cache;

  final GetCredentialsFunc getCredentialsFunc;

  NetworkCuisineRepository(this.getCredentialsFunc);

  @override
  Future<int> get cuisineID async {
    _log.info('Getting cuisine');

    if (_cache != null) {
      _log.info('Returning cache');
      return _cache;
    }

    final credentials = await getCredentialsFunc();

    _log.info('Sending get request to cuisine_id');
    final response = await http.post(
      base_url + '/cuisine_id',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
      }),
    )
        /*.timeout(
          Duration(seconds: 5),
        )*/
        ;

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode == 200) {
      _cache = jsonDecode(response.body)['cuisine_id'];
      return _cache;
    }

    _log.info('status code: ${response.statusCode}');
    throw Exception('Failed to load cuisines');
  }
}

abstract class ScheduleInfoRepository {
  Future<List<ScheduleInfo>> get scheduleInfo;
}

class StubScheduleInfoRepository implements ScheduleInfoRepository {
  final List<ScheduleInfo> input;

  StubScheduleInfoRepository(this.input);

  @override
  Future<List<ScheduleInfo>> get scheduleInfo => Future.value(scheduleInfo);
}

class NetworkScheduleInfoRepository implements ScheduleInfoRepository {
  List<ScheduleInfo> _cache;

  static final _log = Logger('Network schedule info repository');
  static const NotScheduled = 2;
  final GetCredentialsFunc getCredentialsFunc;

  NetworkScheduleInfoRepository(this.getCredentialsFunc);

  @override
  Future<List<ScheduleInfo>> get scheduleInfo async {
    _log.info('get meal info');

    if (_cache != null) {
      _log.info('return cache');
      return _cache;
    }

    final credentials = await getCredentialsFunc();

    final response = await http.post(
      base_url + '/payment_info',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      final error = jsonDecode(response.body)['error'];

      if (error == NotScheduled) {
        return [];
      }

      _log.severe('Could not fetch payment info');
      throw Exception('Could not fetch payment info');
    }

    _cache = (jsonDecode(response.body)['package_info'] as List)
        .map((json) => ScheduleInfo.fromJson(json))
        .toList();

    return _cache;
  }
}
