import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/packages/Model.dart';

class Cuisine {
  final int cuisineID;
  final String name;

  Cuisine(this.cuisineID, this.name);

  @override
  String toString() {
    return 'ID: $cuisineID Name: $name';
  }

  factory Cuisine.fromJson(Map<String, dynamic> json) {
    return Cuisine(
      json['id'],
      json['name'],
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Cuisine &&
          runtimeType == other.runtimeType &&
          cuisineID == other.cuisineID;

  @override
  int get hashCode => cuisineID.hashCode;
}

class MealInfo with ChangeNotifier {
  static final _days = const [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];

  /// If cannot cancel on is not null, days will not contain saturday. It is a hack.
  /// Evening snacks and dinner are the only ones with a non null cannotOrderOn;
  /// both of which are saturdays.
  List<String> get days {
    if (cannotOrderOn == null) {
      return _days;
    }

    return _days.sublist(0, _days.length - 1);
  }

  Map<String, DeliveryLocation> deliveryInfo;

  bool _required = false;

  bool get required => _required;

  set required(value) {
    _required = value;
    notifyListeners();
  }

  Package _requiredPackage;

  Package get requiredPackage => _requiredPackage;

  set requiredPackage(value) {
    _requiredPackage = value;
    notifyListeners();
  }

  final int cannotOrderOn;

  List<DeliveryLocation> deliveryLocations;

  MealInfo(this.deliveryLocations, {this.cannotOrderOn}) {
    assert(
      deliveryLocations != null && deliveryLocations.length > 0,
      'Delivery locations should not be null and should have at least 1 element',
    );

    deliveryInfo = {
      'Monday': null,
      'Tuesday': null,
      'Wednesday': null,
      'Thursday': null,
      'Friday': null,
    };

    if (cannotOrderOn == null) {
      deliveryInfo['Saturday'] = null;
    }
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MealInfo &&
          runtimeType == other.runtimeType &&
          _requiredPackage == other._requiredPackage;

  @override
  int get hashCode => _requiredPackage.hashCode;

  @override
  String toString() {
    return 'Package: $requiredPackage';
  }
}

class MealInfoManager with ChangeNotifier {
  final List<Meal> meals;

  MealInfoManager(this.meals) {
    meals.sort((a, b) => a.priority.compareTo(b.priority));
  }

  var _map = <Meal, MealInfo>{};

  get length => _map.length;

  void add(Meal requiredMeal, MealInfo mealInfo) {
    if (!_map.containsKey(requiredMeal)) {
      _map[requiredMeal] = mealInfo;

      // T changes notify listeners of ChangeNotifierMap that it has changed
      _map[requiredMeal].addListener(() {
        notifyListeners();
      });

      notifyListeners();
    }
  }

  MealInfo operator [](Meal key) => _map[key];

  void remove(Meal key) {
    _map.remove(key);
    notifyListeners();
  }

  void clear() {
    _map.clear();
    notifyListeners();
  }

  List<Meal> get keys {
    final result = <Meal>[];

    /// done to maintain priority
    for (var meal in meals) {
      if (_map.containsKey(meal)) result.add(meal);
    }
    return result;
  }

  @override
  String toString() => _map.toString();

  void forEach(void f(Meal key, MealInfo value)) =>
      _map.forEach((k, v) => f(k, v));

  bool contains(Meal key) => _map.containsKey(key);

  Map<Meal, MealInfo> get internal => _map;
}

class DeliveryInfo {
  final int day;
  final DeliveryLocation location;

  DeliveryInfo(this.day, this.location);

  factory DeliveryInfo.fromJson(Map<String, dynamic> json) {
    return DeliveryInfo(
      json['day'],
      DeliveryLocation.fromInt(json['delivery_location']),
    );
  }
}

class ScheduleInfo {
  final int packageID, mealID;
  final List<DeliveryInfo> deliveryInfo;

  ScheduleInfo(this.packageID, this.deliveryInfo, this.mealID);

  factory ScheduleInfo.fromJson(Map<String, dynamic> json) {
    return ScheduleInfo(
      json['package_id'],
      (json['delivery_info'] as List)
          .map((d) => DeliveryInfo.fromJson(d))
          .toList(),
      json['meal_id'],
    );
  }
}
