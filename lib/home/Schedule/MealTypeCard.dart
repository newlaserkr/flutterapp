import 'package:flutter/material.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/home/GroupPackages.dart';
import 'package:provider/provider.dart';

import 'ViewModel.dart';

class MealTypeCard extends StatefulWidget {
  final Meal meal;
  final bool enabled;

  MealTypeCard({Key key, @required this.meal, @required this.enabled})
      : super(key: key);

  @override
  _MealTypeCardState createState() => _MealTypeCardState();
}

class _MealTypeCardState extends State<MealTypeCard> {
  ScheduleViewModel viewModel;
  List<Package> packages;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    viewModel = Provider.of<ScheduleViewModel>(context);

    packages = viewModel.packagesFor(widget.meal);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Column(
          children: [
            ...viewModel.mealInfoManager[widget.meal].deliveryInfo.keys
                .map((day) {
              return Row(
                children: <Widget>[
                  Text(
                    day,
                    style: TextStyle(fontSize: 16),
                  ),
                  Spacer(),
                  DropdownButton<DeliveryLocation>(
                    items:
                        viewModel.mealInfoManager[widget.meal].deliveryLocations
                            .map(
                          (deliveryLocation) =>
                              DropdownMenuItem<DeliveryLocation>(
                            child: Text(deliveryLocation.name),
                            value: deliveryLocation,
                          ),
                        )
                        .toList(),
                    onChanged: !widget.enabled
                        ? null
                        : (DeliveryLocation value) {
                            viewModel.mealInfoManager[widget.meal]
                                .deliveryInfo[day] = value;
                            setState(() {});
                          },
                    value: viewModel
                        .mealInfoManager[widget.meal].deliveryInfo[day],
                  ),
                ],
              );
            }).toList(),
          ],
        ),
        Visibility(
          visible: widget.enabled,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
            child: Text(
              'Package',
              style: TextStyle(fontSize: 18),
            ),
          ),
        ),
        GroupPackages(
          packages: packages,
          requiredPackage:
              viewModel.mealInfoManager[widget.meal].requiredPackage,
          onPackageChanged: !widget.enabled
              ? null
              : (Package value) {
            setState(() {
              viewModel.mealInfoManager[widget.meal].requiredPackage =
                  value;
            });
          },
        ),
      ],
    );
  }
}
