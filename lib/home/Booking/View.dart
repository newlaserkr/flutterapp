import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/home/Payment/View.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

import '../Exceptions.dart';
import 'ViewModel.dart';

class Booking extends StatefulWidget {
  final GetCredentialsFunc getCredentialsFunc;
  final BookingViewModel bookingViewModel;

  Booking(
    this.getCredentialsFunc,
    this.bookingViewModel,
  );

  @override
  _BookingState createState() => _BookingState();
}

const MEAL_ALREADY_BOOKED_FOR_THIS_WEEK = 2;

class _BookingState extends State<Booking> {
  static final _log = Logger('Booking');
  bool hasNoticeShown;

  bool showCircularProgressIndicator = false;

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      showCircularProgressIndicator: showCircularProgressIndicator,
      getCredentialsFunc: widget.getCredentialsFunc,
      body: FutureBuilder(
        future: widget.bookingViewModel.fetchData(),
        builder: (context, snapshot) {
          _log.info(snapshot);
          if (snapshot.hasError) {
            if (snapshot.error is CouldNotFetchPaymentInfo) {
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: Text('Oops'),
                    content: Text(
                      'Meal\'s Schedule Not Found. Please First Schedule your Meal',
                    ),
                    actions: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context)
                              .pushReplacementNamed('/schedule');
                        },
                        child: Text('Take me to schedule'),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context)
                              .popUntil((route) => route.isFirst);
                        },
                        child: Text('Go back home'),
                      ),
                    ],
                  ),
                );
              });
            } else {
              _log.severe('Something went wrong', snapshot.error);

              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (_) => SomethingWentWrong(),
                );
              });
            }

            return Container();
          }

          if (snapshot.connectionState == ConnectionState.done) {
            if (widget.bookingViewModel.priceOf.length == 0) {
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (context) =>
                      AlertDialog(
                        title: Text('Oops'),
                        content: Text(
                          'You have not yet scheduled yet. Would you like to schedule it now?',
                        ),
                        actions: <Widget>[
                          FlatButton(
                        child: Text('Yes'),
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.of(context)
                              .pushReplacementNamed('/schedule');
                        },
                      ),
                      FlatButton(
                        child: Text('No'),
                        onPressed: () {
                          Navigator.of(context)
                              .popUntil((route) => route.isFirst);
                        },
                      ),
                    ],
                  ),
                );
              });

              return Container();
            }

            if (hasNoticeShown == null) {
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                setState(() {
                  hasNoticeShown = false;
                });
              });
            }

            return ListView(
              children: <Widget>[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      'Payment for Booking till ${DateFormat.yMMMd().format(
                          widget.bookingViewModel.till)}',
                      style: Theme
                          .of(context)
                          .textTheme
                          .headline6,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      rupeeSymbol,
                      style: TextStyle(fontSize: 40),
                    ),
                    Text(
                      widget.bookingViewModel.totalIncludingTaxAndDeposit
                          .toStringAsFixed(2),
                      style: TextStyle(fontSize: 40),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DefaultTextStyle(
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                    ),
                    child: DataTable(
                      dividerThickness: 0,
                      columns: <DataColumn>[
                        DataColumn(
                          label: Text(
                            'Description',
                            style: Theme.of(context).textTheme.headline6,
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            'Price',
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          numeric: true,
                        ),
                      ],
                      rows: <DataRow>[
                        ...widget.bookingViewModel.priceOf.keys
                            .map(
                              (meal) =>
                              buildDataRow(
                                meal.name,
                                widget.bookingViewModel.priceOf[meal]
                                    .toDouble(),
                              ),
                        )
                            .toList(),
                        buildDataRow('Tax', widget.bookingViewModel.tax),
                        buildDataRow(
                            'Deposit', BookingViewModel.DEPOSIT.toDouble()),
                        buildDataRow(
                          'Credit Available ${DateFormat.yMMMd().format(
                              DateTime.now())}',
                          -widget.bookingViewModel.creditAvailable,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Column(
                  children: <Widget>[
                    Visibility(
                      visible: widget.bookingViewModel
                          .totalIncludingTaxAndDeposit > 0,
                      child: RaisedButton(
                        child: Text('Proceed to Pay'),
                        onPressed: () async {
                          final result = await Navigator.of(context).pushNamed(
                            '/payment',
                            arguments:
                            widget.bookingViewModel.totalIncludingTaxAndDeposit,
                          ) as PaymentResult;

                          if (result == null || result.transactionID == null) {
                            showDialog(
                              context: context,
                              builder: (_) => SomethingWentWrong(),
                            );

                            return;
                          }

                          await book(result.transactionID);
                        },
                      ),
                    ),
                    Visibility(
                      visible:
                      widget.bookingViewModel.totalIncludingTaxAndDeposit <= 0,
                      child: RaisedButton(
                        child: Text('Book'),
                        onPressed: book,
                      ),
                    ),
                  ],
                )
              ],
            );
          }

          if (hasNoticeShown != null && hasNoticeShown == false) {
            SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
              showDialog(
                context: context,
                builder: (_) => AlertDialog(
                  title: Text('Note'),
                  content: Text(
                    'Booking will be effective from Next Working Day. For '
                    'booking of today\'s meal, please book through home page. '
                    'The amount will be charged as per regular customer\'s rates',
                    textAlign: TextAlign.justify,
                  ),
                  actions: <Widget>[
                    FlatButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text('Okay'),
                    )
                  ],
                ),
              );
              hasNoticeShown = true;
            });
          }
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        },
      ),
    );
  }

  Future<void> book([String transactionID]) async {
    setState(() {
      showCircularProgressIndicator = true;
    });
    _log.info('getting credentials');

    final credentials = await widget.getCredentialsFunc();

    _log.info('sending post request to book');

    final response = await http.post(
      base_url + '/schedule/book',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
        'transaction_id': transactionID
      }),
    );

    setState(() {
      showCircularProgressIndicator = false;
    });

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      final error = jsonDecode(response.body)['error'];

      _log.severe('error code: $error');

      showDialog(
        context: context,
        builder: (_) => SomethingWentWrong(),
      );

      return;
    }

    final storage = FlutterSecureStorage();

    await storage.write(
      key: 'is_regular_customer',
      value: 'true',
    );

    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Text('Success'),
        content: Text(
          'Tiffin booked successfully',
          textAlign: TextAlign.justify,
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(context).popUntil((route) => route.isFirst);
            },
          ),
        ],
      ),
    );
  }

  DataRow buildDataRow(String description, double price, {style}) {
    if (style == null) {
      style = TextStyle(color: Colors.white, fontSize: 18);
    }
    return DataRow(
      cells: <DataCell>[
        DataCell(
          Text(
            description,
            style: style,
          ),
        ),
        DataCell(
          Text(
            price.toStringAsFixed(2),
            style: style,
          ),
        ),
      ],
    );
  }
}
