import 'package:preetibhojan/common/meals/Model.dart';

class PaymentInfo {
  final Map<Meal, PackageInfo> packageInfo;
  final double creditAvailable;

  const PaymentInfo(this.packageInfo, this.creditAvailable);

  factory PaymentInfo.fromJson(Map<String, dynamic> json, List<Meal> meals) {
    /// count temp credit as credit available
    double creditAvailable = double.parse(json['temp_credit']);

    final packageInfo = <Meal, PackageInfo>{};
    json['package_info'].forEach((package) {
      final meal = meals.firstWhere((m) => m.id == package['meal_id']);
      packageInfo[meal] = PackageInfo.fromJson(package);
    });

    return PaymentInfo(
      packageInfo,
      creditAvailable,
    );
  }
}

class PackageInfo {
  final int price;
  final List<int> daysRequired;

  const PackageInfo(this.price, this.daysRequired);

  factory PackageInfo.fromJson(Map<String, dynamic> json) {
    return PackageInfo(
      json['price'],
      (json['delivery_info'] as List)
          .map<int>((deliveryInfo) => deliveryInfo['day'])
          .toList(),
    );
  }
}
