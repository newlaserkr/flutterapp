import 'dart:collection';
import 'dart:math';

import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/deliverables/Repositories.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/home/Booking/Model.dart';

import 'Repositories.dart';

DateTime nextSaturday(DateTime now) {
  if (now.weekday == DateTime.saturday) {
    return now.add(Duration(days: 7));
  }

  if (now.weekday == DateTime.sunday) {
    return now.add(Duration(days: 6));
  }

  return now.add(Duration(days: 6 - now.weekday));
}

class BookingViewModel {
  static final _log = Logger('Booking view model');

  // ignore: non_constant_identifier_names
  static final GST = config.gst;

  // ignore: non_constant_identifier_names
  static final DEPOSIT = config.deposit;

  final PaymentInfoRepository paymentInfoRepository;
  final DeliverableRepository deliverableRepository;

  var costBreakup = <String, double>{};

  DateTime dateToBookFrom;

  BookingViewModel(
    this.paymentInfoRepository,
    this.deliverableRepository,
    now,) {
    assert(paymentInfoRepository != null && now != null);
    dateToBookFrom = now.add(Duration(days: 1));
  }

  PaymentInfo _paymentInfo;

  /// SplayTreeMap used to maintain priority of meals
  final priceOf = SplayTreeMap<Meal, int>();

  DateTime till;

  Future<void> fetchData() async {
    if (_paymentInfo == null) {
      _paymentInfo = await paymentInfoRepository.paymentInfo;

      creditAvailable = _paymentInfo.creditAvailable;

      till = dateToBookFrom;

      final deliverables =
      await deliverableRepository.deliverablesForNextWorkingDay;

      if (deliverables.isNotEmpty &&
          deliverables.firstWhere(
                (deliverable) =>
                    DateFormat('yyyy/MM/dd').format(deliverable.date) ==
                    DateFormat('yyyy/MM/dd').format(dateToBookFrom),
                orElse: () => null,
              ) !=
              null) {
        creditAvailable += deliverables
            .map((d) => d.price)
            .reduce((value, element) => value + element);
      }

      if (_paymentInfo.packageInfo.length > 0) {
        calculateCost();
      }
    }
  }

  int totalExcludingTaxAndDeposit = 0;
  double totalIncludingTaxAndDeposit = 0;
  double creditAvailable = 0;
  double tax = 0;

  void calculateCost() {
    final _daysRequiredFor = <Meal, int>{};
    _paymentInfo.packageInfo.forEach((meal, packageInfo) {
      _daysRequiredFor[meal] = packageInfo.daysRequired
          .where((day) => day >= dateToBookFrom.weekday)
          .length;
    });

    if (_daysRequiredFor.values.reduce(max) < 3) {
      _paymentInfo.packageInfo.forEach((meal, packageInfo) {
        _daysRequiredFor[meal] += packageInfo.daysRequired.length;
      });

      if (dateToBookFrom.weekday == DateTime.sunday ||
          dateToBookFrom.weekday == DateTime.saturday) {
        till = nextSaturday(dateToBookFrom);
      } else {
        till = nextSaturday(till).add(Duration(days: 7));
      }
    } else {
      till = nextSaturday(till);
    }

    priceOf.clear();

    totalExcludingTaxAndDeposit = 0;
    totalIncludingTaxAndDeposit = 0;

    _paymentInfo.packageInfo.forEach((meal, packageInfo) {
      priceOf[meal] = packageInfo.price * _daysRequiredFor[meal];
      totalExcludingTaxAndDeposit += priceOf[meal];
    });

    tax = totalExcludingTaxAndDeposit * GST / 100;
    totalIncludingTaxAndDeposit =
        totalExcludingTaxAndDeposit + tax + DEPOSIT - creditAvailable;

    _log.info('Days required: $_daysRequiredFor');
  }
}
