import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/home/Exceptions.dart';

import 'Model.dart';

abstract class PaymentInfoRepository {
  Future<PaymentInfo> get paymentInfo;
}

class StubPaymentInfoRepository implements PaymentInfoRepository {
  final PaymentInfo input;

  StubPaymentInfoRepository(this.input);

  @override
  Future<PaymentInfo> get paymentInfo => Future.value(input);
}

class NetworkPaymentInfoRepository implements PaymentInfoRepository {
  static final _log = Logger('Network Payment Info Repository');
  PaymentInfo _cache;
  final GetCredentialsFunc getCredentialsFunc;

  static const NotScheduled = 2;

  NetworkPaymentInfoRepository(this.getCredentialsFunc);

  @override
  Future<PaymentInfo> get paymentInfo async {
    _log.info('Getting all meals');
    if (_cache != null) {
      _log.info('Returning cache');
      return _cache;
    }

    _log.info('Getting credentials');
    final credentials = await getCredentialsFunc();

    _log.info('Sending post request for meals');

    final mealResponse = await http.post(
      base_url + '/meals',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode(
        {
          'phone_number': credentials.phoneNumber,
          'password': credentials.password,
        },
      ),
    );

    _log.info('status code: ${mealResponse.statusCode}');
    _log.info('response: ${mealResponse.body}');

    if (mealResponse.statusCode != 200) {
      _log.severe('Could not fetch meals');
      throw Exception('Unable to fetch meals');
    }

    final meals = (jsonDecode(mealResponse.body) as List)
        .map((json) => Meal.fromJson(json))
        .toList();

    final paymentInfoResponse = await http.post(
      base_url + '/payment_info',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
      }),
    );

    _log.info('status code: ${mealResponse.statusCode}');
    _log.info('response: ${mealResponse.body}');

    if (paymentInfoResponse.statusCode != 200) {
      final error = jsonDecode(paymentInfoResponse.body)['error'];

      if (error == NotScheduled) {
        return PaymentInfo({}, 0);
      }

      _log.severe('Could not fetch payment info');
      throw CouldNotFetchPaymentInfo();
    }

    final paymentInfoJson =
        jsonDecode(paymentInfoResponse.body) as Map<String, dynamic>;

    return PaymentInfo.fromJson(paymentInfoJson, meals);
  }
}

abstract class TransactionIDRepository {
  Future<String> get transactionID;
}

class StubTransactionIDRepository implements TransactionIDRepository {
  final String input;

  StubTransactionIDRepository(this.input);

  @override
  Future<String> get transactionID => Future.value(input);
}

class NetworkTransactionIDRepository implements TransactionIDRepository {
  String _cache;

  static final _log = Logger('Network transaction ID repository');

  final GetCredentialsFunc getCredentialsFunc;

  NetworkTransactionIDRepository(this.getCredentialsFunc);

  @override
  Future<String> get transactionID async {
    _log.info('getting transaction id');

    if (_cache != null) {
      _log.info('returning cache');
      return _cache;
    }

    _log.info('Getting cache');
    final credentials = await getCredentialsFunc();

    _log.info('sending post request for transaction id');
    final response = await http.post(
      base_url + '/transaction_id',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
      }),
    );

    if (response.statusCode != 200) {
      _log.info('Could not fetch transaction id');
      throw Exception('Could not fetch transaction id');
    }

    _cache = jsonDecode(response.body)['result'];

    return _cache;
  }
}
