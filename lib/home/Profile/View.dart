import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';
import 'package:preetibhojan/home/Profile/Models.dart';
import 'package:preetibhojan/util/string.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/EditTextDialog.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:validators/validators.dart';

import 'Repository.dart';
import 'ViewModel.dart';

class Profile extends StatefulWidget {
  final GetCredentialsFunc getCredentialsFunc;

  Profile({Key key, this.getCredentialsFunc}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  static final _log = Logger('Profile');
  bool showCircularProgressIndicator = false;

  ProfileViewModel profileViewModel;

  @override
  void initState() {
    super.initState();
    profileViewModel = ProfileViewModel(
      NetworkCustomerRepository(widget.getCredentialsFunc),
      NetworkOfficeAddressRepository(widget.getCredentialsFunc),
      NetworkHomeAddressRepository(widget.getCredentialsFunc),
      NetworkCuisineRepository(),
      NetworkAreaRepository(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      showCircularProgressIndicator: showCircularProgressIndicator,
      getCredentialsFunc: widget.getCredentialsFunc,
      body: FutureBuilder(
        future: profileViewModel.future,
        builder: (context, snapshot) {
          _log.info(snapshot);
          if (snapshot.hasError) {
            _log.severe('Something went wrong', snapshot.error);

            SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
              showDialog(
                context: context,
                builder: (_) => SomethingWentWrong(),
              );
            });

            return Container();
          }

          if (snapshot.connectionState == ConnectionState.done) {
            _log.info(profileViewModel);

            return ListView(
              padding: const EdgeInsets.fromLTRB(20, 4, 20, 4),
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      '${profileViewModel.newCustomer.firstName.capitalize()} ${profileViewModel.newCustomer.lastName.capitalize()}',
                      style: Theme.of(context).textTheme.headline5.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                  ),
                ),
                Divider(),
                DefaultTextStyle(
                  style: TextStyle(
                    fontSize: 16,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        ContactDetails(profileViewModel),
                        Row(
                          children: <Widget>[
                            CustomDropDownRow<Cuisine>(
                              items: profileViewModel.cuisines
                                  .map(
                                    (cuisine) => DropdownMenuItem(
                                      child: Text(cuisine.name),
                                      value: cuisine,
                                    ),
                                  )
                                  .toList(),
                              onChanged: (Cuisine value) {
                                setState(() {
                                  profileViewModel.newCustomer.cuisineID =
                                      value.cuisineID;
                                });
                              },
                              value: profileViewModel.cuisines.firstWhere(
                                  (element) =>
                                      element.cuisineID ==
                                      profileViewModel.newCustomer.cuisineID),
                              name: 'Cuisine',
                            ),
                          ],
                        ),
                        Divider(),
                        HomeAddressDetails(profileViewModel),
                        Divider(),
                        OfficeAddressDetails(profileViewModel),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            RaisedButton(
                              onPressed: updateDetail,
                              child: Text('Save Edits'),
                            ),
                            Container(
                              width: 50,
                            ),
                            RaisedButton(
                              child: Text('Restore Defaults'),
                              onPressed: () {
                                setState(() {
                                  profileViewModel.reset();
                                });
                              },
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  void updateDetail() async {
    setState(() {
      showCircularProgressIndicator = true;
    });

    final credentials = await widget.getCredentialsFunc();

    var showSuccess = true;

    final updated = <String>[];

    _log.info('change email');
    var response = await http.post(
      base_url + '/change_email',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
        'email': profileViewModel.newCustomer.email,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      showFailure('email');
      showSuccess = false;
    }

    updated.add('Email');

    _log.info('Change cuisine');

    response = await http.post(
      base_url + '/change_cuisine',
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'phone_number': credentials.phoneNumber,
        'password': credentials.password,
        'cuisine_id': profileViewModel.newCustomer.cuisineID,
      }),
    );

    _log.info('status code: ${response.statusCode}');
    _log.info('response: ${response.body}');

    if (response.statusCode != 200) {
      showFailure('cuisine');
      showSuccess = false;
    }

    updated.add('Cuisine');

    if (profileViewModel.newHomeAddress.enabled) {
      if (!profileViewModel.newHomeAddress.isValid()) {
        showDialog(
          context: context,
          builder: (_) => AlertDialog(
            title: Text('Oops'),
            content: Text('Some fields are missing in Home Address'),
          ),
        );

        setState(() {
          showCircularProgressIndicator = false;
        });

        return;
      }

      _log.info('Change home address');
      response = await http.post(
        base_url + '/change_home_address',
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          'phone_number': credentials.phoneNumber,
          'password': credentials.password,
          'home_address': profileViewModel.newHomeAddress,
        }),
      );

      _log.info('status code: ${response.statusCode}');
      _log.info('response: ${response.body}');

      if (response.statusCode != 200) {
        showFailure('Home Address');
        showSuccess = false;
      }

      updated.add('Home Address');
    }

    if (profileViewModel.newOfficeAddress.enabled) {
      if (!profileViewModel.newOfficeAddress.isValid()) {
        showDialog(
          context: context,
          builder: (_) => AlertDialog(
            title: Text('Oops'),
            content: Text('Some fields are missing in Office Address'),
          ),
        );

        setState(() {
          showCircularProgressIndicator = false;
        });

        return;
      }

      _log.info('Change office address');

      response = await http.post(
        base_url + '/change_office_address',
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          'phone_number': credentials.phoneNumber,
          'password': credentials.password,
          'office_address': profileViewModel.newOfficeAddress,
        }),
      );

      _log.info('status code: ${response.statusCode}');
      _log.info('response: ${response.body}');

      if (response.statusCode != 200) {
        showFailure('Office Address');
        showSuccess = false;
      }

      updated.add('Office Address');
    }

    String temp = '';

    if (updated.length == 1) {
      temp = updated[0];
    } else {
      for (int i = 0; i < updated.length - 1; i++) {
        temp += '${updated[i]},   ';
      }

      temp += 'and ${updated[updated.length - 1]}';
    }

    if (showSuccess) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => AlertDialog(
          title: Text('Success'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                'Successfully Changed -- $temp',
                textAlign: TextAlign.justify,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                child: Visibility(
                  visible: profileViewModel.newCustomer.cuisineID !=
                      profileViewModel.customer.cuisineID,
                  child: Text(
                    'Note: As you changed your Cuisine, Your Schedule and '
                    'Cancelled Meals has been deleted. Please Schedule Again',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.lime,
                    ),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                /**
                 * Cannot use Navigator.of(context).popUntil((route) => route.isFirst);
                 * because the cuisine might have been changed. Which means menu might
                 * need to be reloaded.
                 */
                Navigator.of(context)
                    .pushNamedAndRemoveUntil('/home', (route) => false);
              },
            ),
          ],
        ),
      );
    }

    setState(() {
      showCircularProgressIndicator = false;
    });
  }

  void showFailure(String name) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Text('Oops'),
        content: Text('Could not change $name'),
        actions: <Widget>[
          FlatButton(
            child: Text('Okay'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}

class Header extends StatelessWidget {
  final String title;

  const Header({
    Key key,
    @required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: Theme.of(context).textTheme.headline6,
    );
  }
}

class ContactDetails extends StatefulWidget {
  final ProfileViewModel profileViewModel;

  const ContactDetails(this.profileViewModel);

  @override
  _ContactDetailsState createState() => _ContactDetailsState();
}

class _ContactDetailsState extends State<ContactDetails> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        CustomRow(
          leading: 'Phone No.',
          trailing: widget.profileViewModel.newCustomer.phoneNumber,
          onSave: null,
          validator: null,
        ),
        CustomRow(
          leading: 'Email',
          trailing: widget.profileViewModel.newCustomer.email,
          onSave: (value) {
            setState(() {
              widget.profileViewModel.newCustomer.email = value;
            });
          },
          validator: (value) =>
              isEmail(value) ? null : 'Please enter a valid email',
        ),
      ],
    );
  }
}

class HomeAddressDetails extends StatefulWidget {
  final ProfileViewModel profileViewModel;

  const HomeAddressDetails(this.profileViewModel);

  @override
  _HomeAddressDetailsState createState() => _HomeAddressDetailsState();
}

class _HomeAddressDetailsState extends State<HomeAddressDetails> {
  @override
  Widget build(BuildContext context) {
    print(widget.profileViewModel.newHomeAddress);
    return Column(
      children: <Widget>[
        CheckboxListTile(
          value: widget.profileViewModel.newHomeAddress.enabled,
          onChanged: (bool value) {
            setState(() {
              widget.profileViewModel.newHomeAddress.enabled = value;
            });
          },
          title: Text(
            'Home Address',
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        Visibility(
          visible: widget.profileViewModel.newHomeAddress.enabled,
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  CustomDropDownRow<Area>(
                    items: widget.profileViewModel.areas
                        .map(
                          (area) => DropdownMenuItem<Area>(
                            child: Text(area.name),
                            value: area,
                          ),
                        )
                        .toList(),
                    onChanged: (Area value) {
                      setState(() {
                        widget.profileViewModel.homeAddressArea = value;
                      });
                    },
                    value: widget.profileViewModel.homeAddressArea,
                    name: 'Area',
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  CustomDropDownRow<Locality>(
                    items: widget.profileViewModel.homeAddressArea == null
                        ? widget.profileViewModel.localities
                        .map(
                          (locality) =>
                          DropdownMenuItem<Locality>(
                            child: Text(locality.name),
                            value: locality,
                          ),
                    )
                        .toList()
                        : widget.profileViewModel.localities
                        .where((locality) =>
                    locality.areaID ==
                        widget.profileViewModel.homeAddressArea.id)
                        .map(
                          (locality) =>
                          DropdownMenuItem<Locality>(
                            child: Text(locality.name),
                            value: locality,
                          ),
                    )
                        .toList(),
                    onChanged: (Locality value) {
                      setState(() {
                        widget.profileViewModel.homeAddressLocality = value;
                      });
                    },
                    value: widget.profileViewModel.homeAddressLocality,
                    name: 'Locality',
                  ),
                ],
              ),
              CustomRow(
                leading: 'Society',
                validator: (value) => isAlphaNumericUnderScoreOrSpace(value)
                    ? null
                    : 'Society must only contain a-z, A-Z, 0-9, _ and space',
                onSave: (value) {
                  setState(() {
                    widget.profileViewModel.newHomeAddress.society = value;
                  });
                },
                trailing: widget.profileViewModel.newHomeAddress.society,
              ),
              CustomRow(
                leading: 'Building',
                validator: (value) => isAlphaNumericUnderScoreOrSpace(value)
                    ? null
                    : 'Building must only contain a-z, A-Z, 0-9, _ and space',
                onSave: (value) {
                  setState(() {
                    widget.profileViewModel.newHomeAddress.building = value;
                  });
                },
                trailing: widget.profileViewModel.newHomeAddress.building,
              ),
              CustomRow(
                leading: 'Flat No.',
                validator: (value) => isAlphaNumericUnderScoreOrSpace(value)
                    ? null
                    : 'Flat No. must only contain a-z, A-Z, 0-9, _ and space',
                onSave: (value) {
                  setState(() {
                    widget.profileViewModel.newHomeAddress.flatNumber = value;
                  });
                },
                trailing: widget.profileViewModel.newHomeAddress.flatNumber,
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class OfficeAddressDetails extends StatefulWidget {
  final ProfileViewModel profileViewModel;

  const OfficeAddressDetails(this.profileViewModel);

  @override
  _OfficeAddressDetailsState createState() => _OfficeAddressDetailsState();
}

class _OfficeAddressDetailsState extends State<OfficeAddressDetails> {
  @override
  Widget build(BuildContext context) {
    print(widget.profileViewModel.newOfficeAddress);
    return Column(
      children: <Widget>[
        CheckboxListTile(
          value: widget.profileViewModel.newOfficeAddress.enabled,
          onChanged: (bool value) {
            setState(() {
              widget.profileViewModel.newOfficeAddress.enabled = value;
            });
          },
          title: Text(
            'Office Address',
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        Visibility(
          visible: widget.profileViewModel.newOfficeAddress.enabled,
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  CustomDropDownRow<Area>(
                    items: widget.profileViewModel.areas
                        .map(
                          (area) =>
                          DropdownMenuItem<Area>(
                            child: Text(area.name),
                            value: area,
                          ),
                    )
                        .toList(),
                    onChanged: (Area value) {
                      setState(() {
                        widget.profileViewModel.officeAddressArea = value;
                      });
                    },
                    value: widget.profileViewModel.officeAddressArea,
                    name: 'Area',
                  ),
                ],
              ),
              CustomRow(
                leading: 'Tower',
                validator: (value) => isAlphaNumericUnderScoreOrSpace(value)
                    ? null
                    : 'Tower must only contain a-z, A-Z, 0-9, _ and space',
                onSave: (value) {
                  setState(() {
                    widget.profileViewModel.newOfficeAddress.tower = value;
                  });
                },
                trailing: widget.profileViewModel.newOfficeAddress.tower,
              ),
              CustomRow(
                leading: 'Floor',
                validator: (value) =>
                isNumeric(value) && int.parse(value) > 0
                    ? null
                    : 'Floor must be a positive number',
                onSave: (value) {
                  setState(() {
                    widget.profileViewModel.newOfficeAddress.floor = value;
                  });
                },
                trailing:
                widget.profileViewModel.newOfficeAddress.floor.toString(),
                keyboardType: TextInputType.number,
              ),
              CustomRow(
                leading: 'Office No.',
                validator: (value) => isAlphaNumericUnderScoreOrSpace(value)
                    ? null
                    : 'Office no. must only contain a-z, A-Z, 0-9, _ and space',
                onSave: (value) {
                  setState(() {
                    widget.profileViewModel.newOfficeAddress.officeNumber =
                        value;
                  });
                },
                trailing: widget.profileViewModel.newOfficeAddress.officeNumber,
              ),
              CustomRow(
                leading: 'Company',
                validator: (value) => isAlphaNumericUnderScoreOrSpace(value)
                    ? null
                    : 'Company must only contain a-z, A-Z, 0-9, _ and space',
                onSave: (value) {
                  setState(() {
                    widget.profileViewModel.newOfficeAddress.company = value;
                  });
                },
                trailing: widget.profileViewModel.newOfficeAddress.company,
              ),
            ],
          ),
        ),
      ],
    );
  }
}

/// Custom Row for profile.
class CustomRow extends StatelessWidget {
  /// The thing to display on the left side of the row
  final String leading;

  /// The thing to the right side of the row
  final String trailing;

  /// What to do when use successfully saves the value
  final ValueChanged<String> onSave;

  /// a validator to validate the form field
  final FormFieldValidator<String> validator;

  /// type of keyboard
  final TextInputType keyboardType;

  const CustomRow({
    Key key,
    this.leading = '',
    this.trailing = '',
    @required this.onSave,
    @required this.validator,
    this.keyboardType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onSave == null
          ? null
          : () {
        showDialog(
          context: context,
          builder: (_) =>
              EditTextDialog(
                label: leading,
                onCancel: () {
                  Navigator.of(context).pop();
                },
                onSave: onSave,
                validator: validator,
                initialValue: trailing,
                keyboardType: keyboardType,
              ),
        );
      },
      child: Container(
        height: 44,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Flexible(
              child: Text(leading),
              flex: 1,
              fit: FlexFit.tight,
            ),
            Flexible(
              child: Text(trailing),
              flex: 2,
            ),
          ],
        ),
      ),
    );
  }
}

class CustomDropDownRow<T> extends StatelessWidget {
  final String name;
  final List<DropdownMenuItem> items;
  final ValueChanged<T> onChanged;
  final T value;

  const CustomDropDownRow({
    Key key,
    this.name,
    this.items,
    this.onChanged,
    this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 44,
      width: 350,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Flexible(
            child: Text(name),
            flex: 1,
            fit: FlexFit.tight,
          ),
          Flexible(
            fit: FlexFit.loose,
            flex: 2,
            child: DropdownButton<T>(
              items: items,
              onChanged: onChanged,
              value: value,
            ),
          ),
        ],
      ),
    );
  }
}
