import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';

import 'Models.dart';
import 'Repository.dart';

class ProfileViewModel {
  Customer customer;
  HomeAddress homeAddress;
  OfficeAddress officeAddress;

  Customer newCustomer;
  HomeAddress newHomeAddress;
  OfficeAddress newOfficeAddress;

  List<Area> areas;
  List<Locality> localities;

  List<Cuisine> cuisines;

  Area _homeAddressArea;

  Area get homeAddressArea => _homeAddressArea;

  set homeAddressArea(value) {
    _homeAddressArea = value;
    homeAddressLocality = localities
        .firstWhere((locality) => locality.areaID == homeAddressArea.id);
    newHomeAddress.localityID = homeAddressLocality.id;
  }

  Locality _homeAddressLocality;

  Locality get homeAddressLocality => _homeAddressLocality;

  set homeAddressLocality(value) {
    _homeAddressLocality = value;
    newHomeAddress.localityID = homeAddressLocality.id;
  }

  Area _officeAddressArea;

  Area get officeAddressArea => _officeAddressArea;

  set officeAddressArea(value) {
    _officeAddressArea = value;
    newOfficeAddress.areaID = officeAddressArea.id;
  }

  final CustomerRepository customerRepository;
  final OfficeAddressRepository officeAddressRepository;
  final HomeAddressRepository homeAddressRepository;
  final CuisineRepository cuisineRepository;

  final AreaRepository areaRepository;

  ProfileViewModel(
    this.customerRepository,
    this.officeAddressRepository,
    this.homeAddressRepository,
    this.cuisineRepository,
    this.areaRepository,) {
    future = fetchData();
  }

  Future<void> future;

  Future<void> fetchData() async {
    if (customer == null) {
      customer = await customerRepository.customer;
      newCustomer = Customer.clone(customer);
    }

    if (homeAddress == null) {
      homeAddress = await homeAddressRepository.homeAddress;
      newHomeAddress = HomeAddress.clone(homeAddress);
    }

    if (officeAddress == null) {
      officeAddress = await officeAddressRepository.officeAddress;
      newOfficeAddress = OfficeAddress.clone(officeAddress);
    }

    if (cuisines == null) {
      cuisines = await cuisineRepository.cuisines;
    }

    areas = await areaRepository.area();
    localities = await areaRepository.allLocalities;

    setAreaAndLocality();
  }

  void reset() {
    newCustomer = Customer.clone(customer);
    newHomeAddress = HomeAddress.clone(homeAddress);
    newOfficeAddress = OfficeAddress.clone(officeAddress);
    future = fetchData();

    setAreaAndLocality();
  }

  void setAreaAndLocality() {
    if (newHomeAddress.enabled) {
      final temp = localities.firstWhere((locality) =>
      locality.id == newHomeAddress.localityID);
      homeAddressArea = areas.firstWhere((area) => area.id == temp.areaID);
      homeAddressLocality = temp;
    }

    if (newOfficeAddress.enabled) {
      officeAddressArea =
          areas.firstWhere((area) => area.id == newOfficeAddress.areaID);
    }
  }
}
