import 'package:preetibhojan/util/string.dart';

/// If you make any change here, remember to change register and backend
class Customer {
  final String phoneNumber, firstName, lastName;
  int cuisineID;

  /// email is the only field customer is allowed to change
  String email;

  Customer(this.phoneNumber, this.firstName, this.lastName, this.email,
      this.cuisineID);

  factory Customer.clone(Customer customer) {
    return Customer(
      customer.phoneNumber,
      customer.firstName,
      customer.lastName,
      customer.email,
      customer.cuisineID,
    );
  }

  factory Customer.fromJson(Map<String, dynamic> json) {
    return Customer(
      json['phone_number'],
      json['first_name'],
      json['last_name'],
      json['email'],
      json['cuisine_id'],
    );
  }
}

/// If you make any change here, remember to change register and backend
class HomeAddress {
  String building, flatNumber, society;
  int localityID;
  bool enabled = true;

  HomeAddress(this.building,
      this.flatNumber,
      this.society,
      this.localityID,);

  factory HomeAddress.disabled() {
    final temp = HomeAddress('', '', '', -1);
    temp.enabled = false;
    return temp;
  }

  @override
  String toString() {
    return 'HomeAddress{building: $building, flatNumber: $flatNumber, society: $society, localityID: $localityID, enabled: $enabled}';
  }

  factory HomeAddress.clone(HomeAddress homeAddress) {
    final temp = HomeAddress(homeAddress.building, homeAddress.flatNumber,
        homeAddress.society, homeAddress.localityID);

    temp.enabled = homeAddress.enabled;

    return temp;
  }

  Map<String, dynamic> toJson() {
    return {
      'building': building,
      'flat_number': flatNumber,
      'society': society,
      'locality_id': localityID
    };
  }

  factory HomeAddress.fromJson(Map<String, dynamic> json) {
    if (json.containsKey('enabled')) {
      return HomeAddress.disabled();
    }

    return HomeAddress(json['building'], json['flat_number'], json['society'],
        json['locality_id']);
  }

  bool isValid() {
    return isAlphaNumericUnderScoreOrSpace(building) &&
        isAlphaNumericUnderScoreOrSpace(flatNumber) &&
        isAlphaNumericUnderScoreOrSpace(society);
  }
}

/// If you make any change here, remember to change register and backend
class OfficeAddress {
  String tower, officeNumber, company, floor;
  int areaID;
  bool enabled = true;

  OfficeAddress(this.tower,
      this.floor,
      this.officeNumber,
      this.company,
      this.areaID,);

  factory OfficeAddress.disabled() {
    final temp = OfficeAddress('', '', '', '', -1);
    temp.enabled = false;
    return temp;
  }

  @override
  String toString() {
    return 'OfficeAddress{tower: $tower, officeNumber: $officeNumber, company: $company, areaID: $areaID, floor: $floor, enabled: $enabled}';
  }

  factory OfficeAddress.clone(OfficeAddress officeAddress) {
    final temp = OfficeAddress(
      officeAddress.tower,
      officeAddress.floor,
      officeAddress.officeNumber,
      officeAddress.company,
      officeAddress.areaID,
    );

    temp.enabled = officeAddress.enabled;

    return temp;
  }

  Map<String, dynamic> toJson() {
    return {
      'tower': tower,
      'floor': int.parse(floor),
      'office_number': officeNumber,
      'company': company,
      'area_id': areaID
    };
  }

  factory OfficeAddress.fromJson(Map<String, dynamic> json) {
    if (json.containsKey('enabled')) {
      return OfficeAddress.disabled();
    }

    return OfficeAddress(
      json['tower'],
      json['floor'].toString(),
      json['office_number'],
      json['company'],
      json['area_id'],
    );
  }

  bool isValid() {
    return isAlphaNumericUnderScoreOrSpace(tower) &&
        isAlphaNumericUnderScoreOrSpace(officeNumber) &&
        isAlphaNumericUnderScoreOrSpace(company) &&
        int.parse(floor) > 0;
  }
}

/// If you make change here, remember to change register and backend
class Cuisine {
  final int cuisineID;
  final String name;

  Cuisine(this.cuisineID, this.name);

  @override
  String toString() {
    return 'ID: $cuisineID Name: $name';
  }

  factory Cuisine.fromJson(Map<String, dynamic> json) {
    return Cuisine(
      json['id'],
      json['name'],
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is Cuisine &&
              runtimeType == other.runtimeType &&
              cuisineID == other.cuisineID;

  @override
  int get hashCode => cuisineID.hashCode;
}
