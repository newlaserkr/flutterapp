import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/common/deliverables/Model.dart';
import 'package:preetibhojan/common/deliverables/Repositories.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/common/meals/Repository.dart';
import 'package:preetibhojan/common/packages/Model.dart';
import 'package:preetibhojan/common/packages/Repositories.dart';
import 'package:preetibhojan/util/ui/CardTile.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';

class Orders extends StatefulWidget {
  final DeliverableRepository deliverableRepository;
  final PackageRepository packageRepository;
  final MealsRepository mealsRepository;
  final GetCredentialsFunc getCredentialsFunc;

  const Orders(
    this.deliverableRepository,
    this.packageRepository,
    this.mealsRepository,
    this.getCredentialsFunc,
  );

  @override
  _OrdersState createState() => _OrdersState();
}

class _OrdersState extends State<Orders> {
  Future future;

  @override
  void initState() {
    super.initState();
    future = Future.wait([
      widget.deliverableRepository.allDeliverables,
      widget.packageRepository.packages,
      widget.mealsRepository.meals,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      getCredentialsFunc: widget.getCredentialsFunc,
      body: FutureBuilder(
        future: future,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasError) {
            SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
              showDialog(
                context: context,
                builder: (_) => SomethingWentWrong(),
              );
            });

            return Container();
          }

          if (snapshot.connectionState == ConnectionState.done) {
            final deliverables = snapshot.data[0] as List<Deliverable>;

            if (deliverables.length == 0) {
              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (_) => AlertDialog(
                    title: Text('Oops'),
                    content: Text('No Meals Booked Yet'),
                    actions: [
                      FlatButton(
                        child: Text('Okay'),
                        onPressed: () {
                          Navigator.of(context)
                              .popUntil(ModalRoute.withName('/home'));
                        },
                      )
                    ],
                  ),
                );
              });
            }

            final packages = snapshot.data[1] as List<Package>;
            final meals = snapshot.data[2] as List<Meal>;

            final dates = deliverables
                .map((deliverable) => deliverable.date)
                .toSet()
                .toList();

            dates.sort((a, b) => b.compareTo(a));

            return ListView.separated(
              separatorBuilder: (context, index) {
                return Divider(
                  indent: 20,
                  endIndent: 20,
                );
              },
              itemCount: dates.length,
              itemBuilder: (context, index) {
                final date = dates[index];
                final deliverablesForDate =
                deliverables.where((element) => element.date == date);

                if (deliverablesForDate.length == 0) {
                  return Container();
                }

                final packagesForDate = packages.where(
                      (package) =>
                  deliverablesForDate.firstWhere(
                          (deliverable) => deliverable.packageID == package.id,
                      orElse: () => null) !=
                      null,
                );

                final mealsForDate = meals
                    .where(
                      (meal) =>
                  packagesForDate.firstWhere(
                          (package) => package.mealId == meal.id,
                      orElse: () => null) !=
                      null,
                )
                    .toList();

                mealsForDate.sort();

                final total = packagesForDate
                    .map((package) => package.price)
                    .reduce((value, element) => value + element);

                return CardTile(
                  title: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 8.0),
                    child: Row(
                      children: [
                        Flexible(
                          flex: 9,
                          fit: FlexFit.tight,
                          child: Text(
                            '${DateFormat.yMMMd().format(date)}',
                            style: Theme
                                .of(context)
                                .textTheme
                                .headline5
                                .copyWith(fontWeight: FontWeight.bold),
                          ),
                        ),
                        Flexible(
                          flex: 2,
                          fit: FlexFit.tight,
                          child: Text(
                            '$rupeeSymbol $total',
                            style: Theme
                                .of(context)
                                .textTheme
                                .headline5
                                .copyWith(fontWeight: FontWeight.bold),
                            textAlign: TextAlign.end,
                          ),
                        ),
                      ],
                    ),
                  ),
                  child: DefaultTextStyle(
                    style: TextStyle(fontSize: 16),
                    child: Column(
                      children: mealsForDate.map(
                            (meal) {
                          return Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Flexible(
                                flex: 3,
                                fit: FlexFit.tight,
                                child: Text(
                                  meal.name,
                                ),
                              ),
                              Flexible(
                                flex: 5,
                                fit: FlexFit.tight,
                                child: Column(
                                  children: [
                                    ...packagesForDate
                                        .where((package) =>
                                    package.mealId == meal.id)
                                        .map(
                                          (package) {
                                        final deliverable = deliverablesForDate
                                            .firstWhere((p) =>
                                        p.packageID == package.id);
                                        return Padding(
                                          padding: const EdgeInsets.all(4),
                                          child: Row(
                                            children: [
                                              Flexible(
                                                flex: 5,
                                                fit: FlexFit.tight,
                                                child: Wrap(
                                                  children: [
                                                    Text(package.name),
                                                  ],
                                                ),
                                              ),
                                              Flexible(
                                                flex: 1,
                                                fit: FlexFit.tight,
                                                child: Text(
                                                  '${deliverable.quantity}',
                                                  textAlign: TextAlign.end,
                                                ),
                                              ),
                                              Flexible(
                                                flex: 3,
                                                fit: FlexFit.tight,
                                                child: Text(
                                                  '$rupeeSymbol ${deliverable
                                                      .price}',
                                                  textAlign: TextAlign.end,
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      },
                                    ).toList(),
                                  ],
                                ),
                              ),
                            ],
                          );
                        },
                      ).toList(),
                    ),
                  ),
                );
              },
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
