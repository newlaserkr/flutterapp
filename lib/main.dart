import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/Storage.dart';
import 'package:preetibhojan/home/Cancellation/ViewModel.dart';
import 'package:preetibhojan/home/Cart/View.dart';
import 'package:preetibhojan/home/Menu/View.dart';
import 'package:preetibhojan/home/Payment/Repository.dart';
import 'package:preetibhojan/home/Resume/ViewModel.dart';
import 'package:preetibhojan/home/Schedule.dart';
import 'package:preetibhojan/home/Schedule/Repositories.dart';
import 'package:preetibhojan/home/Schedule/ViewModel.dart';
import 'package:preetibhojan/register/Register.dart';
import 'package:preetibhojan/util/ui/CustomBackground.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:provider/provider.dart';

import 'common/cancel/Repository.dart';
import 'common/deliverables/Repositories.dart';
import 'common/delivery_location/Repositories.dart';
import 'common/meals/Repository.dart';
import 'common/packages/Repositories.dart';
import 'home/Booking/Repositories.dart';
import 'home/Booking/View.dart';
import 'home/Booking/ViewModel.dart';
import 'home/Cancellation/Repository.dart';
import 'home/Cancellation/View.dart';
import 'home/Cart/Models.dart';
import 'home/Cart/Repository.dart';
import 'home/Menu/Repository.dart';
import 'home/Menu/ViewModel.dart';
import 'home/Orders/View.dart';
import 'home/Payment/View.dart';
import 'home/Profile/View.dart';
import 'home/Resume/View.dart';
import 'login_or_register/View.dart';

void main() async {
  Logger.root.level = Level.ALL; // defaults to Level.INFO
  Logger.root.onRecord.listen((record) {
    print(
        '${record.level.name}: ${record.loggerName} ${record.time}: ${record.message}');
  });

  runApp(PreetiBhojan());
}

class AuthenticationFailed implements Exception {}

class PhoneNumberOrPasswordNotFound implements Exception {}

Future<CustomerCredentials> getCredentialsAndAuthenticate() async {
  final _log = Logger('GetCredentialsAndAuthenticate');
  _log.info('getting credentials');
  final credentials = await getCredentials();

  if (credentials.phoneNumber == null || credentials.password == null) {
    _log.severe(
        'Phone number or password not found. Pho. ${credentials.phoneNumber} Pass: ${credentials.password}');
    throw PhoneNumberOrPasswordNotFound();
  }

  _log.info('authenticating');
  await authenticate(credentials);

  final temp =
      jsonDecode((await isRegularCustomer(credentials)).body)['result'];

  if (temp != credentials.isRegularCustomer) {
    final storage = FlutterSecureStorage();

    await storage.write(
      key: 'is_regular_customer',
      value: temp.toString(),
    );
  }

  _log.info('isRegularCustomer ${credentials.isRegularCustomer}');
  return credentials;
}

Future<http.Response> isRegularCustomer(CustomerCredentials credentials) async {
  final _log = Logger('isRegularCustomer');
  final response = await http.post(
    base_url + '/am_i_regular_customer',
    headers: {'Content-Type': 'application/json'},
    body: jsonEncode(
      {
        'phone_number': credentials.phoneNumber.trim(),
        'password': credentials.password.trim(),
      },
    ),
  );

  _log.info('status code: ${response.statusCode}');
  _log.info('response: ${response.body}');

  if (response.statusCode != 200) {
    throw StateError(
        'Under no circumstance should isRegularCustomer fail given, customer '
        'is already authenticated');
  }
  return response;
}

Future authenticate(CustomerCredentials credentials) async {
  final _log = Logger('authenticate');

  _log.info(base_url + '/authenticate');

  final response = await http.post(
    base_url + '/authenticate',
    headers: {'Content-Type': 'application/json'},
    body: jsonEncode(
      {
        'phone_number': credentials.phoneNumber.trim(),
        'password': credentials.password.trim(),
      },
    ),
  );

  _log.info('status code: ${response.statusCode}');
  _log.info('response: ${response.body}');

  if (response.statusCode != 200) {
    throw AuthenticationFailed();
  }
}

/// Future.delayed is important because since dart 2 AFAIK, async
/// functions execute until first await is encountered. Without a delay
/// the exception will be thrown in initState method of InitialPage because
/// that's where initPage gets hold of the future. Without it, this results
/// in the app crashing. So while testing, keep the delay. Also, it simulates
/// the delay bound to happen by network calls in real method
Future<CustomerCredentials> regularCustomer() {
  return Future.delayed(
    Duration(milliseconds: 40),
    () => CustomerCredentials(
      '0987654321',
      'abcdefghi',
      'Test Customer',
      true,
    ),
  );
}

Future<CustomerCredentials> newCustomer() {
  return Future.delayed(
    Duration(milliseconds: 40),
    () => throw PhoneNumberOrPasswordNotFound(),
  );
}

Future<CustomerCredentials> needBasedCustomer() {
  return Future.delayed(
    Duration(milliseconds: 40),
    () => CustomerCredentials(
      '1234567809',
      'abcdefghi',
      'Test Customer',
      false,
    ),
  );
}

class PreetiBhojan extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _PreetiBhojanState createState() => _PreetiBhojanState();
}

class _PreetiBhojanState extends State<PreetiBhojan> {
  @override
  Widget build(BuildContext context) {
    const primaryColor = const Color.fromRGBO(0, 122, 164, 1);
    final textTheme = Theme.of(context).textTheme.apply(
          bodyColor: Colors.white,
          displayColor: Colors.white,
        );

    /// Variable extracted so cart and menu both share the same package
    /// repository. Menu should share the package repository with cart
    /// so when cart invalidates cache of package repository, menu gets
    /// updated as well.
    final packageForMenuRepository = NetworkPackagesForMenuRepository();

    return Provider<Cart>(
      create: (BuildContext context) => FoodCart(
        getCredentials,

        /// Cart should share the package repository with Menu so when
        /// cart invalidates cache of package repository, menu gets
        /// updated as well.
        packageForMenuRepository,
        NetworkMealsRepository(getCredentials),
      ),
      child: MaterialApp(
        title: 'Preeti Bhojan',
        theme: ThemeData(
          canvasColor: secondaryColor,
          cardColor: Colors.transparent,
          brightness: Brightness.dark,
          primarySwatch: Colors.blue,
          appBarTheme: AppBarTheme(
            color: secondaryColor,
          ),
          buttonTheme: ButtonThemeData(
            buttonColor: secondaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            textTheme: ButtonTextTheme.accent,
          ),
          dividerTheme: DividerThemeData(
            color: Colors.white,
            thickness: 2,
          ),
          primaryColor: primaryColor,
          accentColor: Colors.white,
          scaffoldBackgroundColor: primaryColor,
          textTheme: textTheme,
          inputDecorationTheme: const InputDecorationTheme(
            labelStyle: TextStyle(color: Colors.white),
            hintStyle: TextStyle(color: Colors.white),
          ),
          errorColor: Colors.white,
        ),
        routes: {
          '/login_or_register': (_) => LoginOrRegister(),
          '/register': (_) => Register(),
          '/schedule': (_) => Schedule(
                ScheduleViewModel(
                  packageRepository: NetworkPackageRepository(),
                  mealRepository: NetworkMealsRepository(getCredentials),
                  cuisineRepository: NetworkCuisineRepository(
                    getCredentials,
                  ),
                  deliveryLocationsRepository:
                      NetworkDeliveryLocationsRepository(getCredentials),
                  scheduleInfoRepository:
                      NetworkScheduleInfoRepository(getCredentials),
                  getCredentialsFunc: getCredentials,
                ),
                getCredentials,
                NetworkDeliverableRepository(getCredentials),
              ),
          '/home': (_) => MenuScreen(
                MenuViewModel(
                  menuRepository: NetworkMenuRepository(getCredentials),
                  getCredentialsFunc: getCredentials,

                  /// Menu should share the package repository with cart so when
                  /// cart invalidates cache of package repository, menu gets
                  /// updated as well.
                  packageRepository: packageForMenuRepository,
                  cuisineRepository: NetworkCuisineRepository(getCredentials),
                  deliveryLocationsRepository:
                      NetworkDeliveryLocationsRepository(getCredentials),
                  mealsRepository: NetworkMealsRepository(getCredentials),
                ),
                getCredentials,
              ),
          '/cancellation': (_) => Cancellation(
                getCredentials,
                CancellationViewModel(
                  NetworkCancellationInfoRepository(getCredentials),
                  NetworkCancelledMealsRepository(getCredentials),
                  NetworkDeliverableRepository(getCredentials),
                  DateTime.now(),
                ),
              ),
          '/resume': (_) => Resume(
                getCredentials,
                ResumeViewModel(
                  NetworkCancelledMealsRepository(getCredentials),
                  NetworkMealsRepository(getCredentials),
                  NetworkDeliverableRepository(getCredentials),
                  DateTime.now(),
                ),
          ),
          // go to initial page when starting
          '/': (_) =>
              InitialPage(
                getCredentialsAndAuthenticateCustomerFunc:
                getCredentialsAndAuthenticate,
                getCredentialsFunc: getCredentials,
              ),
          '/booking': (_) =>
              Booking(
                getCredentials,
                BookingViewModel(
                  NetworkPaymentInfoRepository(getCredentials),
                  NetworkDeliverableRepository(getCredentials),
                  DateTime.now(),
                ),
              ),
          '/profile': (_) =>
              Profile(
                getCredentialsFunc: getCredentials,
              ),
          '/payment': (_) =>
              Payment(
                getCredentials,
                    () => generateTransactionIDFromBackend(getCredentials),
              ),
          '/my_orders': (_) =>
              Orders(
                NetworkDeliverableRepository(getCredentials),
                NetworkPackageRepository(),
                NetworkMealsRepository(getCredentials),
                getCredentials,
              ),
          '/cart': (_) => CartScreen(
            getCredentials,
            NetworkDeliveryLocationsRepository(getCredentials),
            NetworkDeliverableRepository(getCredentials),
            NetworkPaymentInfoRepository(getCredentials),
              ),
        },
      ),
    );
  }
}

class InitialPage extends StatefulWidget {
  /// A function that gets credentials and authenticates them with server
  final GetCredentialsFunc getCredentialsAndAuthenticateCustomerFunc;

  /// A function that just gets credentials (no authentication). At first, I
  /// only had getCredentialsAndAuthenticate but then I realised it is wasteful
  /// because the backend will check if credentials are valid anyway
  final GetCredentialsFunc getCredentialsFunc;

  const InitialPage({
    Key key,
    @required this.getCredentialsAndAuthenticateCustomerFunc,
    @required this.getCredentialsFunc,
  }) : super(key: key);

  @override
  _InitialPageState createState() => _InitialPageState();
}

class _InitialPageState extends State<InitialPage> {
  Future<CustomerCredentials> customerCredentialsFuture;
  bool dialogShowed = false;

  Future<void> configFuture;

  @override
  void initState() {
    super.initState();
    customerCredentialsFuture =
        widget.getCredentialsAndAuthenticateCustomerFunc();
    configFuture = initConfig(NetworkConfigRepository());
  }

  final _log = Logger('InitialPageState');

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: configFuture,
      builder: (context, snapshot) {
        _log.info(snapshot);
        if (snapshot.hasError && !dialogShowed) {
          dialogShowed = true;
          SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
            showDialog(
              context: context,
              builder: (_) => SomethingWentWrong(),
            );
          });
        }

        if (snapshot.connectionState == ConnectionState.done &&
            !snapshot.hasError) {
          return FutureBuilder(
            future: customerCredentialsFuture,
            builder: (context, snapshot) {
              _log.info(snapshot);

              if (snapshot.hasError) {
                if (snapshot.error is PhoneNumberOrPasswordNotFound ||
                    snapshot.error is AuthenticationFailed) {
                  _log.warning(
                      'Could not get credentials or authentication failed ${snapshot.error}');

                  SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                    Navigator.of(context)
                        .pushReplacementNamed('/login_or_register');
                  });
                } else if (!dialogShowed) {
                  SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                    showDialog(
                        context: context, builder: (_) => SomethingWentWrong());
                  });
                  dialogShowed = true;
                }
              }

              if (snapshot.hasData) {
                SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                  Navigator.of(context).pushReplacementNamed('/home');
                });
              }

              return Scaffold(
                appBar: CustomScaffold.appBar,
                body: snapshot.hasError
                    ? CustomBackground(
                        child: Container(),
                      )
                    : CustomBackground(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      ),
              );
            },
          );
        }

        return Scaffold(
          appBar: CustomScaffold.appBar,
          body: snapshot.hasError
              ? CustomBackground(
                  child: Container(),
                )
              : CustomBackground(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
        );
      },
    );
  }
}
