import 'package:flutter/foundation.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';
import 'package:preetibhojan/util/string.dart';
import 'package:validators/validators.dart';

class OfficeAddressViewModel with ChangeNotifier {
  final AreaRepository repository;

  String Function() tower;
  String Function() floor;
  String Function() officeNumber;
  String Function() company;

  OfficeAddressViewModel({@required this.repository});

  List<Area> areas;

  Future<void> fetchData() async {
    areas = await repository.area();

    if (currentArea == null)
      currentArea = areas[0];
  }


  Area _currentArea;

  Area get currentArea => _currentArea;

  set currentArea(value) {
    _currentArea = value;
    notifyListeners();
  }

  bool isTowerValid() => isAlphanumeric(tower().trim());

  bool isFloorValid() =>
      isNumeric(floor().trim()) && int.parse(floor().trim()) > 0;

  bool isOfficeNumberValid() =>
      isAlphaNumericUnderScoreOrSpace(officeNumber().trim());

  bool isCompanyValid() => isAlphaNumericUnderScoreOrSpace(company().trim());

  bool isValid() =>
      isTowerValid() &&
      isFloorValid() &&
      isOfficeNumberValid() &&
      isCompanyValid();

  @override
  String toString() {
    return '${officeNumber()}, ${floor()}, ${tower()}, $currentArea, ${company()}';
  }
}
