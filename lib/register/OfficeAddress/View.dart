import 'package:flutter/material.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/register/OfficeAddress/ViewModel.dart';
import 'package:preetibhojan/util/ui/CardTile.dart';
import 'package:provider/provider.dart';

class OfficeAddress extends StatefulWidget {
  @override
  _OfficeAddressState createState() => _OfficeAddressState();
}

class _OfficeAddressState extends State<OfficeAddress>
    with AutomaticKeepAliveClientMixin {
  final _controllers = {
    'tower': TextEditingController(),
    'floor': TextEditingController(),
    'officeNumber': TextEditingController(),
    'company': TextEditingController(),
  };

  @override
  void dispose() {
    _controllers.forEach((_, controller) {
      controller.dispose();
    });

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final _viewModel = Provider.of<OfficeAddressViewModel>(context);

    _viewModel.tower = () => _controllers['tower'].text;
    _viewModel.floor = () => _controllers['floor'].text;
    _viewModel.officeNumber = () => _controllers['officeNumber'].text;
    _viewModel.company = () => _controllers['company'].text;

    return CardTile(
      title: Text(
        'Office Address',
        style: Theme.of(context).textTheme.headline6,
      ),
      child: Form(
        autovalidate: true,
        child: FutureBuilder(
          future: _viewModel.fetchData(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                !snapshot.hasError) {
              return Column(
                children: <Widget>[
                  DropdownButtonFormField<Area>(
                    isDense: true,
                    decoration: InputDecoration(
                      icon: Icon(Icons.map),
                      labelText: 'Area',
                    ),
                    items: _viewModel.areas.map((Area area) {
                      return DropdownMenuItem<Area>(
                        child: Text(area.name),
                        value: area,
                      );
                    }).toList(),
                    value: _viewModel.currentArea,
                    onChanged: (value) => _viewModel.currentArea = value,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.location_on),
                      labelText: 'Tower',
                    ),
                    validator: (_) =>
                    _viewModel.isTowerValid()
                        ? null
                        : 'Tower must be alphanumeric',
                    controller: _controllers['tower'],
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.domain),
                      labelText: 'Floor',
                    ),

                    // Pretty much useless to have a validator because the virtual
                    // keyboard will do that for us but still ...
                    // Oops is guess dart programmers only use .. ;P

                    validator: (_) => _viewModel.isFloorValid()
                        ? null
                        : 'Floor must be a positive number',
                    controller: _controllers['floor'],
                    keyboardType: TextInputType.number,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.domain),
                      labelText: 'Office No.',
                    ),
                    validator: (_) => _viewModel.isOfficeNumberValid()
                        ? null
                        : 'Office No. must contain only a-z, A-Z, 0-9, _ or space',
                    controller: _controllers['officeNumber'],
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      icon: Icon(Icons.computer),
                      labelText: 'Company',
                    ),
                    validator: (_) => _viewModel.isCompanyValid()
                        ? null
                        : 'Company must contain only a-z, A-Z, 0-9, _ or space',
                    controller: _controllers['company'],
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Container();
            }

            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
