import 'package:flutter/foundation.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';

abstract class OfficeAddressRepository {
  List<Area> get areas;
}

class StubOfficeAddressRepository implements OfficeAddressRepository {
  final List<Area> input;

  StubOfficeAddressRepository({@required this.input});

  @override
  List<Area> get areas => input;
}
