import 'package:validators/validators.dart';

/// Fields containing text input from user needs should not be stored directly as a string, This is because if the text is updated,
/// you will need to add the listener for each controller, A better way according to me is to expose a function that the view can
/// set that will return the current value of text from controller. It is easier to implement for both view model and view

class CustomerViewModel {
  String Function() firstName;
  String Function() lastName;
  String Function() email;
  String Function() phoneNumber;
  String Function() password;

  bool isFirstNameValid() => isAlpha(firstName().trim());

  bool isLastNameValid() => isAlpha(lastName().trim());

  /// Untested because it is validators responsibility to test isEmail
  bool isEmailValid() => isEmail(email().trim());

  bool isPhoneNumberValid() => matches(phoneNumber().trim(), r'^\d{10}$');

  bool isPasswordValid() {
    final temp = password().trim().length;
    return temp >= 8 && temp <= 70;
  }

  bool isValid() =>
      isFirstNameValid() &&
          isLastNameValid() &&
      isEmailValid() &&
      isPhoneNumberValid() &&
          isPasswordValid();

  @override
  String toString() {
    return 'First name: ${firstName()}, Lastname: ${lastName()}, Email: ${email()}, Phone no. ${phoneNumber()}';
  }
}
