import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/register/CuisineInformation/Repositories.dart';
import 'package:preetibhojan/register/CuisineInformation/View.dart';
import 'package:preetibhojan/register/CuisineInformation/ViewModel.dart';
import 'package:preetibhojan/register/MakeRequest.dart';
import 'package:preetibhojan/util/ui/CustomBackground.dart';
import 'package:preetibhojan/util/ui/CustomScaffold.dart';
import 'package:preetibhojan/util/ui/InvalidInputDialog.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:provider/provider.dart';

import '../common/delivery_location/Repositories.dart';
import 'CustomerInformation/View.dart';
import 'CustomerInformation/ViewModel.dart';
import 'HomeAddress/View.dart';
import 'HomeAddress/ViewModel.dart';
import 'OfficeAddress/View.dart';
import 'OfficeAddress/ViewModel.dart';

/// BasicInformation only contains textual input. Hence, it does not need
/// change notifier provider because the view sets the state of the view model
///
/// HomeAddress and CustomerAddress have both drop down menus and textual input.
/// Hence, they need change notifier provider to update the values of dropdown
///
/// In CustomerInformation, the view is basically dumb. It only outputs the state
/// of its view model. So it definitely needs change notifier provider

const CUSTOMER_ALREADY_EXISTS = 2;

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool homeAddressEnabled = false, officeAddressEnabled = false;
  final _log = Logger('Register');
  final pageController = PageController();

  bool showCircularProgressIndicator = false;

  @override
  void dispose() {
    super.dispose();
    pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<CustomerViewModel>(
          create: (_) => CustomerViewModel(),
        ),
        ChangeNotifierProvider<HomeAddressViewModel>(
          create: (_) {
            return HomeAddressViewModel(
              repository: NetworkAreaRepository(),
            );
          },
        ),
        ChangeNotifierProvider<OfficeAddressViewModel>(
          create: (_) {
            return OfficeAddressViewModel(
              repository: NetworkAreaRepository(),
            );
          },
        ),
        ChangeNotifierProvider<CuisineInformationViewModel>(
          create: (BuildContext context) => CuisineInformationViewModel(
            NetworkCuisineRepository(),
          ),
        ),
      ],
      child: Scaffold(
        appBar: CustomScaffold.appBar,
        body: SafeArea(
          child: CustomBackground(
            showCircularProgressIndicator: showCircularProgressIndicator,
            child: PageView(
              controller: pageController,
              children: <Widget>[
                ListView(
                  children: <Widget>[
                    CustomerInformation(),
                    Consumer<CuisineInformationViewModel>(
                      builder: (context, cuisineInformationViewModel, _) {
                        return FutureBuilder(
                          future: cuisineInformationViewModel.fetchCuisines(),
                          builder: (context, snapshot) {
                            if (snapshot.hasError) {
                              SchedulerBinding.instance
                                  .addPostFrameCallback((timeStamp) {
                                showDialog(
                                  context: context,
                                  builder: (_) => SomethingWentWrong(),
                                );
                              });

                              return Container();
                            }

                            if (snapshot.connectionState ==
                                ConnectionState.done) {
                              return CuisineInformation();
                            }

                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          },
                        );
                      },
                    ),
                    Consumer<CustomerViewModel>(
                      builder: (context, customerViewModel, _) {
                        return Column(
                          children: <Widget>[
                            RaisedButton(
                              child: Text('Next'),
                              onPressed: () {
                                if (customerViewModel.isValid()) {
                                  pageController.nextPage(
                                    duration: Duration(milliseconds: 250),
                                    curve: Curves.easeInOut,
                                  );
                                  return;
                                }

                                showDialog(
                                    context: context,
                                    builder: (_) => InvalidInputDialog());
                              },
                            ),
                          ],
                        );
                      },
                    ),
                  ],
                ),
                ListView(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(12),
                      child: Center(
                        child: Text(
                          'Delivery Address',
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: CheckboxListTile(
                        onChanged: (bool value) {
                          setState(() {
                            homeAddressEnabled = value;
                          });
                        },
                        value: homeAddressEnabled,
                        title: Text(
                          'Home Address',
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                    ),
                    Visibility(
                      visible: homeAddressEnabled,
                      child: HomeAddress(),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                      child: CheckboxListTile(
                        onChanged: (bool value) {
                          setState(() {
                            officeAddressEnabled = value;
                          });
                        },
                        value: officeAddressEnabled,
                        title: Text(
                          'Office Address',
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ),
                    ),
                    Visibility(
                      visible: officeAddressEnabled,
                      child: OfficeAddress(),
                    ),

                    /// Any random view model. I only care about the context
                    Consumer<CuisineInformationViewModel>(
                      builder: (context, _, __) {
                        return Column(
                          children: <Widget>[
                            RaisedButton(
                              child: Text(
                                'Register',
                              ),
                              onPressed: () =>
                                  validateInputAndRegister(context),
                            ),
                          ],
                        );
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void validateInputAndRegister(BuildContext context) async {
    final customerViewModel =
        Provider.of<CustomerViewModel>(context, listen: false);
    final homeAddressViewModel =
        Provider.of<HomeAddressViewModel>(context, listen: false);
    final officeAddressViewModel =
        Provider.of<OfficeAddressViewModel>(context, listen: false);
    final cuisineInformationViewModel =
        Provider.of<CuisineInformationViewModel>(context, listen: false);

    _log.info('Home address enabled: $homeAddressEnabled');
    _log.info('Office address enabled: $officeAddressEnabled');

    if (!homeAddressEnabled && !officeAddressEnabled) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Oops'),
          content: Text('Please select either home address or office address'),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );

      return;
    }

    if (!customerViewModel.isValid() ||
        (officeAddressEnabled && !officeAddressViewModel.isValid()) ||
        (homeAddressEnabled && !homeAddressViewModel.isValid())) {
      showDialog(
        context: context,
        builder: (_) => InvalidInputDialog(),
      );

      return;
    }

    setState(() {
      showCircularProgressIndicator = true;
    });

    try {
      await register(
        customerViewModel,
        homeAddressEnabled ? homeAddressViewModel : null,
        officeAddressEnabled ? officeAddressViewModel : null,
        cuisineInformationViewModel,
      );

      final storage = FlutterSecureStorage();

      await storage.write(
        key: 'phone_number',
        value: customerViewModel.phoneNumber().trim(),
      );

      await storage.write(
        key: 'password',
        value: customerViewModel.password().trim(),
      );

      await storage.write(
        key: 'name',
        value: customerViewModel.firstName().trim(),
      );

      SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
        /// Seems to be that when showDialog is called, the register button is
        /// already disposed. Hence using the context in the input doesn't seem
        /// to work

        showDialog(
          context: this.context,
          builder: (context) => AlertDialog(
            title: Text('Welcome to Preeti Bhojan'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Your Registration is Successful.',
                  ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                      child: Text(
                        'If you require any Meal on a Regular Basis, please Schedule '
                            'your Meal. Do you want to Schedule your Meal now?',
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                      child: Text(
                        'Please check your mail for terms and conditions',
                        textAlign: TextAlign.justify,
                        style: TextStyle(color: Colors.lime),
                      ),
                    ),
                  ],
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Yes'),
                    onPressed: () {
                      Navigator.of(context)
                          .pushNamedAndRemoveUntil('/home', (route) => false);
                      Navigator.of(context).pushNamed('/schedule');
                    },
                  ),
                  FlatButton(
                    child: Text('No'),
                    onPressed: () {
                      Navigator.of(context)
                          .pushNamedAndRemoveUntil('/home', (route) => false);
                    },
                  ),
                ],
              ),
        );
      });
    } catch (on, stacktrace) {
      if (!(on is Result)) {
        _log.severe('Unknown exception', on, stacktrace);
        throw on;
      }

      final result = on as Result;

      if (result.errorCode == CUSTOMER_ALREADY_EXISTS) {
        SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
          showDialog(
            context: context,
            builder: (context) =>
                AlertDialog(
                  title: Text('Oops'),
                  content: Text(
                    'A customer with the given phone number '
                        'already exists. Try to log in',
                  ),
                  actions: <Widget>[
                    FlatButton(
                      onPressed: () =>
                          Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_or_register',
                                (route) => false,
                          ),
                      child: Text('Log in'),
                    )
                  ],
                ),
          );
        });
        _log.info('Customer already exists');
        return;
      }

      _log.severe(
          'Unknown error returned by backend. Error code: ${result.errorCode}');

      showDialog(
        context: context,
        builder: (_) => SomethingWentWrong(),
      );
    } finally {
      setState(() {
        showCircularProgressIndicator = false;
      });
    }
  }
}
