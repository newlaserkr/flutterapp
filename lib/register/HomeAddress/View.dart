import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:preetibhojan/util/ui/CardTile.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:provider/provider.dart';

import '../../common/delivery_location/Models.dart';
import 'ViewModel.dart';

class HomeAddress extends StatefulWidget {
  @override
  _HomeAddressState createState() => _HomeAddressState();
}

class _HomeAddressState extends State<HomeAddress>
    with AutomaticKeepAliveClientMixin {
  final _controllers = {
    'society': TextEditingController(),
    'building': TextEditingController(),
    'flatNumber': TextEditingController(),
  };

  @override
  void dispose() {
    _controllers.forEach((_, controller) {
      controller.dispose();
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final _viewModel = Provider.of<HomeAddressViewModel>(context);

    _viewModel.society = () => _controllers['society'].text;
    _viewModel.building = () => _controllers['building'].text;
    _viewModel.flatNumber = () => _controllers['flatNumber'].text;

    final areas = _viewModel.fetchData();

    return CardTile(
      title: Text(
        'Home Address',
        style: Theme.of(context).textTheme.headline6,
      ),
      child: Form(
        autovalidate: true,
        child: FutureBuilder(
          future: areas,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                !snapshot.hasError) {
              return Column(
                children: <Widget>[
                  DropdownButtonFormField<Area>(
                    isDense: true,
                    value: _viewModel.currentArea,
                    decoration: InputDecoration(
                      icon: Icon(Icons.map),
                      labelText: 'Area',
                    ),
                    items: _viewModel.area.map((Area area) {
                      return DropdownMenuItem<Area>(
                        child: Text(area.name),
                        value: area,
                      );
                    }).toList(),
                    onChanged: (value) {
                      _viewModel.currentArea = value;
                    },
                  ),
                  DropdownButtonFormField<Locality>(
                    isDense: true,
                    value: _viewModel.currentLocality,
                    decoration: InputDecoration(
                      icon: Icon(Icons.map),
                      labelText: 'Locality',
                    ),
                    items: _viewModel.locality
                        .where((element) =>
                    element.areaID == _viewModel.currentArea.id)
                        .map((Locality locality) {
                      return DropdownMenuItem<Locality>(
                        child: Text(locality.name),
                        value: locality,
                      );
                    }).toList(),
                    onChanged: (value) {
                      _viewModel.currentLocality = value;
                    },
                  ),
                  TextFormField(
                    controller: _controllers['society'],
                    validator: (_) =>
                    _viewModel.isSocietyValid()
                        ? null
                        : 'Society must only contain a-z, A-Z, 0-9, _ and space',
                    decoration: InputDecoration(
                      icon: Icon(Icons.location_on),
                      labelText: 'Society',
                    ),
                  ),
                  TextFormField(
                    controller: _controllers['building'],
                    validator: (_) =>
                    _viewModel.isBuildingValid()
                        ? null
                        : 'Building must only contain a-z, A-Z, 0-9, _ and space',
                    decoration: InputDecoration(
                      icon: Icon(Icons.domain),
                      labelText: 'Building',
                    ),
                  ),
                  TextFormField(
                    controller: _controllers['flatNumber'],
                    validator: (_) =>
                    _viewModel.isFlatNumberValid()
                        ? null
                        : 'Flat no. must only contain a-z, A-Z, 0-9, _ and space',
                    decoration: InputDecoration(
                      icon: Icon(Icons.domain),
                      labelText: 'Flat No.',
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              SchedulerBinding.instance.addPostFrameCallback((_) {
                showDialog(
                  context: context,
                  builder: (_) => SomethingWentWrong(),
                );
              });

              print(snapshot.error);
              return Container();
            }

            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
