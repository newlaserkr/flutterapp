import 'package:flutter/foundation.dart';
import 'package:preetibhojan/util/string.dart';

import '../../common/delivery_location/Models.dart';
import '../../common/delivery_location/Repositories.dart';

class HomeAddressViewModel with ChangeNotifier {
  final AreaRepository repository;

  HomeAddressViewModel({@required this.repository});

  String Function() building;
  String Function() flatNumber;
  String Function() society;

  List<Area> area;
  List<Locality> locality;

  Future<void> fetchData() async {
    area = await repository.area();

    if (currentArea == null)
      currentArea = area[0];

    locality = await repository.locality(currentArea.id);

    if (currentLocality == null || currentLocality.areaID != currentArea.id)
      currentLocality = locality[0];
  }

  Area _currentArea;
  Locality _currentLocality;

  Area get currentArea => _currentArea;

  Locality get currentLocality => _currentLocality;

  set currentArea(value) {
    _currentArea = value;
    notifyListeners();
  }

  set currentLocality(value) {
    _currentLocality = value;
    notifyListeners();
  }

  bool isSocietyValid() => isAlphaNumericUnderScoreOrSpace(society());

  bool isBuildingValid() => isAlphaNumericUnderScoreOrSpace(building());

  bool isFlatNumberValid() => isAlphaNumericUnderScoreOrSpace(flatNumber());

  @override
  String toString() {
    return '${building()}, ${flatNumber()}, ${society()}, $currentArea, $currentLocality';
  }

  bool isValid() =>
      isSocietyValid() && isBuildingValid() && isFlatNumberValid();
}
