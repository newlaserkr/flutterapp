import 'package:flutter/material.dart';

import 'Model.dart';
import 'Repositories.dart';

class CuisineInformationViewModel with ChangeNotifier {
  final CuisineRepository cuisineRepository;

  CuisineInformationViewModel(this.cuisineRepository);

  Future<List<Cuisine>> fetchCuisines() async {
    final temp = await cuisineRepository.cuisines;
    if (requiredCuisine == null) {
      requiredCuisine = temp[0];
    }

    return temp;
  }

  Cuisine _requiredCuisine;

  Cuisine get requiredCuisine => _requiredCuisine;

  set requiredCuisine(value) {
    _requiredCuisine = value;
    notifyListeners();
  }
}
