import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:preetibhojan/common/Config.dart';

import 'Model.dart';

abstract class CuisineRepository {
  Future<List<Cuisine>> get cuisines;
}

class StubCuisines implements CuisineRepository {
  final List<Cuisine> input;

  StubCuisines({@required this.input});

  @override
  Future<List<Cuisine>> get cuisines => Future.value(input);
}

class NetworkCuisineRepository implements CuisineRepository {
  var _cache = List<Cuisine>();

  @override
  Future<List<Cuisine>> get cuisines async {
    if (_cache.length > 0) return _cache;

    final response = await http.get(base_url +
            '/cuisines') /*.timeout(
          Duration(seconds: 5),
        )*/;

    if (response.statusCode == 200) {
      _cache = (jsonDecode(response.body) as List)
          .map((json) => Cuisine.fromJson(json))
          .toList();
      return _cache;
    }
    throw Exception('Failed to load cuisines');
  }
}
