class Cuisine {
  final int cuisineID;
  final String name;

  Cuisine(this.cuisineID, this.name);

  @override
  String toString() {
    return 'ID: $cuisineID Name: $name';
  }

  factory Cuisine.fromJson(Map<String, dynamic> json) {
    return Cuisine(
      json['id'],
      json['name'],
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Cuisine &&
          runtimeType == other.runtimeType &&
          cuisineID == other.cuisineID;

  @override
  int get hashCode => cuisineID.hashCode;
}
