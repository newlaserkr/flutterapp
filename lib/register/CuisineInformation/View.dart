import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:logging/logging.dart';
import 'package:preetibhojan/register/CuisineInformation/ViewModel.dart';
import 'package:preetibhojan/util/ui/CardTile.dart';
import 'package:preetibhojan/util/ui/SomethingWentWrong.dart';
import 'package:provider/provider.dart';

import 'Model.dart';

class CuisineInformation extends StatelessWidget {
  final _log = Logger('CuisineInformation');

  @override
  Widget build(BuildContext context) {
    return Consumer<CuisineInformationViewModel>(
      builder: (context, viewModel, Widget child) {
        return FutureBuilder(
          future: viewModel.fetchCuisines(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              _log.severe('Could not fetch cuisines');

              SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
                showDialog(
                  context: context,
                  builder: (_) => SomethingWentWrong(),
                );
              });

              return Container();
            }

            if (snapshot.hasData) {
              _log.info('Fetched cuisines');

              return CardTile(
                title: Text(
                  'Select Cuisine',
                  style: Theme.of(context).textTheme.headline6,
                ),
                child: DropdownButtonFormField<Cuisine>(
                  isDense: true,
                  value: viewModel.requiredCuisine,
                  decoration: InputDecoration(
                    icon: Icon(Icons.fastfood),
                    labelText: 'Cuisine',
                  ),
                  onChanged: (value) => viewModel.requiredCuisine = value,
                  items: snapshot.data
                      .map<DropdownMenuItem<Cuisine>>((Cuisine cuisine) {
                    return DropdownMenuItem<Cuisine>(
                      child: Text(cuisine.name),
                      value: cuisine,
                    );
                  }).toList(),
                ),
              );
            }

            return Container();
          },
        );
      },
    );
  }
}
