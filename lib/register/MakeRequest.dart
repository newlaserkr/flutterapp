import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/register/CuisineInformation/ViewModel.dart';
import 'package:preetibhojan/register/HomeAddress/ViewModel.dart';
import 'package:preetibhojan/register/OfficeAddress/ViewModel.dart';

import 'CustomerInformation/ViewModel.dart';

/// I have given numbers instead of long names because this is only expected to be used internally

/// Note to self: MAke sure to trim textual inputs from user
Map<String, dynamic> makeRequest1(CustomerViewModel customerViewModel) {
  return {
    'first_name': customerViewModel.firstName().trim(),
    'last_name': customerViewModel.lastName().trim(),
    'email': customerViewModel.email().trim(),
    'phone_number': customerViewModel.phoneNumber().trim(),
    'password': customerViewModel.password().trim()
  };
}

Map<String, dynamic> makeRequest2(HomeAddressViewModel homeAddressViewModel) {
  return {
    'flat_number': homeAddressViewModel.flatNumber().trim(),
    'building': homeAddressViewModel.building().trim(),
    'society': homeAddressViewModel.society().trim(),
    'locality_id': homeAddressViewModel.currentLocality.id,
  };
}

Map<String, dynamic> makeRequest3(
    OfficeAddressViewModel officeAddressViewModel) {
  return {
    'office_number': officeAddressViewModel.officeNumber().trim(),
    'floor': int.parse(officeAddressViewModel.floor().trim()),
    'tower': officeAddressViewModel.tower().trim(),
    'area_id': officeAddressViewModel.currentArea.id,
    'company': officeAddressViewModel.company().trim(),
  };
}

/// Untested because easy to reason about. It just uses the above functions and combines the output
Map<String, dynamic> makeRequest(
  CustomerViewModel customerViewModel,
  HomeAddressViewModel homeAddressViewModel,
  OfficeAddressViewModel officeAddressViewModel,
  CuisineInformationViewModel cuisineInformationViewModel,
) {
  /// make sure if home address is null, office address is not null and vice versa
  assert(homeAddressViewModel != null || officeAddressViewModel != null);

  final temp = {
    'customer': makeRequest1(customerViewModel),
    'cuisine_id': cuisineInformationViewModel.requiredCuisine.cuisineID,
  };

  if (homeAddressViewModel != null) {
    temp['home_address'] = makeRequest2(homeAddressViewModel);
  }

  if (officeAddressViewModel != null) {
    temp['office_address'] = makeRequest3(officeAddressViewModel);
  }

  return temp;
}

class Result {
  final bool success;
  final int errorCode;

  Result({@required this.success, @required this.errorCode});

  @override
  String toString() => '$success, $errorCode';
}

Future<Result> register(
  CustomerViewModel customerViewModel,
  HomeAddressViewModel homeAddressViewModel,
  OfficeAddressViewModel officeAddressViewModel,
  CuisineInformationViewModel cuisineInformationViewModel,
) async {
  final requestBody = makeRequest(
    customerViewModel,
    homeAddressViewModel,
    officeAddressViewModel,
    cuisineInformationViewModel,
  );

  var response;
  var url =
  /*(mealInformationViewModel.customerType == CustomerType.NeedBased)
      ? base_url + '/register/need_based'
      :*/
  base_url + '/register';

  print(requestBody);

  response = await http.post(
    url,
    headers: {'Content-Type': 'application/json'},
    body: jsonEncode(requestBody),
  );

  if (response.statusCode == 200) {
    return Result(
      success: true,
      errorCode: null,
    );
  }

  throw Result(
    success: false,
    errorCode: jsonDecode(response.body)['error'],
  );
}
