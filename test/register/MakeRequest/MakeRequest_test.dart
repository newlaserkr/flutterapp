import 'package:flutter_test/flutter_test.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/register/CuisineInformation/Model.dart';
import 'package:preetibhojan/register/CuisineInformation/ViewModel.dart';
import 'package:preetibhojan/register/CustomerInformation/ViewModel.dart';
import 'package:preetibhojan/register/HomeAddress/ViewModel.dart';
import 'package:preetibhojan/register/MakeRequest.dart';
import 'package:preetibhojan/register/OfficeAddress/ViewModel.dart';

/// Note to self: Make sure to add \t to textual inputs to ensure that makeRequestX trims them

void main() {
  group('Individual test', () {
    test('Test customer information is serialized correctly', () {
      final customerViewModel = CustomerViewModel();
      customerViewModel.firstName = () => 'abc\t';
      customerViewModel.lastName = () => 'def\t';
      customerViewModel.email = () => 'com\t';
      customerViewModel.phoneNumber = () => '123\t';
      customerViewModel.password = () => 'pass\t';

      expect(makeRequest1(customerViewModel), {
        'first_name': 'abc',
        'last_name': 'def',
        'email': 'com',
        'phone_number': '123',
        'password': 'pass'
      });
    });

    test('Test home address is serialized correctly', () {
      final locality = Locality(1, 'xyz', 1);

      final homeAddressViewModel = HomeAddressViewModel(repository: null);

      homeAddressViewModel.currentLocality = locality;
      homeAddressViewModel.flatNumber = () => 'abc\t';
      homeAddressViewModel.building = () => 'def\t';
      homeAddressViewModel.society = () => 'soc\t';

      expect(makeRequest2(homeAddressViewModel), {
        'flat_number': 'abc',
        'building': 'def',
        'society': 'soc',
        'locality_id': locality.id,
      });
    });

    test('Test office address is serialized correctly', () {
      final area = Area(1, 'area');

      final officeAddressViewModel = OfficeAddressViewModel(repository: null);

      officeAddressViewModel.currentArea = area;
      officeAddressViewModel.tower = () => 'abc\t';
      officeAddressViewModel.floor = () => '1\t';
      officeAddressViewModel.officeNumber = () => 'def\t';
      officeAddressViewModel.company = () => 'ghi';

      expect(makeRequest3(officeAddressViewModel), {
        'office_number': 'def',
        'floor': 1,
        'tower': 'abc',
        'area_id': 1,
        'company': 'ghi',
      });
    });
  });

  group('office address and home address combo', () {
    test(
        'make request throws assertion if both office address and home address is null',
        () {
      expect(
        () => makeRequest(
          CustomerViewModel(),
          null,
          null,
          CuisineInformationViewModel(null),
        ),
        throwsAssertionError,
      );
    });

    final customerViewModel = CustomerViewModel();
    customerViewModel.firstName = () => 'abc\t';
    customerViewModel.lastName = () => 'def\t';
    customerViewModel.email = () => 'com\t';
    customerViewModel.phoneNumber = () => '123\t';
    customerViewModel.password = () => 'pass\t';

    final cuisineInformationViewModel = CuisineInformationViewModel(null);
    cuisineInformationViewModel.requiredCuisine = Cuisine(1, '');

    test('make request works when office address is null', () {
      final locality = Locality(1, 'xyz', 1);

      final homeAddressViewModel = HomeAddressViewModel(repository: null);

      homeAddressViewModel.currentLocality = locality;
      homeAddressViewModel.flatNumber = () => 'abc\t';
      homeAddressViewModel.building = () => 'def\t';
      homeAddressViewModel.society = () => 'soc\t';

      expect(
        makeRequest(customerViewModel, homeAddressViewModel, null,
            cuisineInformationViewModel),
        equals(
          ({
            'customer': {
              'first_name': 'abc',
              'last_name': 'def',
              'email': 'com',
              'phone_number': '123',
              'password': 'pass'
            },
            'home_address': {
              'flat_number': 'abc',
              'building': 'def',
              'society': 'soc',
              'locality_id': locality.id,
            },
            'cuisine_id': 1,
          }),
        ),
      );
    });

    test('make request works when home address is null', () {
      final customerViewModel = CustomerViewModel();
      customerViewModel.firstName = () => 'abc\t';
      customerViewModel.lastName = () => 'def\t';
      customerViewModel.email = () => 'com\t';
      customerViewModel.phoneNumber = () => '123\t';
      customerViewModel.password = () => 'pass\t';

      final area = Area(1, 'area');

      final officeAddressViewModel = OfficeAddressViewModel(repository: null);

      officeAddressViewModel.currentArea = area;
      officeAddressViewModel.tower = () => 'abc\t';
      officeAddressViewModel.floor = () => '1\t';
      officeAddressViewModel.officeNumber = () => 'def\t';
      officeAddressViewModel.company = () => 'ghi';

      final cuisineInformationViewModel = CuisineInformationViewModel(null);
      cuisineInformationViewModel.requiredCuisine = Cuisine(1, '');

      expect(
        makeRequest(customerViewModel, null, officeAddressViewModel,
            cuisineInformationViewModel),
        equals(
          ({
            'customer': {
              'first_name': 'abc',
              'last_name': 'def',
              'email': 'com',
              'phone_number': '123',
              'password': 'pass'
            },
            'office_address': {
              'office_number': 'def',
              'floor': 1,
              'tower': 'abc',
              'area_id': 1,
              'company': 'ghi',
            },
            'cuisine_id': 1,
          }),
        ),
      );
    });

    test(
        'make request works when neither home address nor office address '
        'is null', () {
      final customerViewModel = CustomerViewModel();
      customerViewModel.firstName = () => 'abc\t';
      customerViewModel.lastName = () => 'def\t';
      customerViewModel.email = () => 'com\t';
      customerViewModel.phoneNumber = () => '123\t';
      customerViewModel.password = () => 'pass\t';

      final area = Area(1, 'area');

      final officeAddressViewModel = OfficeAddressViewModel(repository: null);

      officeAddressViewModel.currentArea = area;
      officeAddressViewModel.tower = () => 'abc\t';
      officeAddressViewModel.floor = () => '1\t';
      officeAddressViewModel.officeNumber = () => 'def\t';
      officeAddressViewModel.company = () => 'ghi';

      final cuisineInformationViewModel = CuisineInformationViewModel(null);
      cuisineInformationViewModel.requiredCuisine = Cuisine(1, '');

      final homeAddressViewModel = HomeAddressViewModel(repository: null);

      final locality = Locality(1, 'xyz', 1);

      homeAddressViewModel.currentLocality = locality;
      homeAddressViewModel.flatNumber = () => 'abc\t';
      homeAddressViewModel.building = () => 'def\t';
      homeAddressViewModel.society = () => 'soc\t';

      expect(
        makeRequest(customerViewModel, homeAddressViewModel,
            officeAddressViewModel, cuisineInformationViewModel),
        equals(
          ({
            'customer': {
              'first_name': 'abc',
              'last_name': 'def',
              'email': 'com',
              'phone_number': '123',
              'password': 'pass'
            },
            'office_address': {
              'office_number': 'def',
              'floor': 1,
              'tower': 'abc',
              'area_id': 1,
              'company': 'ghi',
            },
            'cuisine_id': 1,
            'home_address': {
              'flat_number': 'abc',
              'building': 'def',
              'society': 'soc',
              'locality_id': locality.id,
            },
          }),
        ),
      );
    });
  });
}
