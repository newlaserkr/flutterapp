import 'package:flutter_test/flutter_test.dart';
import 'package:preetibhojan/register/CustomerInformation/ViewModel.dart';

import 'Input.dart';

void main() {
  group('Basic information validation', () {
    var viewModel = CustomerViewModel();

    group('individaul input validation', () {
      test('first name should only contain alphabets', () {
        alphabetic.forEach((firstName, isValid) {
          viewModel.firstName = () => firstName;
          expect(viewModel.isFirstNameValid(), equals(isValid));
        });
      });

      test('last name should only contain alphabets', () {
        alphabetic.forEach((lastName, isValid) {
          viewModel.lastName = () => lastName;
          expect(viewModel.isLastNameValid(), equals(isValid));
        });
      });

      test('phone number may be 10 digits only', () {
        phoneNumbers.forEach((phoneNumber, isValid) {
          viewModel.phoneNumber = () => phoneNumber;
          expect(viewModel.isPhoneNumberValid(), equals(isValid));
        });
      });

      test('password is valid if it contains at least 8 characters', () {
        viewModel.password = () => '1234567890';
        expect(viewModel.isPasswordValid(), isTrue);

        viewModel.password = () => '12345678';
        expect(viewModel.isPasswordValid(), isTrue);

        viewModel.password = () => '123456';
        expect(viewModel.isPasswordValid(), isFalse);
      });

      test('password is invalid it it contains more than 70 characters', () {
        var password = '';
        // make a 71 character password
        for (var i = 0; i <= 71; i++) {
          password += 'a';
        }

        viewModel.password = () => password;

        expect(viewModel.isPasswordValid(), isFalse);
      });
    });

    group(
        'isValid should return false if at least one of the fields in invalid',
            () {
          test(
              'isValid should return false if first name is invalid even though all other fields are correct',
                  () {
        viewModel.firstName = () => '0';
        viewModel.lastName = () => 'a';
        viewModel.email = () => 'a';
        viewModel.phoneNumber = () => '0987654321';
        viewModel.password = () => '1234567890';

        expect(viewModel.isValid(), equals(false));
      });

          test(
              'isValid should return false if last name is invalid even though all other fields are correct',
                  () {
        viewModel.firstName = () => 'a';
        viewModel.lastName = () => '0';
        viewModel.email = () => 'a';
        viewModel.phoneNumber = () => '0987654321';
        viewModel.password = () => '1234567890';

        expect(viewModel.isValid(), equals(false));
      });

          test(
              'isValid should return false if company is invalid even though all other fields are correct',
                  () {
        viewModel.firstName = () => 'a';
        viewModel.lastName = () => 'a';
        viewModel.email = () => '!@';
        viewModel.phoneNumber = () => '0987654321';
        viewModel.password = () => '1234567890';

        expect(viewModel.isValid(), equals(false));
      });

          test(
              'isValid should return false if phone number is invalid even though all other fields are correct',
                  () {
        viewModel.firstName = () => 'a';
        viewModel.lastName = () => 'a';
        viewModel.email = () => 'a';
        viewModel.phoneNumber = () => '987654321';
        viewModel.password = () => '1234567890';

        expect(viewModel.isValid(), equals(false));
      });

          test(
              'isValid should return false if password is invalid even though all other fields are correct', () {
            viewModel.firstName = () => 'a';
            viewModel.lastName = () => 'a';
        viewModel.email = () => 'a';
        viewModel.phoneNumber = () => '9876543210';
            viewModel.password = () => '123456';

            expect(viewModel.isValid(), equals(false));
          });
    });
  });
}
