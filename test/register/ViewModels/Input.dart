var alphabetic = {
  'abc': true,
  'abc ': true,
  'abc \t\r\n': true,
  'a bc': false,
  '123': false,
  'abc123': false,
  'abc 123': false,
  '': false,
  ' ': false,
  '\t': false,
  '\t\r': false
};

var alphaNumeric = {
  'abc': true,
  '123': true,
  'abc123 ': true,
  'abc \t\r\n': true,
  'abc132\t\r\n': true,
  'a bc': false,
  'abc 123': false,
  'abc 12344 ': false,
  '': false,
  ' ': false,
  '\t': false,
  '\t\r': false
};

var numeric = {
  '123': true,
  ' 123': true,
  '1233 ': true,
  '123 \t\r\n': true,
  'a': false,
  ' a': false,
  ')(*&^': false,
  '\t': false,
  '\t\r': false
};
var alphaNumericWithSpacesAndUnderscore = {
  'abc': true,
  'a bc': true,
  '123': true,
  'abc123': true,
  'abc 123': true,
  'abc_123 def': true,
  'abc 12334_ \t\r\n': true,
  '': false,
  ' ': false,
  '\t': false,
  '\t\r': false
};

var phoneNumbers = {
  '1234567890': true,
  '1234567890\t\r\n': true,
  '-1234567890': false,
  'a': false,
  '': false,
  '1': false,
  '+911234567890': false,
  '+1234567890': false,
  '-%18963691': false,
};
