import 'package:flutter_test/flutter_test.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';
import 'package:preetibhojan/register/OfficeAddress/ViewModel.dart';

import 'Input.dart';

void main() {
  group('Office Address validation', () {
    final areas = [
      Area(1, 'test1'),
      Area(2, 'test2'),
    ];
    var viewModel = OfficeAddressViewModel(
        repository: StubAreaRepository(input1: areas));

    group('Individual input validation', () {
      test('view model returns correct values for area', () async {
        await viewModel.fetchData();
        expect(viewModel.areas, equals(areas));
        expect(viewModel.currentArea, equals(areas[0]));

        viewModel.currentArea = areas[1];

        expect(viewModel.currentArea, equals(areas[1]));
      });

      test('tower must be alphanumeric', () {
        alphaNumeric.forEach((tower, isValid) {
          viewModel.tower = () => tower;
          expect(viewModel.isTowerValid(), equals(isValid));
        });
      });

      test('floor must be numeric', () {
        numeric.forEach((floor, isValid) {
          viewModel.floor = () => floor;
          expect(viewModel.isFloorValid(), equals(isValid));
        });
      });

      test('office no. must consist of only alphanumeric, _, and spaces', () {
        alphaNumericWithSpacesAndUnderscore.forEach((offIceNumber, isValid) {
          viewModel.officeNumber = () => offIceNumber;
          expect(viewModel.isOfficeNumberValid(), equals(isValid));
        });
      });

      test('company must consist of only alphanumeric, _, and spaces', () {
        alphaNumericWithSpacesAndUnderscore.forEach((offIceNumber, isValid) {
          viewModel.company = () => offIceNumber;
          expect(viewModel.isCompanyValid(), equals(isValid));
        });
      });

      group('isValid should return false when any of input is invalid', () {
        test(
            'isValid should return false when tower is invalid even if all others are valid',
                () {
              viewModel.tower = () => '@';
              viewModel.floor = () => '1';
              viewModel.officeNumber = () => '12';
          viewModel.company = () => 'st34';

          expect(viewModel.isValid(), equals(false));
            });

        test(
            'isValid should return false when floor is invalid even if all others are valid',
                () {
              viewModel.floor = () => '@';
              viewModel.tower = () => '1';
              viewModel.officeNumber = () => '12';
              viewModel.company = () => 'st34';

              expect(viewModel.isValid(), equals(false));
            });

        test(
            'isValid should return false when office number is invalid even if all others are valid',
                () {
              viewModel.officeNumber = () => '@';
              viewModel.floor = () => '1';
              viewModel.tower = () => '12';
              viewModel.company = () => 'st34';

              expect(viewModel.isValid(), equals(false));
            });

        test(
            'isValid should return false when company is invalid even if all others are valid',
                () {
              viewModel.company = () => '@';
              viewModel.floor = () => '1';
              viewModel.tower = () => '12';
              viewModel.officeNumber = () => 'st34';

              expect(viewModel.isValid(), equals(false));
            });
      });
    });
  });
}
