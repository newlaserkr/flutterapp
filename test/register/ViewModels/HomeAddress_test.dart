import 'package:flutter_test/flutter_test.dart';
import 'package:preetibhojan/common/delivery_location/Models.dart';
import 'package:preetibhojan/common/delivery_location/Repositories.dart';
import 'package:preetibhojan/register/HomeAddress/ViewModel.dart';

import 'Input.dart';

void main() {
  group('Home Address Validation', () {
    final areas = [
      Area(1, 'test1'),
      Area(2, 'test2'),
    ];

    final localities = [
      Locality(1, 'locality 1', 1),
      Locality(2, 'locality 2', 1),
      Locality(1, 'locality 3', 2),
      Locality(2, 'locality 4', 2),
    ];

    var viewModel = HomeAddressViewModel(
      repository: StubAreaRepository(input2: localities, input1: areas),
    );

    group('Individual input validation', () {
      test('view model returns correct values for area and locality', () async {
        await viewModel.fetchData();

        expect(viewModel.area, equals(areas));

        viewModel.currentArea = areas[0];
        expect(viewModel.locality, equals([localities[0], localities[1]]));
        expect(viewModel.currentArea, equals(areas[0]));
        expect(viewModel.currentLocality, equals(localities[0]));

        viewModel.currentArea = areas[1];

        await viewModel.fetchData();

        expect(viewModel.locality, equals([localities[2], localities[3]]));
        expect(viewModel.currentArea, equals(areas[1]));
        expect(viewModel.currentLocality, equals(localities[2]));
      });

      test('society should contain only a-z, A-Z, 0-9, _ and space', () {
        alphaNumericWithSpacesAndUnderscore.forEach((society, isValid) {
          viewModel.society = () => society;
          expect(viewModel.isSocietyValid(), equals(isValid));
        });
      });

      test('building shouuld contain only a-z, A-Z, 0-9, _ and space', () {
        alphaNumericWithSpacesAndUnderscore.forEach((building, isValid) {
          viewModel.building = () => building;
          expect(viewModel.isBuildingValid(), equals(isValid));
        });
      });

      test('flat number should contain only a-z, A-Z, 0-9, _ and space', () {
        alphaNumericWithSpacesAndUnderscore.forEach((flatNumber, isValid) {
          viewModel.flatNumber = () => flatNumber;
          expect(viewModel.isFlatNumberValid(), equals(isValid));
        });
      });
    });

    group(
        'isValid should return false if at least one of the fields in invalid',
            () {
          test(
              'isValid should return false if society is invalid even if all others are valid',
                  () {
                viewModel.society = () => '@#';
                viewModel.building = () => 'a';
                viewModel.flatNumber = () => 'a';

                expect(viewModel.isValid(), equals(false));
              });

          test(
              'isValid should return false if building is invalid even if all others are valid',
                  () {
                viewModel.building = () => '@#';
                viewModel.society = () => 'a';
                viewModel.flatNumber = () => 'a';

                expect(viewModel.isValid(), equals(false));
              });
          test(
              'isValid should return false if flat no. is invalid even if all others are valid',
                  () {
                viewModel.flatNumber = () => '@#';
                viewModel.building = () => 'a';
                viewModel.society = () => 'a';

                expect(viewModel.isValid(), equals(false));
              });
        });
  });
}
