import 'package:flutter_test/flutter_test.dart';
import 'package:preetibhojan/common/Config.dart';
import 'package:preetibhojan/common/deliverables/Repositories.dart';
import 'package:preetibhojan/common/meals/Model.dart';
import 'package:preetibhojan/home/Booking/Model.dart';
import 'package:preetibhojan/home/Booking/Repositories.dart';
import 'package:preetibhojan/home/Booking/ViewModel.dart';

void main() {
  group('nextSaturday', () {
    // A sunday
    final now = DateTime(2020, 7, 26);
    test('works for sunday', () {
      expect(nextSaturday(now), equals(DateTime(2020, 8, 1)));
    });
    test('works for monday', () {
      expect(nextSaturday(now.add(Duration(days: 1))),
          equals(DateTime(2020, 8, 1)));
    });
    test('works for tuesday', () {
      expect(nextSaturday(now.add(Duration(days: 2))),
          equals(DateTime(2020, 8, 1)));
    });
    test('works for wednesday', () {
      expect(nextSaturday(now.add(Duration(days: 3))),
          equals(DateTime(2020, 8, 1)));
    });
    test('works for thursday', () {
      expect(nextSaturday(now.add(Duration(days: 4))),
          equals(DateTime(2020, 8, 1)));
    });
    test('works for friday', () {
      expect(nextSaturday(now.add(Duration(days: 5))),
          equals(DateTime(2020, 8, 1)));
    });
    test('works for saturday', () {
      expect(nextSaturday(now.add(Duration(days: 6))),
          equals(DateTime(2020, 8, 8)));
    });
  });
  group('Booking View model test', () {
    /// Assumes GST is 5%
    initConfig(
      StubConfigRepository(
        Config(5, 5, 250),
      ),
    );

    test(
        'calculate cost calculates for remaining days when at least one meal '
        'is ordered for more than 3 days', () async {
      final creditAvailable = 100.toDouble();
      final bookingViewModel = BookingViewModel(
        StubPaymentInfoRepository(
          PaymentInfo(
            {
              Meal(1, 0, null, 'Breakfast', null, null, null): PackageInfo(
                20,
                [1, 2, 3, 4, 5],
              ),
              Meal(2, 1, null, 'Lunch', null, null, null): PackageInfo(
                60,
                [1, 5, 6],
              ),
              Meal(3, 2, null, 'Dinner', null, null, null): PackageInfo(
                60,
                [4, 5, 6],
              ),
            },
            creditAvailable,
          ),
        ),
        // A monday. So calculations will be from Tuesday,
        StubDeliverableRepository([], [], []),
        DateTime(2020, 7, 20),
      );

      await bookingViewModel.fetchData();

      expect(bookingViewModel.creditAvailable, equals(creditAvailable));
      expect(bookingViewModel.totalExcludingTaxAndDeposit, equals(380));
      expect(bookingViewModel.totalIncludingTaxAndDeposit, equals(549));
      expect(bookingViewModel.till, equals(DateTime(2020, 7, 25)));
    });

    test(
        'calculate cost calculates next week when no meal '
        'is ordered for more than 3 days', () async {
      final creditAvailable = 100.toDouble();
      final bookingViewModel = BookingViewModel(
        StubPaymentInfoRepository(
          PaymentInfo({
            Meal(
                1,
                0,
                null,
                'Breakfast',
                null,
                null,
                null): PackageInfo(
              20,
              [1, 2, 3, 4, 5],
            ),
            Meal(
                2,
                1,
                null,
                'Lunch',
                null,
                null,
                null): PackageInfo(
              60,
              [1, 5, 6],
            ),
            Meal(
                3,
                2,
                null,
                'Dinner',
                null,
                null,
                null): PackageInfo(
              60,
              [4, 5, 6],
            ),
          }, creditAvailable),
        ),
        StubDeliverableRepository([], [], []),
        // A thursday. So calculations will be from Friday
        DateTime(2020, 7, 23),
      );

      await bookingViewModel.fetchData();

      expect(bookingViewModel.creditAvailable, equals(creditAvailable));
      expect(bookingViewModel.totalExcludingTaxAndDeposit, equals(720));
      expect(bookingViewModel.totalIncludingTaxAndDeposit, equals(906));
      expect(bookingViewModel.till, equals(DateTime(2020, 8, 1)));
    });

    test('calculate cost works for sunday', () async {
      final creditAvailable = 100.toDouble();
      final bookingViewModel = BookingViewModel(
        StubPaymentInfoRepository(
          PaymentInfo(
            {
              Meal(
                  1,
                  0,
                  null,
                  'Breakfast',
                  null,
                  null,
                  null): PackageInfo(
                20,
                [1, 2, 3, 4, 5],
              ),
              Meal(
                  2,
                  1,
                  null,
                  'Lunch',
                  null,
                  null,
                  null): PackageInfo(
                60,
                [1, 5, 6],
              ),
              Meal(
                  3,
                  2,
                  null,
                  'Diner',
                  null,
                  null,
                  null): PackageInfo(
                60,
                [4, 5, 6],
              ),
            },
            creditAvailable,
          ),
        ),
        StubDeliverableRepository([], [], []),
        // A saturday + 1 = sunday
        DateTime(2020, 7, 25),
      );

      await bookingViewModel.fetchData();

      expect(bookingViewModel.creditAvailable, equals(creditAvailable));
      expect(bookingViewModel.totalExcludingTaxAndDeposit, equals(460));
      expect(bookingViewModel.totalIncludingTaxAndDeposit, equals(633));
      expect(bookingViewModel.till, equals(DateTime(2020, 8, 1)));
    });

    test('calculate cost works for saturday', () async {
      final creditAvailable = 100.toDouble();
      final bookingViewModel = BookingViewModel(
        StubPaymentInfoRepository(
          PaymentInfo(
            {
              Meal(
                  1,
                  0,
                  null,
                  'Breakfast',
                  null,
                  null,
                  null): PackageInfo(
                20,
                [1, 2, 3, 4, 5],
              ),
              Meal(
                  2,
                  1,
                  null,
                  'Lunch',
                  null,
                  null,
                  null): PackageInfo(
                60,
                [1, 5, 6],
              ),
              Meal(
                  3,
                  2,
                  null,
                  'Dinner',
                  null,
                  null,
                  null): PackageInfo(
                60,
                [4, 5, 6],
              ),
            },
            creditAvailable,
          ),
        ),
        StubDeliverableRepository([], [], []),
        // A thursday. So calculations will be from Friday
        DateTime(2020, 7, 24),
      );

      await bookingViewModel.fetchData();

      expect(bookingViewModel.creditAvailable, equals(creditAvailable));
      expect(bookingViewModel.totalExcludingTaxAndDeposit, equals(580));
      expect(bookingViewModel.totalIncludingTaxAndDeposit, equals(759));
      expect(bookingViewModel.till, equals(DateTime(2020, 8, 1)));
    });
  });
}
