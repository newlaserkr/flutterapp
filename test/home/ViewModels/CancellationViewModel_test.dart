import 'package:flutter_test/flutter_test.dart';
import 'package:preetibhojan/common/cancel/Model.dart';
import 'package:preetibhojan/common/cancel/Repository.dart';
import 'package:preetibhojan/common/deliverables/Model.dart';
import 'package:preetibhojan/common/deliverables/Repositories.dart';
import 'package:preetibhojan/home/Cancellation/Model.dart';
import 'package:preetibhojan/home/Cancellation/Repository.dart';
import 'package:preetibhojan/home/Cancellation/ViewModel.dart';

void main() {
  group('CancellationViewModel test', () {
    test(
        'test displayTiles starts from tomorrow when deliverables not set '
        'for tomorrow ', () async {
      var date = DateTime(2020, 7, 22);
      final viewModel = CancellationViewModel(
        StubCancellationInfoRepository(
          CancellationInfo(
            [
              CancellationMeal(1, 'Breakfast', [1, 2, 3, 4, 5], 0),
              CancellationMeal(2, 'Lunch', [1, 2, 3, 6], 1),
              CancellationMeal(3, 'Dinner', [4, 5, 6], 2),
            ],
            DateTime(2020, 8, 1),
          ),
        ),
        StubCancelledMealsRepository(
          [
            CancelledMeal(
              2,
              date.add(
                Duration(days: 10),
              ),
            ),

            /// Everything 7 days later is cancelled. No display tile
            /// representing 29th July should be there
            CancelledMeal(
              1,
              date.add(
                Duration(days: 7),
              ),
            ),
            CancelledMeal(
              2,
              date.add(
                Duration(days: 7),
              ),
            ),
          ],
        ),
        StubDeliverableRepository(
          [
            Deliverable(null, null, null, null, date),
          ],
          [],
          [],
        ),
        date,
      );

      var temp = viewModel.displayTiles;
      expect(
        await temp,
        equals(
          [
            DisplayTile(date.add(Duration(days: 1)), [
              DisplayMeal('Breakfast', date.add(Duration(days: 1)), 1),
              DisplayMeal('Dinner', date.add(Duration(days: 1)), 3),
            ]),
            DisplayTile(date.add(Duration(days: 2)), [
              DisplayMeal('Breakfast', date.add(Duration(days: 2)), 1),
              DisplayMeal('Dinner', date.add(Duration(days: 2)), 3),
            ]),
            DisplayTile(date.add(Duration(days: 3)), [
              DisplayMeal('Lunch', date.add(Duration(days: 3)), 2),
              DisplayMeal('Dinner', date.add(Duration(days: 3)), 3),
            ]),
            DisplayTile(date.add(Duration(days: 5)), [
              DisplayMeal('Breakfast', date.add(Duration(days: 4)), 1),
              DisplayMeal('Lunch', date.add(Duration(days: 4)), 2),
            ]),
            DisplayTile(date.add(Duration(days: 6)), [
              DisplayMeal('Breakfast', date.add(Duration(days: 6)), 1),
              DisplayMeal('Lunch', date.add(Duration(days: 6)), 2),
            ]),
            DisplayTile(date.add(Duration(days: 8)), [
              DisplayMeal('Breakfast', date.add(Duration(days: 8)), 1),
              DisplayMeal('Dinner', date.add(Duration(days: 8)), 3),
            ]),
            DisplayTile(date.add(Duration(days: 9)), [
              DisplayMeal('Breakfast', date.add(Duration(days: 9)), 1),
              DisplayMeal('Dinner', date.add(Duration(days: 9)), 2),
            ]),
            DisplayTile(date.add(Duration(days: 10)), [
              DisplayMeal('Dinner', date.add(Duration(days: 10)), 3),
            ]),
          ],
        ),
      );
    });

    test(
        'test displayTiles starts from tomorrow when deliverables set '
        'for tomorrow ', () async {
      var date = DateTime(2020, 7, 22);
      final viewModel = CancellationViewModel(
        StubCancellationInfoRepository(
          CancellationInfo(
            [
              CancellationMeal(1, 'Breakfast', [1, 2, 3, 4, 5], 0),
              CancellationMeal(2, 'Lunch', [1, 2, 3, 6], 1),
              CancellationMeal(3, 'Dinner', [4, 5, 6], 2),
            ],
            DateTime(2020, 8, 1),
          ),
        ),
        StubCancelledMealsRepository(
          [
            CancelledMeal(
              2,
              date.add(
                Duration(days: 10),
              ),
            ),

            /// Everything 7 days later is cancelled. No display tile
            /// representing 29th July should be there
            CancelledMeal(
              1,
              date.add(
                Duration(days: 7),
              ),
            ),
            CancelledMeal(
              2,
              date.add(
                Duration(days: 7),
              ),
            ),
          ],
        ),
        StubDeliverableRepository(
          [
            Deliverable(null, null, null, null, date.add(Duration(days: 1))),
          ],
          [],
          [],
        ),
        date,
      );

      var temp = viewModel.displayTiles;
      expect(
        await temp,
        equals(
          [
            DisplayTile(date.add(Duration(days: 2)), [
              DisplayMeal('Breakfast', date.add(Duration(days: 2)), 1),
              DisplayMeal('Dinner', date.add(Duration(days: 2)), 3),
            ]),
            DisplayTile(date.add(Duration(days: 3)), [
              DisplayMeal('Lunch', date.add(Duration(days: 3)), 2),
              DisplayMeal('Dinner', date.add(Duration(days: 3)), 3),
            ]),
            DisplayTile(date.add(Duration(days: 5)), [
              DisplayMeal('Breakfast', date.add(Duration(days: 4)), 1),
              DisplayMeal('Lunch', date.add(Duration(days: 4)), 2),
            ]),
            DisplayTile(date.add(Duration(days: 6)), [
              DisplayMeal('Breakfast', date.add(Duration(days: 6)), 1),
              DisplayMeal('Lunch', date.add(Duration(days: 6)), 2),
            ]),
            DisplayTile(date.add(Duration(days: 8)), [
              DisplayMeal('Breakfast', date.add(Duration(days: 8)), 1),
              DisplayMeal('Dinner', date.add(Duration(days: 8)), 3),
            ]),
            DisplayTile(date.add(Duration(days: 9)), [
              DisplayMeal('Breakfast', date.add(Duration(days: 9)), 1),
              DisplayMeal('Dinner', date.add(Duration(days: 9)), 2),
            ]),
            DisplayTile(date.add(Duration(days: 10)), [
              DisplayMeal('Dinner', date.add(Duration(days: 10)), 3),
            ]),
          ],
        ),
      );
    });
  });
}
