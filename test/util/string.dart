import 'package:flutter_test/flutter_test.dart';
import 'package:preetibhojan/util/string.dart';

void main() {
  group('string test', () {
    test('capitalize', () {
      expect('tHIIs'.capitalize(), equals('Thiis'));
    });
  });
}
